/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Timber.Modelo;

import java.io.Serializable;
import java.sql.Date;
import java.util.ArrayList;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

/**
 *
 * @author Alba
 */
public class Paciente implements Serializable {

    @Id @GeneratedValue
    private Long idPaciente;
    private String dni;
    private String sexo;
    private int edad;
    private String nombre;
    private String apellido1;
    private String apellido2;
    private Date fechaNacimiento;
    private String direccion;
    private String referencias;
    private String poblacion;
    private String cPostal;
    private String email;
    private int fijo;
    private int mobil;
    private String alta;
    private InformeSocial informe;
    private FichaSanitaria ficha;
    private Dependencia dependencia;
    private ServicioTeleasistencia servicio;
    private ArrayList<Contacto> contactos;
    private ArrayList<Llamada> llamadas;

    /* Constructor */
    public Paciente(){
        contactos = new ArrayList<Contacto>();
        llamadas = new ArrayList<Llamada>();
    }
    
    public Paciente(String dni, String sexo, String nombre, String apellido1, String apellido2, 
            Date fechaNacimiento, String direccion, String referencias, 
            String poblacion, String cPostal, int fijo, int mobil, String email, String alta) {
        
        this.dni = dni;
        this.sexo = sexo;
        this.nombre = nombre;
        this.apellido1 = apellido1;
        this.apellido2 = apellido2;
        this.fechaNacimiento = fechaNacimiento;
        this.direccion = direccion;
        this.referencias = referencias;
        this.poblacion = poblacion;
        this.cPostal = cPostal;
        this.fijo = fijo;
        this.mobil = mobil;
        this.email = email;
        this.alta = alta;
        
    }
    
    /* Getters and setters */
    public Long getIdPaciente() {
        return idPaciente;
    }
    
    public String getDni() {
        return dni;
    }

    public String getSexo() {
        return sexo;
    }

    public int getEdad() {
        return edad;
    }

    public String getNombre() {
        return nombre;
    }

    public String getApellido1() {
        return apellido1;
    }
    
    public String getApellido2() {
        return apellido2;
    }

    public Date getFechaNacimiento() {
        return fechaNacimiento;
    }

    public String getDireccion() {
        return direccion;
    }

    public String getReferencias() {
        return referencias;
    }

    public String getPoblacion() {
        return poblacion;
    }

    public String getcPostal() {
        return cPostal;
    }

    public String getEmail() {
        return email;
    }

    public String getAlta() {
        return alta;
    }

    public int getFijo() {
        return fijo;
    }

    public int getMobil() {
        return mobil;
    }
    
    public InformeSocial getInforme() {
        return informe;
    }

    public FichaSanitaria getFicha() {
        return ficha;
    }

    public Dependencia getDependencia() {
        return dependencia;
    }

    public ServicioTeleasistencia getServicio() {
        return servicio;
    }

    public ArrayList<Contacto> getContactos() {
        return contactos;
    }

    public ArrayList<Llamada> getLlamadas() {
        return llamadas;
    }
    
    public void setIdPaciente(Long idPaciente) {
        this.idPaciente = idPaciente;
    }
    
    public void setDni(String dni) {
        this.dni = dni;
    }

    public void setSexo(String sexo) {
        this.sexo = sexo;
    }

    public void setEdad(int edad) {
        this.edad = edad;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public void setApellido1(String apellido1) {
        this.apellido1 = apellido1;
    }

    public void setApellido2(String apellido2) {
        this.apellido2 = apellido2;
    }

    public void setFechaNacimiento(Date fechaNacimiento) {
        this.fechaNacimiento = fechaNacimiento;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public void setReferencias(String referencias) {
        this.referencias = referencias;
    }

    public void setPoblacion(String poblacion) {
        this.poblacion = poblacion;
    }

    public void setcPostal(String cPostal) {
        this.cPostal = cPostal;
    }

    public void setFijo(int fijo) {
        this.fijo = fijo;
    }

    public void setMobil(int mobil) {
        this.mobil = mobil;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setAlta(String alta) {
        this.alta = alta;
    }

    public void setInforme(InformeSocial informe) {
        this.informe = informe;
    }

    public void setFicha(FichaSanitaria ficha) {
        this.ficha = ficha;
    }

    public void setDependencia(Dependencia dependencia) {
        this.dependencia = dependencia;
    }

    public void setServicio(ServicioTeleasistencia servicio) {
        this.servicio = servicio;
    }

    public void setContactos(ArrayList<Contacto> contactos) {
        this.contactos = contactos;
    }

    public void setLlamadas(ArrayList<Llamada> llamadas) {
        this.llamadas = llamadas;
    }    
    

}