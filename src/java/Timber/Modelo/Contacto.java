/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Timber.Modelo;

import java.io.Serializable;
import java.sql.Date;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

/**
 *
 * @author Alba
 */
public class Contacto implements Serializable {
    
    @Id @GeneratedValue
    private Long id_contacto;
    private long idPaciente;
    private String sexo;
    private String nombre;
    private String apellido1;
    private String apellido2;
    private String relacionPaciente;
    private String poblacion;
    private String cPostal;
    private String direccion;
    private String telefono;
    private String trabajo;
    private String horarioTrabajo;
    private String email;
    private String llaves;

    /* Constructor */
    public Contacto(){
        
    }

    public Contacto(long idPaciente, String sexo, String nombre, String apellido1, String apellido2, String relacionPaciente, String poblacion, String cPostal, String direccion, String telefono,String trabajo, String horarioTrabajo, String email, String llaves) {
        this.idPaciente = idPaciente;
        this.sexo = sexo;
        this.nombre = nombre;
        this.apellido1 = apellido1;
        this.apellido2 = apellido2;
        this.relacionPaciente = relacionPaciente;
        this.poblacion = poblacion;
        this.cPostal = cPostal;
        this.direccion = direccion;
        this.telefono = telefono;
        this.trabajo = trabajo;
        this.horarioTrabajo = horarioTrabajo;
        this.email = email;
        this.llaves = llaves;
    }
    
    /* Getters and setters */

    public long getIdPaciente() {
        return idPaciente;
    }

    public String getSexo() {
        return sexo;
    }

    public String getNombre() {
        return nombre;
    }

    public String getApellido1() {
        return apellido1;
    }
    
    public String getApellido2() {
        return apellido2;
    }

    public String getRelacionPaciente() {
        return relacionPaciente;
    }

    public String getPoblacion() {
        return poblacion;
    }

    public String getcPostal() {
        return cPostal;
    }

    public String getDireccion() {
        return direccion;
    }

    public String getTelefono() {
        return telefono;
    }

    public String getTrabajo() {
        return trabajo;
    }

    public String getHorarioTrabajo() {
        return horarioTrabajo;
    }

    public String getEmail() {
        return email;
    }

    public String getLlaves() {
        return llaves;
    }

    public void setIdPaciente(long idPaciente) {
        this.idPaciente = idPaciente;
    }

    public void setSexo(String sexo) {
        this.sexo = sexo;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public void setApellido1(String apellido1) {
        this.apellido1 = apellido1;
    }
    
    public void setApellido2(String apellido2) {
        this.apellido2 = apellido2;
    }

    public void setRelacionPaciente(String relacionPaciente) {
        this.relacionPaciente = relacionPaciente;
    }

    public void setPoblacion(String poblacion) {
        this.poblacion = poblacion;
    }

    public void setcPostal(String cPostal) {
        this.cPostal = cPostal;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public void setTrabajo(String trabajo) {
        this.trabajo = trabajo;
    }

    public void setHorarioTrabajo(String horarioTrabajo) {
        this.horarioTrabajo = horarioTrabajo;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setLlaves(String llaves) {
        this.llaves = llaves;
    }
    
}
