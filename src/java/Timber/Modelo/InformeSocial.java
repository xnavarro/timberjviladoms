/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Timber.Modelo;

import java.sql.Date;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

/**
 *
 * @author Alba
 */
public class InformeSocial {
    
    private Long idPaciente;
    private String convivencia;
    private String horarioSol;
    private String relacionFamilia;
    private String visitas;
    private String compatibilidad;
    
    /* Constructor */
    public InformeSocial(){
        
    }

    public InformeSocial(Long idPaciente, String convivencia, String horarioSol, String relacionFamilia, String visitas, String compatibilidad) {
        
        this.idPaciente = idPaciente;
        this.convivencia = convivencia;
        this.horarioSol = horarioSol;
        this.relacionFamilia = relacionFamilia;
        this.visitas = visitas;
        this.compatibilidad = compatibilidad;
    }
    
    /* Getters and Setters */

    public Long getIdPaciente() {
        return idPaciente;
    }

    public String getConvivencia() {
        return convivencia;
    }

    public String getHorarioSol() {
        return horarioSol;
    }

    public String getRelacionFamilia() {
        return relacionFamilia;
    }

    public String getVisitas() {
        return visitas;
    }

    public String getCompatibilidad() {
        return compatibilidad;
    }

    public void setIdPaciente(Long idPaciente) {
        this.idPaciente = idPaciente;
    }

    public void setConvivencia(String convivencia) {
        this.convivencia = convivencia;
    }

    public void setHorarioSol(String horarioSol) {
        this.horarioSol = horarioSol;
    }

    public void setRelacionFamilia(String relacionFamilia) {
        this.relacionFamilia = relacionFamilia;
    }

    public void setVisitas(String visitas) {
        this.visitas = visitas;
    }

    public void setCompatibilidad(String compatibilidad) {
        this.compatibilidad = compatibilidad;
    }
    
}
