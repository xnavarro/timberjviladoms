/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Timber.Modelo;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

/**
 *
 * @author Alba
 */
public class Trabajador implements Serializable {
    
    @Id @GeneratedValue
    private Long idEmpleado;
    private String dni;
    private String nombre;
    private String apellido1;
    private String apellido2;
    private String fechaNacimiento;
    private String esAdmin;
    private String usuario;
    private String contrasenya;

    /* Constructor */
    public Trabajador(){
        
    }
    
    public Trabajador(long idEmpleado, String dni, String nombre, String apellido1, String apellido2, String fechaNacimiento, String esAdmin, String usuario, String contrassenya) {
        this.idEmpleado = idEmpleado;
        this.dni = dni;
        this.nombre = nombre;
        this.apellido1 = apellido1;
        this.apellido2 = apellido2;
        this.fechaNacimiento = fechaNacimiento;
        this.esAdmin = esAdmin;
        this.usuario = usuario;
        this.contrasenya = contrassenya;
    }
    
    /* Getters and setters */
    
    
    
    public String getDni() {
        return dni;
    }

    public String getNombre() {
        return nombre;
    }

    public String getApellido1() {
        return apellido1;
    }
    
    public String getApellido2() {
        return apellido2;
    }

    public String getFechaNacimiento() {
        return fechaNacimiento;
    }

    public String getEsAdmin() {
        return esAdmin;
    }

    public String getUsuario() {
        return usuario;
    }

    public String getContrasenya() {
        return contrasenya;
    }

    public Long getIdEmpleado() {
        return idEmpleado;
    }
    
    public void setDni(String dni) {
        this.dni = dni;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
    
    public void setApellido1(String apellido1) {
        this.apellido1 = apellido1;
    }

    public void setApellido2(String apellido2) {
        this.apellido2 = apellido2;
    }

    public void setFechaNacimiento(String fechaNacimiento) {
        this.fechaNacimiento = fechaNacimiento;
    }

    public void setEsAdmin(String esAdmin) {
        this.esAdmin = esAdmin;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public void setContrasenya(String contrasenya) {
        this.contrasenya = contrasenya;
    }    

    public void setIdEmpleado(Long idEmpleado) {
        this.idEmpleado = idEmpleado;
    }    

}
