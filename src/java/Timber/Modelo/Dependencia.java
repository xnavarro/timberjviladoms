/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Timber.Modelo;

/**
 *
 * @author Alba
 */
public class Dependencia {
    
    private Long idPaciente;
    private String deambulacion;
    private String gradoDependencia;
    private String nivelDependencia;
    private String prestacionEconomica;
    
    /* Constructor */
    public Dependencia(){
        
    }

    public Dependencia(Long idPaciente, String deambulacion, String gradoDependencia, String nivelDependencia, String prestacionEconomica) {
        this.idPaciente = idPaciente;
        this.deambulacion = deambulacion;
        this.gradoDependencia = gradoDependencia;
        this.nivelDependencia = nivelDependencia;
        this.prestacionEconomica = prestacionEconomica;
    }
    
    /* Getters and Setters */

    public Long getIdPaciente() {
        return idPaciente;
    }

    public String getDeambulacion() {
        return deambulacion;
    }

    public String getGradoDependencia() {
        return gradoDependencia;
    }

    public String getNivelDependencia() {
        return nivelDependencia;
    }

    public String getPrestacionEconomica() {
        return prestacionEconomica;
    }

    public void setIdPaciente(Long idPaciente) {
        this.idPaciente = idPaciente;
    }

    public void setDeambulacion(String deambulacion) {
        this.deambulacion = deambulacion;
    }

    public void setGradoDependencia(String gradoDependencia) {
        this.gradoDependencia = gradoDependencia;
    }

    public void setNivelDependencia(String nivelDependencia) {
        this.nivelDependencia = nivelDependencia;
    }

    public void setPrestacionEconomica(String prestacionEconomica) {
        this.prestacionEconomica = prestacionEconomica;
    }
    

}
