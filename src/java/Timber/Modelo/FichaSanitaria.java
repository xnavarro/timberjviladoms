/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Timber.Modelo;

/**
 *
 * @author Alba
 */
public class FichaSanitaria {
    
    private Long idPaciente;
    private double estatura;
    private double peso;
    private String estadoActual;
    private String patologia;
    private String alergias;
    private String sistemasAfectados;
    private String enfermedades;
    private String medicacion;
    private String capacidadCognitiva;
    private String dificultades;
    private String vacunas;
    private String centroSanitario;
    
    public FichaSanitaria(){
        
    }
    
    /* Constructor */
    public FichaSanitaria(Long idPaciente, double estatura, double peso, String estadoActual
            , String patologia, String alergias, String sistemasAfectados
            , String enfermedades, String medicacion, String capacidadCognitiva
            , String dificultades, String vacunas, String centroSanitario) {
        
        this.idPaciente = idPaciente;
        this.estatura = estatura;
        this.peso = peso;
        this.estadoActual = estadoActual;
        this.patologia = patologia;
        this.alergias = alergias;
        this.sistemasAfectados = sistemasAfectados;
        this.enfermedades = enfermedades;
        this.medicacion = medicacion;
        this.capacidadCognitiva = capacidadCognitiva;
        this.dificultades = dificultades;
        this.vacunas = vacunas;
        this.centroSanitario = centroSanitario;
    }
    
    /* Getters and setters */

    public Long getIdPaciente() {
        return idPaciente;
    }

    public double getEstatura() {
        return estatura;
    }

    public double getPeso() {
        return peso;
    }

    public String getEstadoActual() {
        return estadoActual;
    }

    public String getPatologia() {
        return patologia;
    }

    public String getAlergias() {
        return alergias;
    }

    public String getSistemasAfectados() {
        return sistemasAfectados;
    }

    public String getEnfermedades() {
        return enfermedades;
    }

    public String getMedicacion() {
        return medicacion;
    }

    public String getCapacidadCognitiva() {
        return capacidadCognitiva;
    }

    public String getDificultades() {
        return dificultades;
    }

    public String getVacunas() {
        return vacunas;
    }

    public String getCentroSanitario() {
        return centroSanitario;
    }

    public void setIdPaciente(Long idPaciente) {
        this.idPaciente = idPaciente;
    }

    public void setEstatura(double estatura) {
        this.estatura = estatura;
    }

    public void setPeso(double peso) {
        this.peso = peso;
    }

    public void setEstadoActual(String estadoActual) {
        this.estadoActual = estadoActual;
    }

    public void setPatologia(String patologia) {
        this.patologia = patologia;
    }

    public void setAlergias(String alergias) {
        this.alergias = alergias;
    }

    public void setSistemasAfectados(String sistemasAfectados) {
        this.sistemasAfectados = sistemasAfectados;
    }

    public void setEnfermedades(String enfermedades) {
        this.enfermedades = enfermedades;
    }

    public void setMedicacion(String medicacion) {
        this.medicacion = medicacion;
    }

    public void setCapacidadCognitiva(String capacidadCognitiva) {
        this.capacidadCognitiva = capacidadCognitiva;
    }

    public void setDificultades(String dificultades) {
        this.dificultades = dificultades;
    }

    public void setVacunas(String vacunas) {
        this.vacunas = vacunas;
    }

    public void setCentroSanitario(String centroSanitario) {
        this.centroSanitario = centroSanitario;
    }
    
    
}
