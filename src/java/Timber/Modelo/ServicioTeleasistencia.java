/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Timber.Modelo;

import java.sql.Date;

/**
 *
 * @author Alba
 */
public class ServicioTeleasistencia {
    
    private Long idEmpleado;
    private Date fecha;
    private String cuotasServicio;
    private String datosBanco;
    private String nombreContratador;
    private String apellido1Contratador;
    private String apellido2Contratador;

    
    private String dniContratador;
    private String telefonoContratador;
    private String provinciaContratador;
    private String cpostalContratador;
    private String serviciosBasicos;
    private String modalidades;
    private String prestacionesAdicionales;
    
    public ServicioTeleasistencia(){
        
    }

    /* Constructor */

    public ServicioTeleasistencia(Long idEmpleado, Date fecha, String cuotasServicio, String datosBanco, String nombreContratador, String apellido1Contratador, String apellido2Contratador, String dniContratador, String telefonoContratador, String provinciaContratador, String cpostalContratador, String serviciosBasicos, String modalidades, String prestacionesAdicionales) {
        this.idEmpleado = idEmpleado;
        this.fecha = fecha;
        this.cuotasServicio = cuotasServicio;
        this.datosBanco = datosBanco;
        this.nombreContratador = nombreContratador;
        this.apellido1Contratador = apellido1Contratador;
        this.apellido2Contratador = apellido2Contratador;
        this.dniContratador = dniContratador;
        this.telefonoContratador = telefonoContratador;
        this.provinciaContratador = provinciaContratador;
        this.cpostalContratador = cpostalContratador;
        this.serviciosBasicos = serviciosBasicos;
        this.modalidades = modalidades;
        this.prestacionesAdicionales = prestacionesAdicionales;
    }
    
    /* Getters and Setters */

    public Long getIdEmpleado() {
        return idEmpleado;
    }

    public Date getFecha() {
        return fecha;
    }

    public String getCuotasServicio() {
        return cuotasServicio;
    }

    public String getDatosBanco() {
        return datosBanco;
    }

    public String getNombreContratador() {
        return nombreContratador;
    }

    public String getDniContratador() {
        return dniContratador;
    }

    public String getTelefonoContratador() {
        return telefonoContratador;
    }

    public String getProvinciaContratador() {
        return provinciaContratador;
    }

    public String getCpostalContratador() {
        return cpostalContratador;
    }

    public String getServiciosBasicos() {
        return serviciosBasicos;
    }

    public String getModalidades() {
        return modalidades;
    }

    public String getPrestacionesAdicionales() {
        return prestacionesAdicionales;
    }
    
    public String getApellido1Contratador() {
        return apellido1Contratador;
    }

    public void setApellido1Contratador(String apellido1Contratador) {
        this.apellido1Contratador = apellido1Contratador;
    }

    public String getApellido2Contratador() {
        return apellido2Contratador;
    }

    public void setApellido2Contratador(String apellido2Contratador) {
        this.apellido2Contratador = apellido2Contratador;
    }

    public void setIdEmpleado(Long idEmpleado) {
        this.idEmpleado = idEmpleado;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public void setCuotasServicio(String cuotasServicio) {
        this.cuotasServicio = cuotasServicio;
    }

    public void setDatosBanco(String datosBanco) {
        this.datosBanco = datosBanco;
    }

    public void setNombreContratador(String nombreContratador) {
        this.nombreContratador = nombreContratador;
    }

    public void setDniContratador(String dniContratador) {
        this.dniContratador = dniContratador;
    }

    public void setTelefonoContratador(String telefonoContratador) {
        this.telefonoContratador = telefonoContratador;
    }

    public void setProvinciaContratador(String provinciaContratador) {
        this.provinciaContratador = provinciaContratador;
    }

    public void setCpostalContratador(String cpostalContratador) {
        this.cpostalContratador = cpostalContratador;
    }

    public void setServiciosBasicos(String serviciosBasicos) {
        this.serviciosBasicos = serviciosBasicos;
    }

    public void setModalidades(String modalidades) {
        this.modalidades = modalidades;
    }

    public void setPrestacionesAdicionales(String prestacionesAdicionales) {
        this.prestacionesAdicionales = prestacionesAdicionales;
    }
    
    
}
