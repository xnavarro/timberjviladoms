/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Timber.Modelo;

import java.io.Serializable;
import java.sql.Date;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

/**
 *
 * @author Alba
 */
public class Llamada implements Serializable {
    
    @Id @GeneratedValue
    private Long id_llamada;
    private Long idPaciente;
    private Long idContacto;
    private Long idEmpleado;
    private Date inicioLlamada;
    private Date finLlamada;
    
    /* Constructor */
    
    public Llamada(){
    
    }

    public Llamada(Long idPaciente, Long idContacto, Long idEmpleado, Date inicioLlamada, Date finLlamada) {
        this.idPaciente = idPaciente;
        this.idContacto = idContacto;
        this.idEmpleado = idEmpleado;
        this.inicioLlamada = inicioLlamada;
        this.finLlamada = finLlamada;
    }
    
    /* Getters and setters */

    public Long getIdPaciente() {
        return idPaciente;
    }

    public Long getIdContacto() {
        return idContacto;
    }

    public Long getIdEmpleado() {
        return idEmpleado;
    }

    public Date getInicioLlamada() {
        return inicioLlamada;
    }

    public Date getFinLlamada() {
        return finLlamada;
    }

    public void setIdPaciente(Long idPaciente) {
        this.idPaciente = idPaciente;
    }

    public void setIdContacto(Long idContacto) {
        this.idContacto = idContacto;
    }

    public void setIdEmpleado(Long idEmpleado) {
        this.idEmpleado = idEmpleado;
    }

    public void setInicioLlamada(Date inicioLlamada) {
        this.inicioLlamada = inicioLlamada;
    }

    public void setFinLlamada(Date finLlamada) {
        this.finLlamada = finLlamada;
    }
    
    
    
}
