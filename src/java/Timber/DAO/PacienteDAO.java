/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Timber.DAO;


import Timber.Modelo.Contacto;
import Timber.Modelo.Dependencia;
import Timber.Modelo.FichaSanitaria;
import Timber.Modelo.InformeSocial;
import Timber.Modelo.Paciente;
import Timber.Modelo.ServicioTeleasistencia;
import Timber.Modelo.Trabajador;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Alba
 */
public class PacienteDAO {
    /*
    public static final String INSERT_EMPLOYEE_QUERY = "INSERT INTO PACIENTE (nombre,apellido1) values (?,?)";*/
    
    public PacienteDAO(){
        
    }
  
           
    public List<Paciente> obtenerPacientes() throws SQLException {
        List<Paciente> pacientes = new ArrayList<Paciente>();
        
        Connection con = ConexionDB.conexionDB();
        Statement st = con.createStatement();
        ResultSet rs = null;
        String consulta = "select * from paciente";
        rs = st.executeQuery(consulta);
        long idPaciente;
        while(rs.next()){
                Paciente pac = new Paciente();
                idPaciente = rs.getLong("id_paciente");
                pac.setIdPaciente(idPaciente);
                pac.setNombre(rs.getString("nombre"));
                pac.setApellido1(rs.getString("apellido1"));
                pac.setApellido2(rs.getString("apellido2"));
                pac.setDni(rs.getString("dni"));
                pac.setEdad(rs.getInt("edad"));
                pac.setFechaNacimiento(rs.getDate("fecha_nacimiento"));
                pac.setSexo(rs.getString("sexo"));
                pac.setDireccion(rs.getString("direccion"));
                pac.setReferencias(rs.getString("referencias"));
                pac.setPoblacion(rs.getString("poblacion"));
                pac.setcPostal(rs.getString("c_postal"));
                pac.setEmail(rs.getString("email"));
//                // DEPENDENCIA
//                ResultSet dependencia = st.executeQuery("Select * from dependencia where id_paciente = "+idPaciente);
//                if (dependencia.first()) {
//                    pac.setDependencia(new Dependencia(dependencia.getLong("id_paciente"), dependencia.getString("deambulacion"), dependencia.getString("grado_dependencia"), dependencia.getString("nivel_dependencia"), dependencia.getString("prestacion_economica")));
//                }
//                // INFORME SOCIAL
//                ResultSet informesocial = st.executeQuery("Select * from informesocial where id_paciente = "+idPaciente);
//                if (informesocial.first()) {
//                    pac.setInforme(new InformeSocial(informesocial.getLong("id_paciente"), informesocial.getString("convivencia"), informesocial.getString("horarioSol"), informesocial.getString("relacionFamilia"), informesocial.getString("visitas"), informesocial.getString("compatibilidad")));
//                }
//                // FICHA SANITARIA
//                ResultSet fichasanitaria = st.executeQuery("Select * from fichasanitaria where id_paciente = "+idPaciente);
//                if (fichasanitaria.first()) {
//                    pac.setFicha(new FichaSanitaria(fichasanitaria.getLong("id_paciente"), fichasanitaria.getInt("estatura"), fichasanitaria.getInt("peso"), fichasanitaria.getString("estadoActual"), fichasanitaria.getString("patologia"), fichasanitaria.getString("alergias"), fichasanitaria.getString("sistemasAfectados"), fichasanitaria.getString("enfermedades"), fichasanitaria.getString("medicacion"), fichasanitaria.getString("capacidadCognitiva"), fichasanitaria.getString("dificultades"), fichasanitaria.getString("vacunas"), fichasanitaria.getString("centroSanitario")));
//                }
//                // SERVICIO TELEASISTENCIA
//                ResultSet sta = st.executeQuery("Select * from servicioteleasistencia where id_paciente = "+idPaciente);
//                if (sta.first()) {
//                    pac.setServicio(new ServicioTeleasistencia(sta.getLong("id_paciente"), sta.getDate("fecha_alta"), sta.getString("cuotasServicio"), sta.getString("datosBanco"), sta.getString("nombreContratador"), sta.getString("apellido1Contratador"), sta.getString("apellido2Contratador"), sta.getString("dniContratador"), sta.getString("telefonoContratador"), sta.getString("provinciaContratador"), sta.getString("cpostalContratador"), sta.getString("serviciosBasicos"), sta.getString("modalidades"), sta.getString("prestacionesAdicionales")));
//                }
//                // CONTACTOS
//                ResultSet aux = st.executeQuery("Select * from contacto where id_paciente = "+idPaciente);
//                ArrayList<Contacto> contactos = new ArrayList<Contacto>();
//                while (aux.next()) {
//                    contactos.add(new Contacto(aux.getLong("id_paciente"), aux.getString("nombre"), aux.getString("apellido1"), aux.getString("apellido2"), aux.getString("sexo"), aux.getString("relacion_paciente"), aux.getString("poblacion"), aux.getString("c_postal"), aux.getString("direccion"), aux.getString("telefono"), aux.getString("trabajo"), aux.getString("horario_trabajo"), aux.getString("email"), aux.getString("llaves")));
//                }
//                pac.setContactos(contactos);
                pacientes.add(pac);
        }
        return pacientes;
    }
    
    public Paciente obtenerPaciente(Paciente pac) throws SQLException {
        List<Paciente> pacientes = new ArrayList<Paciente>();
        
        ConexionDB db = ConexionDB.getInstance();
        Connection con = db.conexionDB();
        Statement st = con.createStatement();
        ResultSet rs = null;
        String consulta = "Select * from paciente where dni = '"+pac.getDni()+"'";
        rs = st.executeQuery(consulta);
        
        if (rs.first()){
                pac.setIdPaciente(rs.getLong("id_paciente"));
                pac.setNombre(rs.getString("nombre"));
                pac.setApellido1(rs.getString("apellido1"));
                pac.setApellido2(rs.getString("apellido2"));
                pac.setDni(rs.getString("dni"));
                pac.setEdad(rs.getInt("edad"));
                pac.setFechaNacimiento(rs.getDate("fecha_nacimiento"));
                pac.setSexo(rs.getString("sexo"));
                pac.setDireccion(rs.getString("direccion"));
                pac.setReferencias(rs.getString("referencias"));
                pac.setPoblacion(rs.getString("poblacion"));
                pac.setcPostal(rs.getString("c_postal"));
                pac.setcPostal(rs.getString("tlf_fijo"));
                pac.setcPostal(rs.getString("tlf_mobil"));
                pac.setEmail(rs.getString("email"));
                // DEPENDENCIA
                long idPaciente = rs.getLong("id_paciente");
                ResultSet dependencia = st.executeQuery("Select * from dependencia where id_paciente = "+idPaciente);
                if (dependencia.first()) {
                    pac.setDependencia(new Dependencia(dependencia.getLong("id_paciente"), dependencia.getString("deambulacion"), dependencia.getString("grado_dependencia"), dependencia.getString("nivel_dependencia"), dependencia.getString("prestacion_economica")));
                }
                // INFORME SOCIAL
                ResultSet informesocial = st.executeQuery("Select * from informesocial where id_paciente = "+idPaciente);
                if (informesocial.first()) {
                    pac.setInforme(new InformeSocial(informesocial.getLong("id_paciente"), informesocial.getString("convivencia"), informesocial.getString("horarioSol"), informesocial.getString("relacionFamilia"), informesocial.getString("visitas"), informesocial.getString("compatibilidad")));
                }
                // FICHA SANITARIA
                ResultSet fichasanitaria = st.executeQuery("Select * from fichasanitaria where id_paciente = "+idPaciente);
                if (fichasanitaria.first()) {
                    pac.setFicha(new FichaSanitaria(fichasanitaria.getLong("id_paciente"), fichasanitaria.getInt("estatura"), fichasanitaria.getInt("peso"), fichasanitaria.getString("estadoActual"), fichasanitaria.getString("patologia"), fichasanitaria.getString("alergias"), fichasanitaria.getString("sistemasAfectados"), fichasanitaria.getString("enfermedades"), fichasanitaria.getString("medicacion"), fichasanitaria.getString("capacidadCognitiva"), fichasanitaria.getString("dificultades"), fichasanitaria.getString("vacunas"), fichasanitaria.getString("centroSanitario")));
                }
                // SERVICIO TELEASISTENCIA
                ResultSet sta = st.executeQuery("Select * from servicioteleasistencia where id_paciente = "+idPaciente);
                if (sta.first()) {
                    pac.setServicio(new ServicioTeleasistencia(sta.getLong("id_paciente"), sta.getDate("fecha_alta"), sta.getString("cuotasServicio"), sta.getString("datosBanco"), sta.getString("nombreContratador"), sta.getString("apellido1Contratador"), sta.getString("apellido2Contratador"), sta.getString("dniContratador"), sta.getString("telefonoContratador"), sta.getString("provinciaContratador"), sta.getString("cpostalContratador"), sta.getString("serviciosBasicos"), sta.getString("modalidades"), sta.getString("prestacionesAdicionales")));
                }
                // CONTACTOS
                ResultSet aux = st.executeQuery("Select * from contacto where id_paciente = "+idPaciente);
                ArrayList<Contacto> contactos = new ArrayList<Contacto>();
                while (aux.next()) {
                    contactos.add(new Contacto(aux.getLong("id_paciente"), aux.getString("nombre"), aux.getString("apellido1"), aux.getString("apellido2"), aux.getString("sexo"), aux.getString("relacion_paciente"), aux.getString("poblacion"), aux.getString("c_postal"), aux.getString("direccion"), aux.getString("telefono"), aux.getString("trabajo"), aux.getString("horario_trabajo"), aux.getString("email"), aux.getString("llaves")));
                }
                pac.setContactos(contactos);
                pacientes.add(pac);
        }
        return pac;
    }
    
    public void borrarPaciente(Paciente pac) throws SQLException{
        ConexionDB db = ConexionDB.getInstance();
        Connection con = db.conexionDB();
        Statement st = con.createStatement();
        String consulta = "DELETE FROM paciente WHERE dni = '" + pac.getDni() + "'";
        st.executeUpdate(consulta);
    }
    
    public void insertarPaciente(Paciente pac) throws SQLException {
        ConexionDB db = ConexionDB.getInstance();
        Connection con = db.conexionDB();
        con.setAutoCommit(false);
        Statement st = con.createStatement();
        String consulta = "INSERT INTO paciente VALUES(DEFAULT, '"+pac.getEdad()+"', '"+pac.getSexo()+"', '"+pac.getNombre()+"', '"+pac.getApellido1()+"', '"+pac.getApellido2()+"', "
                + "'"+pac.getFechaNacimiento()+"', '"+pac.getDni()+"', '"+pac.getDireccion()+"', '"+pac.getReferencias()+"', '"+pac.getPoblacion()+"', '"+pac.getcPostal()+"', "
                + "'"+pac.getFijo()+"', '"+pac.getMobil()+"', '"+pac.getEmail()+"', '')";
        st.executeUpdate(consulta);
        // INSERTAR EN DEPENDENCIA Y INFORME SOCIAL
        // INSERTAR FICHA SANITARIA
        // INSERTAR SERVICIO DE TELEASISTENCIA
    }
    
    
    public void updatePaciente(Paciente pac, long idPaciente) throws SQLException {
        ConexionDB db = ConexionDB.getInstance();
        Connection con = db.conexionDB();
        Statement st = con.createStatement();
        String consulta = "UPDATE paciente SET edad='"+pac.getEdad()+"', sexo='"+pac.getSexo()+"', nombre'"+pac.getNombre()+"', apellido1='"+pac.getApellido1()+"', apellido2'"+pac.getApellido2()+"', "
                + "fecha_nacimiento='"+pac.getFechaNacimiento()+"', dni='"+pac.getDni()+"', direccion='"+pac.getDireccion()+"', referencias='"+pac.getReferencias()+"', "
                + "poblacion='"+pac.getPoblacion()+"', c_postal='"+pac.getcPostal()+"', tlf_fijo='"+pac.getFijo()+"', tlf_mobil='"+pac.getMobil()+"', email='"+pac.getEmail()+"' "
                + "WHERE id_paciente = "+idPaciente;
        st.executeUpdate(consulta);
    }
    
    public boolean obtenerDniContratador(String dni) throws SQLException {
        ConexionDB db = ConexionDB.getInstance();
        Connection con = db.conexionDB();
        Statement st = con.createStatement();
        ResultSet rs = null;
        String consulta = "Select * from servicioteleasistencia where dni = '"+dni+"'";
        rs = st.executeQuery(consulta);
        if (rs.first()) {
            if (rs.getString("dniContratador") == null || rs.getString("dniContratador").equals("")) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }
                /*
    public static void insertarPaciente(String nombre, String apellido1)
            throws SQLException {
        ConexionDB db = ConexionDB.getInstance();
        Connection con = db.conexionDB();
        PreparedStatement stmt = con.prepareStatement(INSERT_EMPLOYEE_QUERY);
        stmt.setString("Omar",nombre);
        stmt.setString("Ruiz",apellido1);
 
        stmt.executeUpdate();
 
        System.out.println("Employee Data inserted successfully for ID=" + id);
        stmt.close();
    
    web: http://www.journaldev.com/2483/jdbc-transaction-management-and-savepoint-example-tutorial
    }
    */
}