/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Timber.DAO;

import Timber.Modelo.Trabajador;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Alba
 */
public class TrabajadorDAO {

    public TrabajadorDAO() {

    }

    /* Método que lista todos los trabajadores */
    public List<Trabajador> obtenerTrabajadores() throws SQLException {
        List<Trabajador> trabajadores = new ArrayList<Trabajador>();

        ConexionDB db = ConexionDB.getInstance();
        Connection con = db.conexionDB();
        Statement st = con.createStatement();
        ResultSet rs = null;
        String Consulta = "Select * from empleado";
        rs = st.executeQuery(Consulta);

        while (rs.next()) {
            Trabajador emp = new Trabajador();
            emp.setIdEmpleado(rs.getLong("id_empleado"));
            emp.setDni(rs.getString("dni"));
            emp.setNombre(rs.getString("nombre"));
            emp.setApellido1(rs.getString("apellido1"));
            emp.setApellido2(rs.getString("apellido2"));
            emp.setFechaNacimiento(rs.getDate("fecha_nacimiento").toString());
            emp.setEsAdmin(rs.getString("esAdmin"));
            emp.setUsuario(rs.getString("usuario"));
            emp.setContrasenya(rs.getString("contrasenya"));
            trabajadores.add(emp);
        }
        return trabajadores;
    }

    public Trabajador obtenerTrabajador(String var, boolean isDni) throws SQLException {
        ConexionDB db = ConexionDB.getInstance();
        Connection con = db.conexionDB();
        Statement st = con.createStatement();
        ResultSet rs = null;
        String consulta = "";
        if (!isDni) {
            consulta = "Select * from empleado where usuario = '" + var + "'";
        } else {
            consulta = "Select * from empleado where dni = '" + var + "'";
        }
        rs = st.executeQuery(consulta);

        Trabajador emp = null;
        if (rs.first()) {
            emp = new Trabajador();
            emp.setIdEmpleado(rs.getLong("id_empleado"));
            emp.setDni(rs.getString("dni"));
            emp.setNombre(rs.getString("nombre"));
            emp.setApellido1(rs.getString("apellido1"));
            emp.setApellido2(rs.getString("apellido2"));
            emp.setFechaNacimiento(rs.getDate("fecha_nacimiento").toString());
            emp.setEsAdmin(rs.getString("esAdmin"));
            emp.setUsuario(rs.getString("usuario"));
            emp.setContrasenya(rs.getString("contrasenya"));
        }
        return emp;
    }

    public boolean insertarTrabajador(Trabajador trab) throws SQLException {
        ConexionDB db = ConexionDB.getInstance();
        Connection con = db.conexionDB();
        Statement st = con.createStatement();
        Trabajador aux = obtenerTrabajador(trab.getDni(), true);
        if (aux == null) {
            aux = obtenerTrabajador(trab.getUsuario(), false);
            if (aux == null) {
                String Consulta = "INSERT INTO empleado(`dni`,`nombre`,`apellido1`, `apellido2`, `fecha_nacimiento`,`esAdmin`,`usuario`,`contrasenya`) VALUES ('" + trab.getDni() + "', '" + trab.getNombre() + "', "
                        + "'" + trab.getApellido1() + "', '" + trab.getApellido2() + "', STR_TO_DATE('" + trab.getFechaNacimiento() + "', '%Y-%m-%d'), '" + trab.getEsAdmin() + "', '" + trab.getUsuario() + "', '" + trab.getContrasenya() + "')";
                st.executeUpdate(Consulta);
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    public boolean updateTrabajador(Trabajador trab, long idEmpleado) throws SQLException {
        ConexionDB db = ConexionDB.getInstance();
        Connection con = db.conexionDB();
        Statement st = con.createStatement();
        String consulta = "UPDATE empleado SET dni = '" + trab.getDni() + "', nombre = '" + trab.getNombre() + "', apellido1 = '" + trab.getApellido1() + "', apellido2 = '" + trab.getApellido2() + "', fecha_nacimiento = STR_TO_DATE('" + trab.getFechaNacimiento() + "', '%Y-%m-%d'), usuario = '"+trab.getUsuario()+"', contrasenya = '"+trab.getContrasenya()+"', esAdmin = '" + trab.getEsAdmin() + "' WHERE id_empleado = " + idEmpleado;
        boolean res =  (st.executeUpdate(consulta)>=1);
        return res;
    }

    public void borrarTrabajador(Trabajador trabajador) throws SQLException {
        ConexionDB db = ConexionDB.getInstance();
        Connection con = db.conexionDB();
        Statement st = con.createStatement();
        String Consulta = "DELETE FROM empleado WHERE dni = '" + trabajador.getDni() + "'";
        st.executeUpdate(Consulta);
    }

}
