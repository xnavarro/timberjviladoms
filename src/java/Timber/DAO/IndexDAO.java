/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Timber.DAO;

import java.sql.Connection;
import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Alba
 */
public class IndexDAO {

    public boolean Autenticacion(String user, String pass) throws SQLException {
        ConexionDB db = ConexionDB.getInstance();
        Connection con = db.conexionDB();
        Statement st = con.createStatement();
        ResultSet rs = null;
        String Consulta = "Select * from empleado";
        if (user == null || user.equals("") || pass == null || pass.equals("")) {
            return false;
        }
        rs = st.executeQuery(Consulta);

        while (rs.next()) {
            if (user.equals(rs.getString("usuario")) && pass.equals(rs.getString("contrasenya"))) {
                return true;
            }

        }
        return false;
    }

    public boolean esAdministrador(String user, String pass) throws SQLException {
        ConexionDB db = ConexionDB.getInstance();
        Connection con = db.conexionDB();
        Statement st = con.createStatement();
        ResultSet rs = null;

        String Consulta = "Select * from empleado";
        rs = st.executeQuery(Consulta);

        while (rs.next()) {
            if (user.equals(rs.getString("usuario")) && pass.equals(rs.getString("contrasenya"))
                    && rs.getString("esAdmin").equals("SI")) {
                return true;
            }
        }

        return false;
    }
}
