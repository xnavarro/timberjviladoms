package Timber.DAO;

import java.io.File;
import java.sql.Connection;
import java.sql.Date;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


//import com.yayotech.vistas.AñadirContacto;
//import com.yayotech.vistas.Borrar;
//import com.yayotech.vistas.BuscarModificar;
//import com.yayotech.vistas.Contactos;
//import com.yayotech.vistas.Opciones;
//import com.yayotech.vistas.Pestañas;
//import com.yayotech.vistas.PestañasModificar;
//import com.yayotech.vistas.Vista;

/**
 * Clase modelo que contiene los mñtodos principales para todas las pantallas de la interfaz.
 * @author Andrei Iacob
 * @version 19/05/2015
 */
public class ConexionDB_bk {
        
	/**
	 * Variable String con el url que hace falta para conectarse a una base de datos en cuestiñn.
	 */
	private String url = "jdbc:mysql://localhost:3306/Timber";
	/**
	 * Variable Connection con la que se harñn las conexiones y desconexiones de la base de datos.
	 */
	private Connection con;
	/**
	 * Variable PreparedStatement con la que se harñn sentencias SQL en la base de datos.
	 */
	private PreparedStatement stm;
	/**
	 * Variable Statement con la que se harñn sentencias SQL en la base de datos, sobretodo bñsquedas.
	 */
	private Statement statement;
	/**
	 * Variable Pestañas para poder trabajar con la interfaz e introducir datos en la base de datos a travñs de ella.
	 */
//	private Pestañas p;
//	/**
//	 * Variable Borrar para poder trabajar con la interfaz y dar de baja pacientes a travñs de ella.
//	 */
//	private Borrar b;
//	/**
//	 * Variable Opciones que controla algunas configuraciones de la aplicaciñn.
//	 */
//	private Opciones o;
	
//	private AñadirContacto ac;
//	private Contactos contacto;
//	private Vista v;
//	private BuscarModificar bm;
//	private PestañasModificar pm;
	
//	public ConexionDB() throws ClassNotFoundException{
//            
//	}
	
	/**
	 * Mñtodo que se conecta a la base de datos.	
	 * @param usuari es el identificador para conectarse a la base de datos
	 * @param contrasena es la contraseña del usuario para poder acceder a la base de datos.
	 * @return devuelve true en caso de que los datos sean vñlidos para la conexiñn, en caso contrario no se conecta.
	 */
	public Connection conectarDB(String usuari, String contrasena) {
		try {
			System.out.println(url);
			return con = DriverManager.getConnection(url, usuari, contrasena);
//			System.out.println("Se ha conectado a la base de datos.");
//			return true;
		} catch (SQLException ex) {
			System.out.println(ex.getSQLState());
                        return null;
//			return false;
		}
	}
	
	/**
	 * Método que comprueba si la conexiñn a la base de datos esta abierta y la cierra.
	 */
	public void cerrarDB(){
		try {
			if (con != null && !con.isClosed()) {
				con.close();
				System.out.println("La base de datos se ha cerrado correctamente.");
			}
		} catch (SQLException ex) {
			System.out.println("Error: " + ex.getSQLState());
		}
	}

	/**
	 * Método que recibe los datos del paciente desde la interfaz y los inserta en una tabla de la base de datos.
	 * @param edat Valor de la edad del paciente.
	 * @param sexe Sexo del paciente.
	 * @param nom Nombre del paciente.
	 * @param cognoms Apellidos del paciente.
	 * @param any Año de nacimiento del paciente.
	 * @param mes Mes de nacimiento del paciente.
	 * @param dia Dia de nacimiento del paciente.
	 * @param DNI DNI del paciente.
	 * @param adreca Direccion del paciente.
	 * @param referencies Referencias de como llegar a la casa del paciente.
	 * @param poblacio Poblaciñn del paciente.
	 * @param cPostal Cñdigo postal del paciente.
	 * @param telefon1 Telñfono personal del paciente.
	 * @param telefon2 Telñfono personal 2 del paciente.
	 * @param email Email del paciente.
	 */
	public void insertarPacient(int edat,String sexe, String nom, String cognom1, String cognom2,
			int any, int mes, int dia, String DNI, String adreca, String referencies,
			String poblacio, String cPostal, String telefon1, String telefon2,
			String email) {
		try {
			any = any -1800;
			Date d = new Date(any, mes, dia);
			String sentencia = "INSERT INTO paciente(edad,sexo,nombre,apellido1,apellido2,fecha_nacimiento,dni,direccion,referencias,poblacion,c_postal,tlf_fijo,tlf_mobil,email) Values("+edat+",'"+sexe+"','"+nom+"','"+cognom1+"','"+cognom2+"','"+d+"','"+DNI+"','"+adreca+"','"+referencies+"','"+poblacio+"','"+cPostal+"','"+telefon1+"','"+telefon2+"','"+email+"')";
			stm = con.prepareStatement(sentencia);
			stm.executeUpdate();
			System.out.println("Se ha insertado el registro 'Pacient'.");
		} catch (SQLException ex) {
			System.out.println("Error: " + ex.getSQLState() + ex.getLocalizedMessage());
		} finally {
			try {
				if (stm != null && !stm.isClosed()) {
					stm.close();
				}
			} catch (SQLException ex) {
				System.out.println(ex.getSQLState());
			}
		}
	}
	
	/**
	 * Mñtodo que introduce un contacto para nuestro paciente en una tabla de la base de datos. 
	 * @param codi_pacient Codigo del paciente para introducir un contacto enlazado con este.
	 * @param nom_cognoms Nombre y apellidos del contacto.
	 * @param sexe Sexo del contacto.
	 * @param relacio_pacient Relaciñn del contacto con el paciente.
	 * @param poblacio Poblaciñn del contacto.
	 * @param c_postal Cñdigo postal del contacto.
	 * @param adreca Direcciñn del contacto-
 	 * @param telefon Telñfono del contacto.
	 * @param domicili Domicilio del contacto.
	 * @param feina Afirmaciñn o negaciñn sobre si el contacto tiene trabajo.
	 * @param horari_feina Horario en el que trabaja.
	 * @param tel_mobil Telñfono personal del contacto. 
	 * @param email Email del contacto.
	 * @param claus Afirmaciñn o negaciñn sobre si el contacto posee las llaves del domicilio del paciente.
	 */
	public void insertarContacte(String codi_pacient, String nom_cognoms,String sexe,String relacio_pacient,String poblacio,String c_postal,String adreca,String telefon,String domicili,String feina,String horari_feina, String tel_mobil, String email, String claus) {
		try {
			String sentencia = "INSERT INTO contacte(codi_pacient, nom_cognoms,sexe, relacio_pacient,poblacio,c_postal,adreca,telefon,domicili,feina,horari_feina,til_mobil,email,claus) Values("+codi_pacient+",'"+nom_cognoms+"','"+sexe+"','"+relacio_pacient+"','"+poblacio+"','"+c_postal+"','"+adreca+"','"+telefon+"','"+domicili+"','"+feina+"','"+horari_feina+"','"+tel_mobil+"','"+email+"','"+claus+"')";
			stm = con.prepareStatement(sentencia);
			stm.executeUpdate();
			System.out.println("Se ha insertado el registro 'Contacte'.");
		} catch (SQLException ex) {
			System.out.println("Error: " + ex.getSQLState() + ex.getLocalizedMessage());
		} finally {
			try {
				if (stm != null && !stm.isClosed()) {
					stm.close();
				}
			} catch (SQLException ex) {
				System.out.println(ex.getSQLState() + ex.getLocalizedMessage());
			}
		}
	}
		
	/**
	 * Mñtodo que introduce las dependencias del paciente en una tabla de la base de datos.
	 * @param codi_pacient Cñdigo del paciente para enlazar estas dependencias con ñl.
	 * @param deambulacio Deambulaciñn del paciente.
	 * @param grauDep Grado de dependencia del paciente.
	 * @param nivDep Nivel de dependencia del paciente.
	 * @param prestEconomica Afirmaciñn o negaciñn sobre si el paciente necesita prestaciñn econñmica.
	 */
	public void insertarDependenciaPacient(String codi_pacient, String deambulacio, String grauDep, String nivDep, String prestEconomica){
		try{
			String sentencia ="INSERT INTO dependencia Values("+codi_pacient+",'"+deambulacio+"','"+grauDep+"','"+nivDep+"','"+prestEconomica+"')";
			stm = con.prepareStatement(sentencia);
			stm.executeUpdate();
			System.out.println("Se ha insertado el registro 'Depñndencia'.");
		}catch(SQLException ex){
			ex.printStackTrace();
			System.out.println(ex.getSQLState() + ex.getLocalizedMessage());
		}finally {
			try {
				if (stm != null && !stm.isClosed()) {
					stm.close();
				}
			} catch (SQLException ex) {
				System.out.println(ex.getSQLState() + ex.getLocalizedMessage());
			}
		}
	}
	
	/**
	 * Mñtodo que inserta el informe social del paciente en una base de datos.
	 * @param codi_pacient Cñdigo del paciente para poder enlazar el informe social con ñl.
	 * @param convivencia Convivencia del paciente.
	 * @param horariSol Horario en el que el paciente se encuentra solo.
	 * @param relacioFamilia Relaciñn del paciente con su familia.
	 * @param visites Afirmaciñn o negaciñn sobre si el paciente quiere recibir visitas.
	 * @param compatibilitat Compatibilidad del paciente con las visitas.
	 */
	public void insertarInformeSocial(String codi_pacient, String convivencia, String horariSol, String relacioFamilia, String visites, String compatibilitat){
		try{
			String sentencia ="INSERT INTO informeSocial Values("+codi_pacient+",'"+convivencia+"','"+horariSol+"','"+relacioFamilia+"','"+visites+"','"+compatibilitat+"')";
			stm = con.prepareStatement(sentencia);
			stm.executeUpdate();
			System.out.println("Se ha insertado el registro 'Informe Social'.");
		}catch(SQLException ex){
			ex.printStackTrace();
			System.out.println(ex.getSQLState() + ex.getLocalizedMessage());
		}finally {
			try {
				if (stm != null && !stm.isClosed()) {
					stm.close();
				}
			} catch (SQLException ex) {
				System.out.println(ex.getSQLState() + ex.getLocalizedMessage());
			}
		}
	}
	
	/**
	 * Mñtodo que introduce la ficha sanitaria del paciente en una tabla de la base de datos.
	 * @param codi_pacient Cñdigo del paciente para poder enlazar la ficha sanitaria con ñl.
	 * @param estatura Estatura del paciente.
	 * @param pes Peso del paciente.
	 * @param estatActual Estado actual del paciente.
	 * @param patologia Patologias del paciente.
	 * @param alergies Alergias del paciente.
	 * @param sistemesAfectats Sistemas del cuerpo humano afectados del paciente.
	 * @param malalties Enfermedades del paciente.
	 * @param medicacio Medicaciñn del paciente.
	 * @param capacitatCognitiva Capacidad cognitiva del paciente.
	 * @param dificultats Cualquier tipo de dificultades que tenga el paciente.
	 * @param vacunes Vacunas que tenga o necesite el paciente.
	 * @param centreSanitari Centro sanitario al que acude el paciente.
	 */
	public void insertarFitxaSanitaria(String codi_pacient, float estatura, float pes, String estatActual, String patologia, String alergies, String sistemesAfectats, String malalties, String medicacio, String capacitatCognitiva, String dificultats, String vacunes, String centreSanitari){
		try{
			String sentencia ="INSERT INTO fitxaSanitaria VALUES("+codi_pacient+","+estatura+","+pes+",'"+estatActual+"','"+patologia+"','"+alergies+"','"+sistemesAfectats+"','"+malalties+"','"+medicacio+"','"+capacitatCognitiva+"','"+dificultats+"','"+vacunes+"','"+centreSanitari+"')";
			stm = con.prepareStatement(sentencia);
			stm.executeUpdate();
			System.out.println("Se ha insertado el registro 'Fitxa Sanitñria'.");
		}catch(SQLException ex){
			ex.printStackTrace();
			System.out.println(ex.getSQLState() + ex.getLocalizedMessage());
		}finally {
			try {
				if (stm != null && !stm.isClosed()) {
					stm.close();
				}
			} catch (SQLException ex) {
				System.out.println(ex.getSQLState() + ex.getLocalizedMessage());
			}
		}
	}
	
	/**
	 * Mñtodo que introduce los datos del momento del dado de alta en el servicio de teleasistencia en una tabla de la base de datos.
	 * @param codi_pacient Cñdigo del paciente para enlazarlo con la tabla de Servicio de Teleasistencia.
	 * @param anyAlta Año en el que se da de alta en el servicio de teleasistencia.
	 * @param mesAlta Mes en el que se da de alta en el servicio de teleasistencia.
	 * @param diaAlta Dia en el que se da de alta en el servicio de teleasistencia.
	 * @param quotesServei Cuotas del servicio de teleasistencia.
	 * @param dadesBanc Datos del banco del cual se extraerñ el dinero para cobrar el servicio de teleasistencia.
	 * @param nomContractant Nombre de la persona que contrata el servicio de teleasistencia.
	 * @param cognomsContractant Apellidos de la persona que contrata el servicio de teleasistencia.
	 * @param dniContractant DNI de la persona que contrata el servicio de teleasistencia.
	 * @param telefonContractant Telefono de la persona que contrata el servicio de teleasistencia.
	 * @param provinciaContractant Provñncia de la persona que contrata el servicio de teleasistencia.
	 * @param cpostalContractant Cñdigo postal de la persona que contrata el servicio de teleasistencia.
	 * @param serveisBasics Servicios contratados por el paciente.
	 * @param modalitats Modalidades contratadas por el paciente.
	 * @param prestacionsAdicionals Prestaciones adicionales contratadas para la ayuda del paciente.
	 */
	public void insertarServeiTeleasistencia(String codi_pacient, int anyAlta, int mesAlta, int diaAlta, String quotesServei, String dadesBanc, String nomContractant, String cognomsContractant, String dniContractant, String telefonContractant, String provinciaContractant, String cpostalContractant, String serveisBasics, String modalitats, String prestacionsAdicionals){
		try{
			Date d = new Date(anyAlta, mesAlta, diaAlta);
			String sentencia ="INSERT INTO serveiTeleasistencia VALUES("+codi_pacient+",'"+d+"','"+quotesServei+"','"+dadesBanc+"','"+nomContractant+"','"+cognomsContractant+"','"+dniContractant+"','"+telefonContractant+"','"+provinciaContractant+"','"+cpostalContractant+"','"+serveisBasics+"','"+modalitats+"','"+prestacionsAdicionals+"')";
			stm = con.prepareStatement(sentencia);
			stm.executeUpdate();
			System.out.println("Se ha insertado el registro 'Servei Teleasistñncia'.");
		}catch(SQLException ex){
			ex.printStackTrace();
			System.out.println(ex.getSQLState() + ex.getLocalizedMessage());
		}finally {
			try {
				if (stm != null && !stm.isClosed()) {
					stm.close();
				}
			} catch (SQLException ex) {
				System.out.println(ex.getSQLState() + ex.getLocalizedMessage());
			}
		}
	}
	
	/**
	 * Mñtodo que da de baja un paciente.
	 * @param dni Parñmetro por el cual se busca el paciente que se quiere dar de baja.
	 */
	public void darBaja(String dni){
		try{
			String sentencia = "UPDATE pacient SET alta = 'N' WHERE alta = 'S' AND DNI = '" +dni+"'";
			stm = con.prepareStatement(sentencia);
			stm.executeUpdate();
			System.out.println("Se ha dado de baja el paciente.");
		}catch(SQLException ex){
			ex.printStackTrace();
			System.out.println(ex.getSQLState() + ex.getLocalizedMessage());
		}finally {
			try {
				if (stm != null && !stm.isClosed()) {
					stm.close();
				}
			} catch (SQLException ex) {
				System.out.println(ex.getSQLState() + ex.getLocalizedMessage());
			}
		}
	}
	
	/**
	 * Mñtodo que busca nombre y apellido de un paciente.
	 * @param dni Parñmetro por el cual se buscan los  nombres y apellidos de la persona.
	 * @return Devuelve un array con los datos que se han buscado, en este caso nombre y apellidos.
	 */
	public ArrayList<String> buscarNombresPaciente(String dni){
		try{
			ArrayList<String> arr = new ArrayList<String>();
			String sentencia = "SELECT nom, cognoms from pacient where dni='" + dni + "'";
			statement = con.createStatement();
			ResultSet rs = statement.executeQuery(sentencia);
			while(rs.next()){
				String nom = rs.getString("nom");
				String cognoms = rs.getString("cognoms");
				arr.add(nom);
				arr.add(cognoms);
				System.out.println("Se ha encontrado a la persona en la base de datos.");
				return arr;
			}
		}catch(SQLException ex){
			ex.printStackTrace();
			System.out.println(ex.getSQLState() + ex.getLocalizedMessage());
		}finally {
			try {
				if (stm != null && !stm.isClosed()) {
					stm.close();
				}
			} catch (SQLException ex) {
				System.out.println(ex.getSQLState());
				ex.printStackTrace();
			}
		}
		return null;
	}
	
	/**
	 * Mñtodo que busca el cñdigo del paciente introducido en la base de datos.
	 * @param dni Parñmetro por el cual se busca a la persona.
	 * @return Devuelve el cñdigo del paciente introducido en la base de datos para poder enlazar las tablas entre ellas.
	 */
	public String buscarCodiPacient(String dni){
		try{
			String sentencia = "SELECT codi FROM pacient WHERE DNI = '" + dni + "'";
			statement = con.createStatement();
			ResultSet rs = statement.executeQuery(sentencia);
			while(rs.next()){
				String codi = rs.getString("codi");
				System.out.println("Se ha encontrado a la persona en la base de datos.");
				return codi;
			}
		}catch(SQLException ex){
			ex.printStackTrace();
			System.out.println(ex.getSQLState() + ex.getLocalizedMessage());
		}finally {
			try {
				if (stm != null && !stm.isClosed()) {
					stm.close();
				}
			} catch (SQLException ex) {
				System.out.println(ex.getSQLState());
				ex.printStackTrace();
			}
		}
		return null;
	}
	
	/**
	 * Mñtodo que busca el telñfono del paciente.
	 * @param dni Parñmetro por el cual se busca el telñfono del paciente.
	 * @return Devuelve el nñmero de telefono del paciente buscado con su DNI.
	 */
	public String buscarTelefonoPaciente(String dni){
		try{
			String sentencia = "SELECT tlf_contacte_1 FROM pacient where DNI  = '" + dni + "'";
			statement = con.createStatement();
			ResultSet rs = statement.executeQuery(sentencia);
			while(rs.next()){
				String tlf = rs.getString("tlf_contacte_1");
				System.out.println("Se ha encontrado el telefono en la base de datos.");
				return tlf;
			}
		}catch(SQLException ex){
			ex.printStackTrace();
			System.out.println(ex.getSQLState() + ex.getLocalizedMessage());
		}finally {
			try {
				if (stm != null && !stm.isClosed()) {
					stm.close();
				}
			} catch (SQLException ex) {
				System.out.println(ex.getSQLState());
				ex.printStackTrace();
			}
		}
		return null;
	}
	
	/**
	 * Mñtodo que busca todos los datos de los pacientes de la base de datos.
	 * @param dni Parñmetro que se introduce para buscar una persona en cuestiñn.
	 * @return Devuelve un array lleno con todos los datos del paciente para poder ser modificados.
	 */
	public ArrayList<String> buscarPacienteDNI(String dni){
		try{
			ArrayList<String> arr = new ArrayList<String>();
			String sentencia = "SELECT edat, sexe, nom, cognoms, data_naixament, DNI, adreca, referencies, poblacio, c_postal, tlf_contacte_1, tlf_contacte_2, email from pacient where dni='" + dni + "'";
			statement = con.createStatement();
			ResultSet rs = statement.executeQuery(sentencia);
			while(rs.next()){
				String edat = rs.getString("edat");
				String sexe = rs.getString("sexe");
				String nom = rs.getString("nom");
				String cognoms = rs.getString("cognoms");
				String data_naixement = rs.getString("data_naixament");
				String DNI = rs.getString("DNI");
				String adreca = rs.getString("adreca");
				String referencies = rs.getString("referencies");
				String poblacio = rs.getString("poblacio");
				String c_postal = rs.getString("c_postal");
				String tlf_contacte_1 = rs.getString("tlf_contacte_1");
				String tlf_contacte_2 = rs.getString("tlf_contacte_2");
				String email = rs.getString("email");
				arr.add(edat);
				arr.add(sexe);
				arr.add(nom);
				arr.add(cognoms);
				arr.add(data_naixement);
				arr.add(DNI);
				arr.add(adreca);
				arr.add(referencies);
				arr.add(poblacio);
				arr.add(c_postal);
				arr.add(tlf_contacte_1);
				arr.add(tlf_contacte_2);
				arr.add(email);
				System.out.println("Se ha encontrado a la persona en la base de datos.");
				return arr;
			}
		}catch(SQLException ex){
			ex.printStackTrace();
			System.out.println(ex.getSQLState() + ex.getLocalizedMessage());
		}finally {
			try {
				if (stm != null && !stm.isClosed()) {
					stm.close();
				}
			} catch (SQLException ex) {
				System.out.println(ex.getSQLState());
				ex.printStackTrace();
			}
		}
		return null;
	}
	
	/**
	 * Mñtodo que busca los datos del contacto de algñn paciente.
	 * @param codi Parñmetro que se introduce para buscar el contacto del paciente con ese codigo.
	 * @return Devuelve un array con los datos del contacto.
	 */
	public ArrayList<String> buscarContactoDNI(String codi){
		try{
			ArrayList<String> arr = new ArrayList<String>();
			String sentencia = "SELECT nom_cognoms, sexe, relacio_pacient, poblacio, c_postal, adreca, telefon, domicili, feina, horari_feina, til_mobil, email, claus from contacte where codi_pacient=" + codi;
			statement = con.createStatement();
			ResultSet rs = statement.executeQuery(sentencia);
			while(rs.next()){
				String nom_cognoms = rs.getString("nom_cognoms");
				String relacio_pacient = rs.getString("relacio_pacient");
				String poblacio = rs.getString("poblacio");
				String c_postal = rs.getString("c_postal");
				String adreca = rs.getString("adreca");
				String telefon = rs.getString("telefon");
				String domicili = rs.getString("domicili");
				String feina = rs.getString("feina");
				String horari_feina = rs.getString("horari_feina");
				String til_mobil = rs.getString("til_mobil");
				String email = rs.getString("email");
				String claus = rs.getString("claus");
				arr.add(nom_cognoms);
				arr.add(relacio_pacient);
				arr.add(poblacio);
				arr.add(c_postal);
				arr.add(adreca);
				arr.add(telefon);
				arr.add(domicili);
				arr.add(feina);
				arr.add(horari_feina);
				arr.add(til_mobil);
				arr.add(email);
				arr.add(claus);
				System.out.println("Se ha encontrado a la persona en la base de datos.");
				return arr;
			}
		}catch(SQLException ex){
			ex.printStackTrace();
			System.out.println(ex.getSQLState() + ex.getLocalizedMessage());
		}finally {
			try {
				if (stm != null && !stm.isClosed()) {
					stm.close();
				}
			} catch (SQLException ex) {
				System.out.println(ex.getSQLState());
				ex.printStackTrace();
			}
		}
		return null;
	}	
	
	/**
	 * Mñtodo que busca las dependencias de un paciente.
	 * @param codi Parñmetro por el cual se buscan las dependencias de ese paciente.
	 * @return Devuelve un array con todos los datos de las dependencias del paciente en cuestiñn.
	 */
	public ArrayList<String> buscarDependenciaDNI(String codi){
		try{
			ArrayList<String> arr = new ArrayList<String>();
			String sentencia = "SELECT deambulacio, grau_dependencia, nivell_dependencia, prestacio_economica from dependencia where codi_pacient=" + codi ;
			statement = con.createStatement();
			ResultSet rs = statement.executeQuery(sentencia);
			while(rs.next()){
				String deambulacio = rs.getString("deambulacio");
				String grau_dependencia = rs.getString("grau_dependencia");
				String nivell_dependencia = rs.getString("nivell_dependencia");
				String prestacio_economica = rs.getString("prestacio_economica");
				arr.add(deambulacio);
				arr.add(grau_dependencia);
				arr.add(nivell_dependencia);
				arr.add(prestacio_economica);
				System.out.println("Se ha encontrado a la persona en la base de datos.");
				return arr;
			}
		}catch(SQLException ex){
			ex.printStackTrace();
			System.out.println(ex.getSQLState() + ex.getLocalizedMessage());
		}finally {
			try {
				if (stm != null && !stm.isClosed()) {
					stm.close();
				}
			} catch (SQLException ex) {
				System.out.println(ex.getSQLState());
				ex.printStackTrace();
			}
		}
		return null;
	}
	
		
	public ArrayList<String> buscarInformeSocialDNI(String codi){
		try{
			ArrayList<String> arr = new ArrayList<String>();
			String sentencia = "SELECT convivencia, horariSol, relacioFamilia, visites, compatibilitat from informeSocial where codi_pacient=" + codi ;
			statement = con.createStatement();
			ResultSet rs = statement.executeQuery(sentencia);
			while(rs.next()){
				String convivencia = rs.getString("convivencia");
				String horariSol = rs.getString("horariSol");
				String relacioFamilia = rs.getString("relacioFamilia");
				String visites = rs.getString("visites");
				String compatibilitat = rs.getString("compatibilitat");
				arr.add(convivencia);
				arr.add(horariSol);
				arr.add(relacioFamilia);
				arr.add(visites);
				arr.add(compatibilitat);
				System.out.println("Se ha encontrado a la persona en la base de datos.");
				return arr;
			}
		}catch(SQLException ex){
			ex.printStackTrace();
			System.out.println(ex.getSQLState() + ex.getLocalizedMessage());
		}finally {
			try {
				if (stm != null && !stm.isClosed()) {
					stm.close();
				}
			} catch (SQLException ex) {
				System.out.println(ex.getSQLState());
				ex.printStackTrace();
			}
		}
		return null;
	}
	

	public ArrayList<String> buscarFitxaSanitariaDNI(String codi){
		try{
			ArrayList<String> arr = new ArrayList<String>();
			String sentencia = "SELECT estatura, pes, estatActual, patologia, alergies, sistemesAfectats, malalties, medicacio, capacitatCognitiva, dificultats, vacunes, centreSanitari from fitxaSanitaria where codi_pacient=" + codi ;
			statement = con.createStatement();
			ResultSet rs = statement.executeQuery(sentencia);
			while(rs.next()){
				String estatura = rs.getString("estatura");
				String pes = rs.getString("pes");
				String estatActual = rs.getString("estatActual");
				String patologia = rs.getString("patologia");
				String alergies = rs.getString("alergies");
				String sistemesAfectats = rs.getString("sistemesAfectats");
				String malalties = rs.getString("malalties");
				String medicacio = rs.getString("medicacio");
				String capacitatCognitiva = rs.getString("capacitatCognitiva");
				String dificultats = rs.getString("dificultats");
				String vacunes = rs.getString("vacunes");
				String centreSanitari = rs.getString("centreSanitari");
				arr.add(estatura);
				arr.add(pes);
				arr.add(estatActual);
				arr.add(patologia);
				arr.add(alergies);
				arr.add(sistemesAfectats);
				arr.add(malalties);
				arr.add(medicacio);
				arr.add(capacitatCognitiva);
				arr.add(dificultats);
				arr.add(vacunes);
				arr.add(centreSanitari);
				System.out.println("Se ha encontrado a la persona en la base de datos.");
				return arr;
			}
		}catch(SQLException ex){
			ex.printStackTrace();
			System.out.println(ex.getSQLState() + ex.getLocalizedMessage());
		}finally {
			try {
				if (stm != null && !stm.isClosed()) {
					stm.close();
				}
			} catch (SQLException ex) {
				System.out.println(ex.getSQLState());
				ex.printStackTrace();
			}
		}
		return null;
	}
	
	public ArrayList<String> buscarServeiTeleasitenciaDNI(String codi){
		try{
			ArrayList<String> arr = new ArrayList<String>();
			String sentencia = "SELECT nomContractant, cognomsContractant, dniContractant, telefonContractant, provinciaContractant, cpostalContractant, serveisBasics, modalitats, prestacionsAdicionals from serveiTeleasistencia where codi_pacient=" + codi ;
			statement = con.createStatement();
			ResultSet rs = statement.executeQuery(sentencia);
			while(rs.next()){
				String nomContractant = rs.getString("nomContractant");
				String cognomsContractant = rs.getString("cognomsContractant");
				String dniContractant = rs.getString("dniContractant");
				String telefonContractant = rs.getString("telefonContractant");
				String provinciaContractant = rs.getString("provinciaContractant");
				String cpostalContractant = rs.getString("cpostalContractant");
				String serveisBasics = rs.getString("serveisBasics");
				String modalitats = rs.getString("modalitats");
				String prestacionsAdicionals = rs.getString("prestacionsAdicionals");
				arr.add(nomContractant);
				arr.add(cognomsContractant);
				arr.add(dniContractant);
				arr.add(telefonContractant);
				arr.add(provinciaContractant);
				arr.add(cpostalContractant);
				arr.add(serveisBasics);
				arr.add(modalitats);
				arr.add(prestacionsAdicionals);
				System.out.println("Se ha encontrado a la persona en la base de datos.");
				return arr;
			}
		}catch(SQLException ex){
			ex.printStackTrace();
			System.out.println(ex.getSQLState() + ex.getLocalizedMessage());
		}finally {
			try {
				if (stm != null && !stm.isClosed()) {
					stm.close();
				}
			} catch (SQLException ex) {
				System.out.println(ex.getSQLState());
				ex.printStackTrace();
			}
		}
		return null;
	}
	
	public void updatePaciente(int edat,String sexe, String nom, String cognoms,
			int any, int mes, int dia, String DNI, String adreca, String referencies,
			String poblacio, String cPostal, String telefon1, String telefon2,
			String email, String codi){
		try {
			any = any-1000;
			Date d = new Date(any, mes, dia);
			String sentencia = "UPDATE pacient SET edat="+edat+",sexe='"+sexe+"',nom='"+nom+"',cognoms='"+cognoms+"',data_naixament='"+d+"',DNI='"+DNI+"',adreca='"+adreca+"',referencies='"+referencies+"',poblacio='"+poblacio+"',c_postal='"+cPostal+"',tlf_contacte_1='"+telefon1+"',tlf_contacte_2='"+telefon2+"', email='"+email+"' where codi ='" + codi +"'"; 
			stm = con.prepareStatement(sentencia);
			stm.executeUpdate();
			System.out.println("Se ha actualizado el registro 'Pacient'.");
		} catch (SQLException ex) {
			System.out.println("Error: " + ex.getSQLState() + ex.getLocalizedMessage());
		} finally {
			try {
				if (stm != null && !stm.isClosed()) {
					stm.close();
				}
			} catch (SQLException ex) {
				System.out.println(ex.getSQLState());
			}
		}
	}
	
	public void updateContacto(String nom_cognoms,String sexe,String relacio_pacient,String poblacio,String c_postal,String adreca,String telefon,String domicili,String feina,String horari_feina, String tel_mobil, String email, String claus, String codi){
		try {
			String sentencia = "UPDATE contacte SET nom_cognoms='"+nom_cognoms+"',sexe='"+sexe+"',relacio_pacient='"+relacio_pacient+"',poblacio='"+poblacio+"',c_postal='"+c_postal+"',adreca='"+adreca+"',adreca='"+adreca+"',telefon='"+telefon+"',domicili='"+domicili+"',feina='"+feina+"',horari_feina='"+horari_feina+"',til_mobil='"+tel_mobil+"', email='"+email+"', claus='"+claus+"' where codi_pacient ='" + codi +"'";  
			stm = con.prepareStatement(sentencia);
			stm.executeUpdate();
			System.out.println("Se ha actualizado el registro 'Contacte'.");
		} catch (SQLException ex) {
			System.out.println("Error: " + ex.getSQLState() + ex.getLocalizedMessage());
		} finally {
			try {
				if (stm != null && !stm.isClosed()) {
					stm.close();
				}
			} catch (SQLException ex) {
				System.out.println(ex.getSQLState());
			}
		}
	}
	
	public void updateDependencia(String deambulacio, String grauDep, String nivDep, String prestEconomica, String codi){
		try {
			String sentencia = "UPDATE dependencia SET deambulacio='"+deambulacio+"',grauDep='"+grauDep+"',nivDep='"+nivDep+"',prestEconomica='"+prestEconomica+"' where codi_pacient ='" + codi +"'";  
			stm = con.prepareStatement(sentencia);
			stm.executeUpdate();
			System.out.println("Se ha actualizado el registro 'Dependencia'.");
		} catch (SQLException ex) {
			System.out.println("Error: " + ex.getSQLState() + ex.getLocalizedMessage());
		} finally {
			try {
				if (stm != null && !stm.isClosed()) {
					stm.close();
				}
			} catch (SQLException ex) {
				System.out.println(ex.getSQLState());
			}
		}
	}
	
	public void updateInformeSocial(String convivencia, String horariSol, String relacioFamilia, String visites, String compatibilitat, String codi){
		try {
			String sentencia = "UPDATE informeSocial SET convivencia='"+convivencia+"',horariSol='"+horariSol+"',relacioFamilia='"+relacioFamilia+"',visites='"+visites+"', compatibilitat='"+compatibilitat+"' where codi_pacient ='" + codi +"'"; 
			stm = con.prepareStatement(sentencia);
			stm.executeUpdate();
			System.out.println("Se ha actualizado el registro 'Dependencia'.");
		} catch (SQLException ex) {
			System.out.println("Error: " + ex.getSQLState() + ex.getLocalizedMessage());
		} finally {
			try {
				if (stm != null && !stm.isClosed()) {
					stm.close();
				}
			} catch (SQLException ex) {
				System.out.println(ex.getSQLState());
			}
		}
	}

		public void updateFitxaSanitaria(float estatura, float pes, String estatActual, String patologia, String alergies, String sistemesAfectats, String malalties, String medicacio, String capacitatCognitiva, String dificultats, String vacunes, String centreSanitari, String codi){
			try {
				String sentencia = "UPDATE fitxaSanitaria SET estatura="+estatura+",pes="+pes+",estatActual='"+estatActual+"',patologia='"+patologia+"',alergies='"+alergies+"',sistemesAfectats='"+sistemesAfectats+"',malalties='"+malalties+"',medicacio='"+medicacio+"',capacitatCognitiva='"+capacitatCognitiva+"',dificultats='"+dificultats+"',vacunes='"+vacunes+"',centreSanitari='"+centreSanitari+"' where codi_pacient ='" + codi +"'"; 
				stm = con.prepareStatement(sentencia);
				stm.executeUpdate();
				System.out.println("Se ha actualizado el registro 'FitxaSanitaria'.");
			} catch (SQLException ex) {
				System.out.println("Error: " + ex.getSQLState() + ex.getLocalizedMessage());
			} finally {
				try {
					if (stm != null && !stm.isClosed()) {
						stm.close();
					}
				} catch (SQLException ex) {
					System.out.println(ex.getSQLState());
				}
			}
		}
	
		public void updateServeiTeleasistencia(int anyAlta, int mesAlta, int diaAlta, String quotesServei, String dadesBanc, String nomContractant, String cognomsContractant, String dniContractant, String telefonContractant, String provinciaContractant, String cpostalContractant, String serveisBasics, String modalitats, String prestacionsAdicionals, String codi){
			try {
				Date d = new Date(anyAlta, mesAlta, diaAlta);
				String sentencia = "UPDATE serveiTeleasistencia SET dataAlta='"+d+"',quotesServei='"+quotesServei+"',dadesBanc='"+dadesBanc+"',nomContractant='"+nomContractant+"',cognomsContractant='"+cognomsContractant+"',dniContractant='"+dniContractant+"',telefonContractant='"+telefonContractant+"',provinciaContractant='"+provinciaContractant+"',cpostalContractant='"+cpostalContractant+"',serveisBasics='"+serveisBasics+"',modalitats='"+modalitats+"',prestacionsAdicionals='"+prestacionsAdicionals+"' where codi_pacient ='" + codi +"'";  
				stm = con.prepareStatement(sentencia);
				stm.executeUpdate();
				System.out.println("Se ha actualizado el registro 'ServeiTeleasistencia'.");
			} catch (SQLException ex) {
				System.out.println("Error: " + ex.getSQLState() + ex.getLocalizedMessage());
			} finally {
				try {
					if (stm != null && !stm.isClosed()) {
						stm.close();
					}
				} catch (SQLException ex) {
					System.out.println(ex.getSQLState());
				}
			}
		}
	
	
	/**
	 * Mñtodo que busca los datos del paciente en la base de datos.
	 * @param nom2 String con el nombre del paciente del cual se quieren buscar los datos.
	 * @param cognom2 String con los apellidos del paciente del cual se quieren buscar los datos.
	 * @return Devuelve un array con los datos del paciente buscado.
	 */
	public ArrayList<String> buscarPacienteNomCognom(String nom2, String cognom2){
		try{
			ArrayList<String> arr = new ArrayList<String>();
			String sentencia = "SELECT edat, sexe, nom, cognoms, data_naixement, DNI, adreca, referencies, poblacio, c_postal, tlf_contacte_1, tlf_contacte_2, email from pacient where nom='" + nom2 + "' AND cognom ='"+ cognom2 +"'";
			
			statement = con.createStatement();
			ResultSet rs = statement.executeQuery(sentencia);
			while(rs.next()){
				String edat = rs.getString("edat");
				String sexe = rs.getString("sexe");
				String nom = rs.getString("nom");
				String cognoms = rs.getString("cognoms");
				String data_naixement = rs.getString("data_naixement");
				String DNI = rs.getString("DNI");
				String adreca = rs.getString("adreca");
				String referencies = rs.getString("referencies");
				String poblacio = rs.getString("poblacio");
				String c_postal = rs.getString("c_postal");
				String tlf_contacte_1 = rs.getString("tlf_contacte_1");
				String tlf_contacte_2 = rs.getString("tlf_contacte_2");
				String email = rs.getString("email");
				arr.add(edat);
				arr.add(sexe);
				arr.add(nom);
				arr.add(cognoms);
				arr.add(data_naixement);
				arr.add(DNI);
				arr.add(adreca);
				arr.add(referencies);
				arr.add(poblacio);
				arr.add(c_postal);
				arr.add(tlf_contacte_1);
				arr.add(tlf_contacte_2);
				arr.add(email);
				System.out.println("Se ha encontrado a la persona en la base de datos.");
				return arr;
			}
		}catch(SQLException ex){
			ex.printStackTrace();
			System.out.println(ex.getSQLState() + ex.getLocalizedMessage());
		}finally {
			try {
				if (stm != null && !stm.isClosed()) {
					stm.close();
				}
			} catch (SQLException ex) {
				System.out.println(ex.getSQLState());
				ex.printStackTrace();
			}
		}
		return null;
	}
	
	/**
	 * Mñtodo que busca el contacto de algñn paciente por el nombre de este paciente.
	 * @param nom2 String con el nombre del paciente del cual se quiere buscar el contacto.
	 * @param cognom2 String con los apellidos del paciente del cual se quiere buscar el contacto.
	 * @return Devuelve un array con los datos del contacto del paciente.
	 */
	public ArrayList<String> buscarContactoNomCognom(String nom2,String cognom2){
		try{
			ArrayList<String> arr = new ArrayList<String>();
			String sentencia = "SELECT nom_cognoms, sexe, relacio_pacient, poblacio, c_postal, adreca, telefon, domicili, feina, horari_feina, til_mobil, email, claus from contacte where nom_cognoms='" + nom2 +" "+ cognom2 +"'";
			statement = con.createStatement();
			ResultSet rs = statement.executeQuery(sentencia);
			while(rs.next()){
				String nom_cognoms = rs.getString("nom_cognoms");
				String relacio_pacient = rs.getString("relacio_pacient");
				String poblacio = rs.getString("poblacio");
				String c_postal = rs.getString("c_postal");
				String adreca = rs.getString("adreca");
				String telefon = rs.getString("telefon");
				String domicili = rs.getString("domicili");
				String feina = rs.getString("feina");
				String horari_feina = rs.getString("horari_feina");
				String til_mobil = rs.getString("til_mobil");
				String email = rs.getString("email");
				String claus = rs.getString("claus");
				arr.add(nom_cognoms);
				arr.add(relacio_pacient);
				arr.add(poblacio);
				arr.add(c_postal);
				arr.add(adreca);
				arr.add(telefon);
				arr.add(domicili);
				arr.add(feina);
				arr.add(horari_feina);
				arr.add(til_mobil);
				arr.add(email);
				arr.add(claus);
				System.out.println("Se ha encontrado a la persona en la base de datos.");
				return arr;
			}
		}catch(SQLException ex){
			ex.printStackTrace();
			System.out.println(ex.getSQLState() + ex.getLocalizedMessage());
		}finally {
			try {
				if (stm != null && !stm.isClosed()) {
					stm.close();
				}
			} catch (SQLException ex) {
				System.out.println(ex.getSQLState());
				ex.printStackTrace();
			}
		}
		return null;
	}	
	
	/**
	 * Mñtodo que busca las dependencias del paciente por su cñdigo de identificaciñn de la base de datos.
	 * @param codi Parñmetro por el cual se busca al paciente.
	 * @return Devuelve un array con todos los datos de las dependencias del paciente con el cñdigo introducido.
	 */
	public ArrayList<String> buscarDependenciaNomCognom(int codi){
		try{
			ArrayList<String> arr = new ArrayList<String>();
			String sentencia = "SELECT deambulacio, grau_dependencia, nivell_dependencia, prestacio_economica from contacte where codi_pacient='" + codi + "'";
			statement = con.createStatement();
			ResultSet rs = statement.executeQuery(sentencia);
			while(rs.next()){
				String deambulacio = rs.getString("deambulacio");
				String grau_dependencia = rs.getString("grau_dependencia");
				String nivell_dependencia = rs.getString("nivell_dependencia");
				String prestacio_economica = rs.getString("prestacio_economica");
				arr.add(deambulacio);
				arr.add(grau_dependencia);
				arr.add(nivell_dependencia);
				arr.add(prestacio_economica);
				System.out.println("Se ha encontrado a la persona en la base de datos.");
				return arr;
			}
		}catch(SQLException ex){
			ex.printStackTrace();
			System.out.println(ex.getSQLState() + ex.getLocalizedMessage());
		}finally {
			try {
				if (stm != null && !stm.isClosed()) {
					stm.close();
				}
			} catch (SQLException ex) {
				System.out.println(ex.getSQLState());
				ex.printStackTrace();
			}
		}
		return null;
	}
	
	
//	public void buscarDNI(String dni){
//		try{
//			String sentencia = "SELECT DNI FROM pacient where DNI='"+dni+"'";
//			statement = con.createStatement();
//			ResultSet rs = statement.executeQuery(sentencia);
//			if(rs.next()){
//				ac.setVisible(false);
//				contacto.setVisible(true);
//				v.setEnabled(true);
//			} else {
//				JOptionPane.showMessageDialog(ac, "El DNI no existe.");
//			}
//		}catch(SQLException ex){
//			ex.printStackTrace();
//			System.out.println(ex.getSQLState() + ex.getLocalizedMessage());
//		}finally {
//			try {
//				if (stm != null && !stm.isClosed()) {
//					stm.close();
//				}
//			} catch (SQLException ex) {
//				System.out.println(ex.getSQLState());
//				ex.printStackTrace();
//			}
//		}
//	}
	
//	public void rellenar(String numero, ModeloMariaDB modelo, PestañasModificar pm){
//		this.pm = pm;
//		pm.setVisible(true);
//		ArrayList<String> array = new ArrayList<String>(); 
//		String codiPacient = modelo.buscarCodiPacient(numero);
//		
//		//BUSCAR DATOS PARA MODIFICAR PACIENTE
//		array = modelo.buscarPacienteDNI(numero);
//		pm.getTxtEdat().setText(array.get(0));
//		if(array.get(1).equalsIgnoreCase("Home")){
//			pm.getCboSexe().setSelectedIndex(1);
//		}else if(array.get(1).equalsIgnoreCase("Dona")){
//			pm.getCboSexe().setSelectedIndex(2);
//		}
//		pm.getTxtNom().setText(array.get(2));
//		pm.getTxtCognoms().setText(array.get(3));
//		//FALTA DATA NAIX
//		pm.getTxtDni().setText(array.get(5));
//		//FALTA ADRECA
//		pm.getTxtInstruccions().setText(array.get(7));
//		pm.getTxtPoblacio().setText(array.get(8));
//		pm.getTxtCodiPostal().setText(array.get(9));
//		pm.getTxtTelefon1().setText(array.get(10));
//		pm.getTxtTelefon2().setText(array.get(11));
//		pm.getTxtEmail().setText(array.get(12));
//		
//		
//		//BUSCAR DATOS PARA MODIFICAR CONTACTO PACIENTE
//		array = new ArrayList<String>(); 
//		array = modelo.buscarContactoDNI(codiPacient);
//		//POR SI DA NULL!!!!!!!!!!!!
//		//pm.getTxtNomContacte().setText(array.get(0));
//		pm.getTxtRelacio().setText(array.get(1));	
//		pm.getTxtPoblacioContacte().setText(array.get(2));
//		pm.getTxtCodiPostalContacte().setText(array.get(3));
//		pm.getTxtAdrecaContacte().setText(array.get(4));	
//		pm.getTxtTelefonContacte().setText(array.get(5));
//		pm.getTxtDomiciliContacte().setText(array.get(6));
//		if(array.get(7).equalsIgnoreCase("SI")){
//			pm.getCboFeina().setSelectedIndex(1);
//		}else if(array.get(7).equalsIgnoreCase("NO")){
//			pm.getCboFeina().setSelectedIndex(2);
//		}
//		
//		if(array.get(8).equalsIgnoreCase("MATñ")){
//			pm.getCboHorariFeina().setSelectedIndex(1);
//		}else if(array.get(8).equalsIgnoreCase("TARDA")){
//			pm.getCboHorariFeina().setSelectedIndex(2);
//		}else if(array.get(8).equalsIgnoreCase("NIT")){
//			pm.getCboHorariFeina().setSelectedIndex(3);
//		}
//		
//		pm.getTxtTelefonContacte().setText(array.get(9));
//		pm.getTxtEmailContacte().setText(array.get(10));
//		if(array.get(11).equalsIgnoreCase("SI")){
//			pm.getCboClaus().setSelectedIndex(1);
//		}else if(array.get(11).equalsIgnoreCase("NO")){
//			pm.getCboClaus().setSelectedIndex(2);
//		}
//		
//
//		//BUSCAR DATOS PARA MODIFICAR DEPENDENCIA PACIENTE
//		array = new ArrayList<String>(); 
//		array = modelo.buscarDependenciaDNI(codiPacient);
//		//FALTA DEAMBULACIO
//		if(array.get(1).equalsIgnoreCase("1")){
//			pm.getCboGrauDependencia().setSelectedIndex(1);
//		}else if(array.get(1).equalsIgnoreCase("2")){
//			pm.getCboGrauDependencia().setSelectedIndex(2);
//		}else if(array.get(1).equalsIgnoreCase("3")){
//			pm.getCboGrauDependencia().setSelectedIndex(3);
//		}
//		
//		if(array.get(2).equalsIgnoreCase("1")){
//			pm.getCboNivellDependencia().setSelectedIndex(1);
//		}else if(array.get(2).equalsIgnoreCase("2")){
//			pm.getCboNivellDependencia().setSelectedIndex(2);
//		}
//		
//		if(array.get(3).equalsIgnoreCase("SI")){
//			pm.getCboPrestacioEconomica().setSelectedIndex(1);
//		}else if(array.get(3).equalsIgnoreCase("NO")){
//			pm.getCboPrestacioEconomica().setSelectedIndex(2);
//		}
//		
//
//		//BUSCAR DATOS PARA MODIFICAR INFORME SOCIAL PACIENTE
//		array = new ArrayList<String>(); 
//		array = modelo.buscarInformeSocialDNI(codiPacient);
//		if(array.get(0).equalsIgnoreCase("Solo/a")){
//			pm.getCboSituacioConvivencial().setSelectedIndex(1);
//		}else if(array.get(0).equalsIgnoreCase("Acompañado/a")){
//			pm.getCboSituacioConvivencial().setSelectedIndex(2);
//		}
//		
//		if(array.get(1).equalsIgnoreCase("Mañanas")){
//			pm.getCboHoresSol().setSelectedIndex(1);
//		}else if(array.get(1).equalsIgnoreCase("Tardes")){
//			pm.getCboHoresSol().setSelectedIndex(2);
//		}else if(array.get(1).equalsIgnoreCase("Noches")){
//			pm.getCboHoresSol().setSelectedIndex(3);
//		}
//		
//		if(array.get(2).equalsIgnoreCase("Buena")){
//			pm.getCboRelacioFamilia().setSelectedIndex(1);
//		}else if(array.get(2).equalsIgnoreCase("Aceptable")){
//			pm.getCboRelacioFamilia().setSelectedIndex(2);
//		}else if(array.get(2).equalsIgnoreCase("Mala")){
//			pm.getCboRelacioFamilia().setSelectedIndex(3);
//		}else if(array.get(2).equalsIgnoreCase("Sin relaciñn")){
//			pm.getCboRelacioFamilia().setSelectedIndex(4);
//		}
//		
//		if(array.get(3).equalsIgnoreCase("Diñriamente")){
//			pm.getCboVisites().setSelectedIndex(1);
//		}else if(array.get(3).equalsIgnoreCase("Semanalmente")){
//			pm.getCboVisites().setSelectedIndex(2);
//		}else if(array.get(3).equalsIgnoreCase("Mensualmente")){
//			pm.getCboVisites().setSelectedIndex(3);
//		}else if(array.get(3).equalsIgnoreCase("Poco")){
//			pm.getCboVisites().setSelectedIndex(4);
//		}else if(array.get(3).equalsIgnoreCase("Nunca")){
//			pm.getCboVisites().setSelectedIndex(5);
//		}
//		//FALTA COMPATIBILITAT
//		
//		
//		//BUSCAR DATOS PARA MODIFICAR FICHA SANITARIA PACIENTE
//		array = new ArrayList<String>(); 
//		array = modelo.buscarFitxaSanitariaDNI(codiPacient);
//		pm.getTxtEstatura().setText(array.get(0));
//		pm.getTxtPes().setText(array.get(1));
//		if(array.get(2).equalsIgnoreCase("Buen estado")){
//			pm.getCboEstatActual().setSelectedIndex(1);
//		}else if(array.get(2).equalsIgnoreCase("Enfermo")){
//			pm.getCboEstatActual().setSelectedIndex(2);
//		}
//		pm.getTxtPatologia().setText(array.get(3));
//		pm.getTxtAlergies().setText(array.get(4));
//		//FALTAN SISTEMAS AFECTADOS CUERPO
//		pm.getTxtAreaMalalties().setText(array.get(6));
//		pm.getTxtAreaMedicacio().setText(array.get(7));
//		if(array.get(8).equalsIgnoreCase("Buena")){
//			pm.getCboCapacitatCognitiva().setSelectedIndex(1);
//		}else if(array.get(8).equalsIgnoreCase("Regular")){
//			pm.getCboCapacitatCognitiva().setSelectedIndex(2);
//		}else if(array.get(8).equalsIgnoreCase("Mala")){
//			pm.getCboCapacitatCognitiva().setSelectedIndex(3);
//		}
//		pm.getTxtDificultats().setText(array.get(9));
//		pm.getTxtVacunes().setText(array.get(10));
//		//FALTA CENTRO SANITARIO
//		
//		
//		array = new ArrayList<String>(); 
//		array = modelo.buscarServeiTeleasitenciaDNI(codiPacient);
//		//BUSCAR DATOS PARA MODIFICAR SERVICIO TELEASISTENCIA PACIENTE
//		pm.getTxtNomContractant().setText(array.get(0));
//		pm.getTxtCognomsContractant().setText(array.get(1));
//		pm.getTxtDNIContractant().setText(array.get(2));
//		pm.getTxtTelefonContractant().setText(array.get(3));
//		pm.getTxtProvinciaContractant().setText(array.get(4));
//		pm.getTxtCodiPostalContractant().setText(array.get(5));
//		//FALTA SERVEIS BASICS
//		//FALTA MODALITATS
//		//FALTA PRESTACIONSADICIONALS
//	}
	
	
	
//	public void buscarDNIModificar(String dni){
//		try{
//			String sentencia = "SELECT DNI FROM pacient where DNI='"+dni+"'";
//			statement = con.createStatement();
//			ResultSet rs = statement.executeQuery(sentencia);
//			if(rs.next()){
//				bm.setVisible(false);
//				pm.setVisible(true);
//				v.setEnabled(true);
//			} else {
//				JOptionPane.showMessageDialog(ac, "El DNI no existe.");
//			}
//		}catch(SQLException ex){
//			ex.printStackTrace();
//			System.out.println(ex.getSQLState() + ex.getLocalizedMessage());
//		}finally {
//			try {
//				if (stm != null && !stm.isClosed()) {
//					stm.close();
//				}
//			} catch (SQLException ex) {
//				System.out.println(ex.getSQLState());
//				ex.printStackTrace();
//			}
//		}
//	}
	
	/**
	 * Mñtodo que comprueba la validez de un documento de identidad, ya sea DNI o NIE.
	 * @param nif String con el nñmero del documento de identidad.
	 * @return Devuelve un valor cierto en caso de que el documento introducido sea vñlido y uno falso en caso de que no lo sea.
	 */
	public static boolean isNifNie(String nif){
		if (nif.toUpperCase().startsWith("X")||nif.toUpperCase().startsWith("Y")||nif.toUpperCase().startsWith("Z"))
		nif = nif.substring(1);
		 
		Pattern nifPattern =
		Pattern.compile("(\\d{1,8})([TRWAGMYFPDXBNJZSQVHLCKEtrwagmyfpdxbnjzsqvhlcke])");
		Matcher m = nifPattern.matcher(nif);
		if(m.matches()){
			String letra = m.group(2);
			String letras = "TRWAGMYFPDXBNJZSQVHLCKE";
			int dni = Integer.parseInt(m.group(1));
			dni = dni % 23;
			String reference = letras.substring(dni,dni+1);
			if (reference.equalsIgnoreCase(letra)){
				return true;
			}else{
				return false;
			}
		}else{
			return false;
		}
	}



	
//	public AñadirContacto getAc() {
//		return ac;
//	}
//
//	public void setAc(AñadirContacto ac) {
//		this.ac = ac;
//	}
//
//	public Contactos getContacto() {
//		return contacto;
//	}
//
//	public void setContacto(Contactos contacto) {
//		this.contacto = contacto;
//	}
//	
//	public Vista getV() {
//		return v;
//	}
//
//	public void setV(Vista v) {
//		this.v = v;
//	}
//
//	public BuscarModificar getBm() {
//		return bm;
//	}
//	
//	public void setBm(BuscarModificar bm) {
//		this.bm = bm;
//	}
//
//	public PestañasModificar getPm() {
//		return pm;
//	}
//
//	public void setPm(PestañasModificar pm) {
//		this.pm = pm;
//	}
}
