/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Timber.DAO;

import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Connection;
/**
 *
 * @author Alba
 */
public class ConexionDB {
    private static ConexionDB instance = null;
    public static final String USERNAME = "root";
    public static final String PASSWORD = "";
    public static final String HOST = "localhost";
    public static final String PORT = "3306";
    public static final String DATABASE = "Timber";
    public static final String CLASSNAME = "com.mysql.jdbc.Driver";
    public static final String URL = "jdbc:mysql://" + HOST + ":" + PORT + "/" + DATABASE;

    public java.sql.Connection con;
    
    private ConexionDB() {
        try {
            Class.forName(CLASSNAME);
        } catch (ClassNotFoundException ex) {
            ex.printStackTrace();
        }
    }
    
    public static ConexionDB getInstance(){
        if (instance == null){
            instance = new ConexionDB();
        }
        return instance;
    }
    
    public static Connection conexionDB() {
        Connection con = null;
        
        try {
            con = DriverManager.getConnection(URL,USERNAME,PASSWORD);
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return con;
    }
    
    	/**
	 * Método que comprueba si la conexión a la base de datos esta abierta y la cierra.
	*/
	/*public void cerrarDB(){
		try {
			if (con != null && !con.isClosed()) {
				con.close();
				System.out.println("La base de datos se ha cerrado correctamente.");
			}
		} catch (SQLException ex) {
			System.out.println("Error: " + ex.getSQLState());
		}
	}*/
   //public static void main(String[]args){
    //    ConexionDB con = new ConexionDB();
    //}
}
