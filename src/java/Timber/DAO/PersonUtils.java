/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Timber.DAO;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 *
 * @author Alba
 */
public class PersonUtils {

    public static boolean isNifNie(String nif) {
        if (nif.toUpperCase().startsWith("X") || nif.toUpperCase().startsWith("Y") || nif.toUpperCase().startsWith("Z")) {
            nif = nif.substring(1);
        }

        Pattern nifPattern = Pattern.compile("(\\d{1,8})([TRWAGMYFPDXBNJZSQVHLCKEtrwagmyfpdxbnjzsqvhlcke])");
        Matcher m = nifPattern.matcher(nif);
        if (m.matches()) {
            String letra = m.group(2);
            String letras = "TRWAGMYFPDXBNJZSQVHLCKE";
            int dni = Integer.parseInt(m.group(1));
            dni = dni % 23;
            String reference = letras.substring(dni, dni + 1);
            if (reference.equalsIgnoreCase(letra)) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }
    
    public static boolean validadorCampoDecimal(String numero) {
        boolean decimal = numero.matches("^\\d+(\\.\\d{1,2})?$");
        return decimal;
    }

    public static boolean validadorCampoTexto(String texto) {
        boolean soloTexto = texto.matches("([a-z]|[A-Z]|\\s)+");
        return soloTexto;
    }
    
    public static boolean validadorCampoNumerico(String numero) {
        try {
            Integer.parseInt(numero);
            return true;
        } catch (NumberFormatException ex) {
            return false;
        }
    }
    
    public static boolean validadorCampoEmail(String texto) {
        boolean soloTexto = texto.matches("^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$");
        return soloTexto;
    }
    
    public static boolean validadorCampoVacio(String texto) {
        if (texto != null && !texto.equals("")) {
            return false;
        }
        return true;
    }

}
