package Timber.Controladores;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintStream;
import java.util.Random;

import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.UnsupportedAudioFileException;
import javax.swing.JApplet;

import com.yayotech.vistas.Vista;

// TODO: Auto-generated Javadoc
/**
 * <b>Class implementada de Runnable que actua como un proceso para ejecutar el mñtodo de la class CMD "introducir" para introducir
 * comandos a un proceso externo.</b>
 * @author Sergio
 * @version 21/05/2015
 */
class IntroducirCMD implements Runnable {
	
	
	/**
	 * <i>Objeto que contiene los mñtodos y variables que se van a utilizar.</i>
	 */
	private CMD cmd;
	
	/**
	 * <u>Constructor para rellenar las variables que se van a necesitar para la funcionalidad de esta clase.</u>
	 * @param cmd Objeto que contiene la funcionalidad.
	 */
	public IntroducirCMD(CMD cmd){
	    this.cmd = cmd;
	}
	
	/**
	 * <u>Mñtodo sobreescrito de Runnable que se usa para crear un bucle y hacer que se pueda introducir todo el rato comandos dentro
	 * del proceso externo.</u>
	 */
	public void run(){
	    try {
	        while(!cmd.comando.equalsIgnoreCase("quit")){
	            Thread.sleep(1000);
                cmd.introducir();
	        }
	    } catch(InterruptedException e){
	        System.out.println("Error en run intro: " + e);
	    }
	}

}

/**
 * <b>Class CMD class que se usa como guia, con metodos sincronizados que hace ejecutar un mñtodo cuando termina el otro...</b>
 * @author Sergio
 * @version 21/05/2015
 */
class CMD {
	
	
	public boolean random = false;
	/**
	 * <i>Boolean para controlar cuando tiene que esperar un proceso a otro.</i>
	 */
	public boolean cmd = false;
	/**
	 * <i>Objeto para controlar el proceso externo.</i>
	 */
	private Process p;
	/**
	 * <i>Comando por defecto que tiene que ejecutar el mñtodo introducir, para mostrar que esta haciendo en cada momento el proceso.</i>
	 */
	public String comando = "calls", password;
	/**
	 * <i>Objecto Phone para controlar la interfaz grafica en todo momento.</i>
	 */
	private Vista vista;
	/**
	 * <i>Boolean para controlar cuando se tiene que mostrar cada ventana de la inerfaz grafica.</i>
	 */
	public boolean volver = true;
	/**
	 * <i>String donde guardamos el nñmero de telefono que nos esta llamando.</i>
	 */
	private String numero = "";
	/**
	 * <i>Boolean para controlar cuando se tiene que mostrar cada ventana del la interfaz grafica.</i>
	 */
	public boolean terminar = true;
	
	
	private Reproductor r;
	
		
	/**
	 * <u>Mñtodo contructor para rellenar las variables necesarias para el correcto funcionamiento del telefono.</u>
	 * @param p Objecto proceso en que vamos a ir leyendo y ejecutando cosas sobre el.
	 * @param vista Objecto phone con el que podremos controlar la interfaz.
	 */
	public CMD(Process p,Vista vista, Reproductor r){
	    this.p = p; 
	    this.vista = vista;
	    this.r = r;
	}
	
	/**
	 * <u>Mñtodo introducir, es sincronizado con el mñtodo mostrar y se utiliza para introducir comandos al proceso externo.</u>
	 */
	public synchronized void introducir(){ 
	    try{
	        while(!cmd){
	            wait();
	        }
	        this.cmd = false;
	        System.out.print(comando);
	        PrintStream o = new PrintStream(p.getOutputStream());
	        o.println(comando);
	        o.flush();
	        comando = "calls";
	    } catch (InterruptedException e) {
	        System.out.println("Error en wait: " + e);
	    } 
	}
	
	/**
	 * <u>Mñtodo mostrar, se sincroniza con el mñtodo introducir y se utiliza para mostrar la informaciñn que nos aporta el proceso
	 * externo.</u>
	 */
	public synchronized void mostrar(){
	    try {
	        InputStream i = p.getInputStream();
	        String aux = String.valueOf((char)i.read());
	        while(i.available() > 0){
	        	aux += String.valueOf((char)i.read());
	        }
	        System.out.println(aux);
	        if(volver){
	        	vista.getCall().setVisible(false);
	        	vista.getAns().setVisible(true);
	        	vista.invalidate();
	        } 
	        if(aux.contains("No active call")){
	        	
	        	volver = true;
	        	if(r.audioClipEntrante().isRunning() || r.audioClipLlamadas().isRunning()){
        			r.pararSonido();
        		}
	        }
        	if(aux.contains("Registration")){
        		comando = password;
        	}
        	if(aux.contains("Not answered")){
        		random = false;
	        	vista.getPm().setVisible(false);
        	}
        	if(aux.contains("Busy")){
        		random = false;
	        	vista.getPm().setVisible(false);

        	}
	        if(aux.contains("Id")){
	        	String [] aux1 = aux.split("\"");
	        	if(aux.contains("StreamsRunning")) {
	        		if(r.audioClipEntrante().isRunning() || r.audioClipLlamadas().isRunning()){
	        			r.pararSonido();
	        		}
	        		if(terminar){
	        			Thread.sleep(1000);
	        			terminar = false;
	        		} else if(aux1.length == 3){
	        			vista.setLabel("Hablando con ");
	        			vista.setLabel1(aux1[1]);
	        		} else {
	        			vista.setLabel("Hablando con ");
	        			vista.setLabel1(vista.getNumero());
	        		}
	        	} else if(terminar){
        			Thread.sleep(1000);
        			terminar = false;
        		} else if (aux.contains("OutgoingRinging")){
        			if(!r.audioClipLlamadas().isRunning()){
        				r.iniciarSonidoLlamar();        				
        			}
        			vista.setLabel("Llamando a ");
        			vista.setLabel1(vista.getNumero());
        			vista.getCall().setVisible(true);
        			vista.getAns().setVisible(false);
        			vista.invalidate();
        			volver = false;
	        	} else if(aux1.length == 3){
	        		if(!r.audioClipEntrante().isRunning()){
	        			r.iniciarSonidoEntrante();
	        		}
	        		vista.setLabel("Llamanda entrante");
	        		vista.setLabel1(aux1[1]);
	        		vista.getCall().setVisible(true);
	        		vista.getAns().setVisible(false);
	        		vista.invalidate();
        			volver = false;
	        		numero = aux1[1];
	        		if(!random){
	        			vista.getModelo().rellenar(numero, vista.getModelo(), vista.getPm());
	        			random = true;
	        		}
	        	}
        	}
	        this.cmd = true;
	        if(!comando.equalsIgnoreCase("quit")){
	            notify();
	        } 
	    } catch (IOException ex) {
	        ex.printStackTrace();
	    } catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
}

/**
 * <b>Class MostraCMD implementada de Runable que crea un bucle para poder introducir cosas dentro del proceso.</b>
 * @author Sergio
 * @version 21/05/2015
 */
class MostraCMD implements Runnable {
	
	/**
	 * <i>Objeto CMD con el que indicaremos cuando tiene que mostrar el contenido del proceso.</i>
	 */
	private CMD cmd;
	
	/**
	 * <u>Mñtodo contructor que se utilizarñ para rellenar las variables para el correcto funcionamiento de la clase.</u>
	 * @param cmd Objecto para darle funcionalidad el proceso externo.
	 */
	public MostraCMD(CMD cmd){
	    this.cmd = cmd;
	}
	
	/**
	 * <u>Mñtodo sobreescrito de Runnable, crea un bucle y lo que hace es indicarle al objeto CMD cuando tiene que mostrar el contenido
	 * del proceso externo.</u>
	 */
	public void run(){
	    try {
	    	while(!cmd.comando.equalsIgnoreCase("quit")){
	            Thread.sleep(1000);
                cmd.mostrar();
	        }
	    } catch(InterruptedException e){
	        System.out.println("Error en run mostrar: " + e);
	    }
	}
}

/**
 * <b>Class Reproductor que ejecuta la mñsica del telefono cuando sea pertinente.</b>
 * @author Sergio
 * @version 21/05/2015
 */
class Reproductor {

	/**
	 * <i>Objeto que guarda el tono de llamada entrante.</i>
	 */
	private AudioInputStream llamadas;
	/**
	 * <i>Object que guarda el tono de llamada que se utilizarñ al llamar.</i>
	 */
	private AudioInputStream llamar;
	/**
	 * <i>Objeto para iniciar el tono de llamada al llamar.</i>
	 */
	private Clip audioClipLlamadas;
	/**
	 * <i>Objecto para iniciar el tono de llamada entrante.</i>
	 */
	private Clip audioClipEntrante;
    	
	/**
	 * <u>Mñtodo constructor para iniciar las variables de los tonos de llamada y iniciar los clip para poder empezar a escuchar los
	 * tonos cuando sea necesario.</u>
	 * @param llamadas String con la ruta del tono de llamada Entrante.
	 * @param llamar String con la ruta del tono que se escucharñ al llamar.
	 */
	public Reproductor(){
		try {
			this.llamadas = AudioSystem.getAudioInputStream(new File(CONF.recogerSonidoEntrante()));
			this.llamar =  AudioSystem.getAudioInputStream(new File(CONF.recogerSonidoLlamar()));
			this.audioClipLlamadas = AudioSystem.getClip();
			this.audioClipEntrante = AudioSystem.getClip();
			this.audioClipLlamadas.open(this.llamar);
			this.audioClipEntrante.open(this.llamadas);
		} catch (UnsupportedAudioFileException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (LineUnavailableException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * <u>Mñtodo getter para recoger el clip del tono de llamar.</u>
	 * @return clip 
	 */
	public Clip audioClipLlamadas(){
		return audioClipLlamadas;
	}
	
	/**
	 * <u>Mñtodo getter para recoger el clip del tono entrante.</u>
	 * @return clip
	 */
	public Clip audioClipEntrante(){
		return audioClipEntrante;
	}
	
	/**
	 * <u>Mñtodo para iniciar el sonido de llamada entrante.</u>
	 */
	public void iniciarSonidoEntrante(){
		audioClipEntrante.setFramePosition(0);
		audioClipEntrante.loop(20);
	}
	
	/**
	 * <u>Mñtodo para iniciar el sonido del tono al llamar.</u>
	 */
	public void iniciarSonidoLlamar(){
		audioClipLlamadas.setFramePosition(0);
		audioClipLlamadas.loop(20);
	}
	
	/**
	 * <u>Mñtodo para parar el sonido que se esta escuchando en el momento.</u>
	 */
	public void pararSonido(){
		if(audioClipLlamadas.isRunning()){
			audioClipLlamadas.stop();
		}
		if(audioClipEntrante.isRunning()){
			audioClipEntrante.stop();
		}
	}	
	
	/**
	 * <u>Mñtodo para cambiar el Sonido al hacer una llamada.</u>
	 * @param sonido 
	 */
	public void cambiarSonidoLlamar(String sonido){
		try {
			this.llamar =  AudioSystem.getAudioInputStream(new File(sonido));
		} catch (UnsupportedAudioFileException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * <u>Mñtodo para cambiar el Sonido de la llamada entrante.</u>
	 * @param sonido
	 */
	public void cambiarSonidoEntrante(String sonido){
		try {
			this.llamadas = AudioSystem.getAudioInputStream(new File(sonido));
		} catch (UnsupportedAudioFileException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}

/**
 * <b>Class principal que crea todos los hilos y inicia el servicio que va a utilizar el telefono movil.</b>
 * @author Sergio
 * @version 21/05/2015
 */
public class Servicio {
    
    /**
     * <i>String con el que guardamos la ruta donde se encuentra el proceso que vamos a ejecutar.</i> 
     */
    private String iniciar = CONF.recogerProcess();
    //"C:" + File.separator + "Program Files" + File.separator + "Linphone" + File.separator + "bin" + File.separator +"linphonec.exe"
    /**
     * <i>Objeto cmd con el que enviaremos los camdos que va a ejecutar el proceso externo.</i>
     */
    private CMD cmd;
    
    /**
     * <i>Processo externo que queremos controlar.</i>
     */
    private Process p;
    
    /**
     * <i>Objecto de la interfaz grafica para poder controlar sus componentes</i>
     */
    private Vista vista;
    
    /**
     * <i>Objeto Reproductor que se utiliza para escuchar la musica cuando te llaman o llamas.</i>
     */
    private Reproductor r;
    
    /**
     * <u>Instancia del constructor para guardar todas las variables necesarias para el correcto funcionamiento de la clase.</u>
     *
     * @param iniciar String con la ruta donde se encuentra el proceso externo para el funcionamiento del telefono.
     * @param vista Objeto phone con el que contralemos la interfaz.
     * @param llamadas String con la ruta del tono de llamadas entrantes.
     * @param llamar String con la ruta del tono al hacer una llamada.
     */
    public Servicio(Vista vista){
    	this.vista = vista;
    	iniciarTelefono();
    }
    
    /**
     * <u>Mñtodo que inicia el proceso externo y los hilos para su correcto funcionamiento.</u>
     * @param llamadas Tono de llamadas entrantes.
     * @param llamar Tono al llamar.
     */
    public void iniciarTelefono(){
    	try {
            ProcessBuilder builder = new ProcessBuilder(iniciar);
            builder.redirectErrorStream(true);
            p = builder.start();
            r = new Reproductor();
            cmd = new CMD(p,vista, r);
            new Thread(new MostraCMD(cmd)).start();
            new Thread(new IntroducirCMD(cmd)).start();
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }
    
    public void unregister(){
    	cmd.comando = "unregister";
    }
    
    public void registrarse(String user, String password){
    	cmd.comando = "register sip:"+user+"@"+CONF.recogerIPTelf()+" sip:"+CONF.recogerIPTelf()+" "+password;
    	cmd.password = password;
    }
    
    /**
     * <u>Mñtodo para introducir un comando generico, sobre el proceso externo.</u>
     * @param comando Comando que se quiere introducir.
     */
    public void introducirComando(String comando){
    	cmd.comando = comando;
    }
    
    /**
     * <u>Comando para llamar a un nñmero determinado.</u>
     * @param telf Nñmero que se quiere llamar.
     */
    public void llamar(String telf){
    	cmd.comando = "call " + telf;
		
    }
    
    /**
     * <u>Mñtodo para responder las llamadas entrantes.</u>
     */
    public void responder(){
    	cmd.comando = "answer";
    	if(r.audioClipEntrante().isRunning()){
    		r.pararSonido();
    	}
    	
    }
    
    /**
     * <u>Mñtodo para colgar una llamada entrando o en ejecuciñn.</u>
     */
    public void colgar(){
    	cmd.comando = "terminate";
    	if(r.audioClipEntrante().isRunning() || r.audioClipLlamadas().isRunning()){
    		r.pararSonido();
    	}
    	cmd.random = false;
    	vista.getPm().setVisible(false);
    }
    
    /**
     * <u>Mñtodo para cerrar el proceso externo y finalizar las tareas del telefono.</u>
     */
    public void finalizarTelefono(){
    	cmd.comando = "quit";
    	if(r.audioClipEntrante().isRunning() || r.audioClipLlamadas().isRunning()){
    		r.pararSonido();
    	}
    }
    
    /**
     * <u>Mñtodo para recoger el objeto proceso.</u>
     * @return Process
     */
    public Process getProcces(){
    	return p;
    }
    
    /**
     * <u>Mñtodo para recoger el objeto CMD.</u>
     * @return cmd
     */
    public CMD getCMD(){
    	return cmd;
    }
    
    /**
     * <u>Mñtodo para recoger el objeto Reproductor.</u>
     * @return reproductor
     */
    public Reproductor getReproducto(){
    	return r;
    }
    
    /**
     * <u>Mñtodo para recoger la ruta donde se encuentra el proceso externo que va a utilizar el telefono.</u>
     * @return ruta
     */
    public String getIniciar(){
    	return iniciar;
    }
    
    /**
     * <u>Mñtodo para cambiar la ruta donde se encuentra el proceso externo y indicarle de donde lo tiene que iniciar.</u>
     *
     * @param iniciar the new iniciar
     */
    public void setIniciar(String iniciar){
    	this.iniciar = iniciar;
    }
    
}
