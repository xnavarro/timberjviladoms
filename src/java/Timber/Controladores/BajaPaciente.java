package Timber.Controladores;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Clase que da de baja al paciente del servicio telef�nico.
 * @author Sergio Ascanio, Andrei Iacob, Aitor L�pez
 * @version 19/05/2015
 */
public class BajaPaciente {
    public static String ip;
    
	/**
	 * M�todo que da de baja al usuario del servicio telef�nico.
	 * @param numeroTlf
	 */
    public void setIp(String ip){
    	this.ip = ip;
    }
    public String getIp(){
    	return ip;
    }
    
    public static void bajaPaciente(String numeroTlf){
        String busqueda = "[" + numeroTlf + "]";
        File f = new File("X:"+File.separator +"users.conf");
        List<String> contenido = leerDocumento(f,busqueda);
        escribirDocumento(contenido,f);
    }
    
    /**
     * M�todo que lee el documento el cual hay que modificar para dar de baja al usuario.
     * @param f File con la ruta del fichero.
     * @param busqueda String con la busqueda del usuario.
     * @return Devuelve una lista con los datos que hay que dar de baja del usuario.
     */
    private static List<String> leerDocumento(File f, String busqueda){
        BufferedReader br = null;
        try {
            List<String> contenido = new ArrayList<>();
            br = new BufferedReader(new FileReader(f));
            String line;
            boolean encontrado = false;
            int n = 37, count = 0;
            while((line = br.readLine())!= null){
                if(!encontrado){    
                    if(!line.equalsIgnoreCase(busqueda) && !encontrado){
                        contenido.add(line);
                        System.out.println(line);
                    } else {
                        encontrado = true;
                    }
                } else {
                    if(count < n){
                        count++;
                    } else {
                        encontrado = false;
                    }
                }
            }
            try {
                br.close();
            } catch (IOException ex) {
                Logger.getLogger(BajaPaciente.class.getName()).log(Level.SEVERE, null, ex);
            }
            return contenido;
        } catch (FileNotFoundException ex) {
            Logger.getLogger(BajaPaciente.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(BajaPaciente.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                br.close();
            } catch (IOException ex) {
                Logger.getLogger(BajaPaciente.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return null;
    }
    
    /**
     * M�todo que actualiza el documento con los usuarios para dar de baja al paciente buscado previamente.
     * @param contenido Par�metro con la lista de contenido del usuario buscado
     * @param f Fichero en el cual se encuentran los datos de los usuarios.
     */
    private static void escribirDocumento(List<String> contenido,File f){
        PrintWriter dos = null;
        try {
            dos = new PrintWriter(new FileWriter(f,false));
            for(int i = 0; i < contenido.size(); i++){
                dos.println(contenido.get(i));
            }
        } catch (IOException ex) {
            Logger.getLogger(BajaPaciente.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            dos.close();
        }
    }
    
}
