package Timber.Controladores;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import com.yayotech.vistas.Vista;

/**
 * <b>Class controlador que se utiliza como medio para contactar con la vista y con los mñtodos para poder manejar la funcionalidad
 * de la aplicaciñn correctamente.</b>
 * @author Sergio
 * @version 19/05/2015
 */
public class ControladorPhone {
	
	/**
	 * <b>Mñtodo para incomporar a un botñn una funcionalidad de añadir un nñmero a la aplicaciñn.</b>
	 * @param vista Phone el objeto de la interfaz para poder añadirle dicho nñmero.
	 * @param n String con el nñmero que queremos que añada a dicha interfaz.
	 * @return ActionListener Que contiene la funciñn que el botñn va a ejecutar cuando sea presionado.
	 */
	public ActionListener addNumero(Vista vista, String n){
		return new addNumero(vista,n);
	}
	
	/**
	 * <b>Mñtodo para eliminar un nñmero de la la interfaz por si en alguno nos hemos equivocado y poder retroceder.</b>
	 * @param vista Phone el objeto de la interfaz para poder eliminar el ñltimo nñmero introducido.
	 * @return ActionListener Que contiene la funciñn que el botñn va a ejecutar cuando sea presionado.
	 */
	public ActionListener eliminarNumero(Vista vista){
		return new eliminarNumero(vista);
	}
	
	/**
	 * <b>Mñtodo para llamar a un nñmero escrito previamente en la interfaz.</b>
	 * @param vista Phone el objeto de la interfaz con el que sacaremos de ella el nñmero al que queremos llamar.
	 * @return ActionListener Que contiene la funciñn que el botñn va a ejecutar cuando sea presionado.
	 */
	public ActionListener llamarNumero(Vista vista){
		return new llamarNumero(vista);
	}
	
	/**
	 * <b>Mñtodo para coger una llamada entrante.</b>
	 * @param vista Phone el objeto de la interfaz con el que podremos coger el servicio y indicar que queremos coger dicha llamada.
	 * @return ActionListener Que contiene la funciñn que el botñn va a ejecutar cuando sea presionado.
	 */
	public ActionListener cogerLlamada(Vista vista){
		return new cogerLLamada(vista);
	}
	
	/**
	 * <b>Mñtodo para colgar una llamada entrante, o en ejecuciñn.</b>
	 * @param vista Phone el objeto de la interfaz con el que podremos coger el servicio y indicar que queremos colgar dicha llamada.
	 * @return ActionListener Que contiene la funciñn que el botñn va a ejecutar cuando sea presionado.
	 */
	public ActionListener colgarLlamada(Vista vista){
		return new colgarLLamada(vista);
	}
	
	/**
	 * <b>Class implementada de ActionListener creada para añadir nñmero en la interfaz.</b>
	 * @author Sergio 
	 * @version 19/05/2015
	 */
	public class addNumero implements ActionListener {
		
	/**
	 * <i><b>Vista</b>: Objeto con el que añadiremos el nñmero que nosotros queramos introducir en la aplicaciñn.</i>
	 */
		private Vista p;
		/**
		 * <i><b>n(Nñmero)</b>: Nñmero que queremos añadir en la aplicaciñn.</i>
		 */
		private String n;
		
		/**
		 * <u>Mñtodo constructor con el que rellenamos las variables que necesitamos.<7u>
		 * @param p Vista variable que se usarñ para introducir los nñmeros.
		 * @param n String variable que contiene el nñmero que queremos introducir.
		 */
		public addNumero(Vista p, String n){
			this.p = p;
			this.n = n;
		}
		
		/**
		 * <u>Mñtodo sobre escrito que ejecuta la funciñn de añadir un nñmero en la aplicaciñn.</u>
		 */
		@Override
		public void actionPerformed(ActionEvent e){
			if(n.equalsIgnoreCase("ast")){
				p.setNumero(p.getNumero() + "*");
			} else if(n.equalsIgnoreCase("alm")){
				p.setNumero(p.getNumero() + "#");
			} else {
				p.setNumero(p.getNumero() + n);
			}
		}
	}
	
	/**
	 * <b>Class para eliminar los nñmeros que hemos introducido por si nos hemos equivocado.
	 * Class implementada de ActionListener para enviar dicha funcionalidad a los botñnes.</b>
	 * @author Sergio
	 * @version 19/05/2015
	 */
	public class eliminarNumero implements ActionListener {
	
		/**
		 * <i><b>Vista</b>: Objeto con el que recogeremos el campo de texto y le iremos quitando nñmeros.</i>
		 */
		private Vista p;
		
		/**
		 * <u>Mñtodo contructor con el que rellenamos las variables necesarias para la correcta funcionalidad de la clase.</u>
		 * @param p Phone objecto con el que controlaremos la interfaz.
		 */
		public eliminarNumero(Vista p){
			this.p = p;
		}
		
		/**
		 * <u>Mñtodo sobreescrito al implementar de ActionListener que tiene la funcionalidad de ir eliminando los nñmeros marcados
		 * cuando se presione el botñn.</u>
		 */
		@Override
		public void actionPerformed(ActionEvent e){
			String aux = p.getNumero();
			if(aux.length() > 0){
				if(aux.length() == 1){
					p.setNumero("");
				} else {
					p.setNumero(aux.substring(0, aux.length()-1));
				}
			}
		}
	}
	
	/**
	 * <b>Class implementada de ActionListener para darle la funcionalidad al botñn de llamar.
	 * Class que llama a un nñmero deperminado indicado con anterioridad en la aplicaciñn.</b>
	 * @author Sergio
	 * @version 19/05/2015
	 */
	public class llamarNumero implements ActionListener {
		
		/**
		 * <i><b>Vista</b>: Objeto de la interfaz con el que recogeremos el servicio de llamadas y el nñmero al que queremos llamar.</i>
		 */
		private Vista p;
		
		/**
		 * <u>Mñtodo constructor con el que rellenaremos las variables necesarias para la funcionalidad de la clase.</u>
		 * @param p Vista Objecto con el que controlaremos la interfaz.
		 */
		public llamarNumero(Vista p){
			this.p = p;		}
		
		/**
		 * <u>Mñtodo sobreescrito al implementar de ActionListener que indica al serviciñ la ejecuciñn de una llamada a un nñmero</u>
		 * marcado en la interfaz.
		 */
		@Override
		public void actionPerformed(ActionEvent e){
			p.getServicio().llamar(p.getNumero());
		}
	}
	
	/**
	 * <b>Class coger llamada implementada de ActionListener para darle la funcionalidad a los botones.
	 * Class recoge las llamadas entrantes en la aplicaciñn.</b>
	 * @author Sergio
	 * @version 19/05/2015
	 */
	public class cogerLLamada implements ActionListener {
		
		/**
		 * <i><b>Vista</b>: Objeto de la interfaz con el que recogeremos el servicio para recoger llamadas.</i>
		 */
		private Vista p;
		
		/**
		 * <u>Mñtodo contructor para rellenar las variables necesarias para la correcta funcionalidad de la clase.</u>
		 * @param p Objeto Phone con el que controlaremos el funcionamiento de la aplicaciñn
		 */
		public cogerLLamada(Vista p){
			this.p = p;
		}
	
		/**
		 * <u>Mñtodo sobreescrito al implementar de ActionListener que ejecuta el mñtodo del servicio responder para coger las llamadas.</u>
		 */
		@Override
		public void actionPerformed(ActionEvent e){
			p.getServicio().responder();
		}
	
	}
	
	/**
	 * <b>Class colgar llamadas implementada de actionListener para indicar la funcionalidad a los botones.
	 * Class que se utilizarñ para colgar todas las llamadas tanto entrantes como en ejecuciñn.</b>
	 * @author Sergio
	 *
	 */
	public class colgarLLamada implements ActionListener {
	
		/**
		 * <i><b>Vista</b>: Objeto con el que recogeremos el servicio y colgaremos todo.</i>
		 */
		private Vista p;
		
		/**
		 * <u>Mñtodo constructor para rellenar las variables necesarias para la funcionalidad de la clase.</u>
		 * @param p Objeto Phone con el que controlaremos la interficie.
		 */
		public colgarLLamada(Vista p){
			this.p = p;
		}
		
		/**
		 * <u>Mñtodo sobreescrito de ActionListener que se utilizarñ para ponerle una funcionalidad a un botñn en este caso colgar.</u>
		 */
		@Override
		public void actionPerformed(ActionEvent e){
			p.getServicio().colgar();
			p.setLabel("COLGANDO ...");
			p.getServicio().getCMD().terminar = true;
		}

	}

}
