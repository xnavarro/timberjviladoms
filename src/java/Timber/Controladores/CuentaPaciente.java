package Timber.Controladores;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;


/**
 * Clase que permite dar de alta al paciente del servicio de teleasistencia en el servicio telef�nico.
 * @author Sergio Ascanio
 * @version 19/05/2015
 */
public class CuentaPaciente {
	public static String ip = CONF.recogerIPTelf();
	/**
	 * M�todo que da de alta al usuario/paciente en el servicio telef�nico de nuestro proyecto.
	 * @param usuario String con el nombre del usuario a dar de alta.
	 * @param numeroTlf String con el n�mero de telefono del usuario a dar de alta.
	 * @param password String con la contra�a del usuario a dar de alta.
	 */
	
	public void setIp(String ip){
		this.ip = ip;
		CONF.cambiarIPTelf(ip);
	}
    
	/**
	 * M�todo que crea una cuenta del servicio telef�nico al usuario deseado.
	 * @param usuario Nombre del usuario al que se le da de alta en el servicio telef�nico.
	 * @param numeroTlf N�mero del tel�fono del usuario al que se da de alta en el servicio telef�nico.
	 * @param password Contrase�a del usuario para el servicio telef�nico.
	 */
	public static void crearCuenta(String usuario, String numeroTlf, String password) {
		PrintWriter dos = null;
		
		try {
			File f = new File("X:"+File.separator + "users.conf");
			dos = new PrintWriter(new FileWriter(f, true));
			dos.println();
			dos.println("[" + numeroTlf + "]");
			dos.println("fullname = " + usuario);
			dos.println("registersip = no");
			dos.println("host = dynamic");
			dos.println("callgroup = 1");
			dos.println("mailbox = " + numeroTlf);
			dos.println("call-limit = 100");
			dos.println("type = peer");
			dos.println("username = " + numeroTlf);
			dos.println("transfer = yes");
			dos.println("call counter = yes");
			dos.println("context = DLPN_Client");
			dos.println("cid_number = " + numeroTlf);
			dos.println("hasvoicemail = no");
			dos.println("vmsecret =");
			dos.println("email =");
			dos.println("threewaycalling = no");
			dos.println("hasdirectory = no");
			dos.println("callwaiting = no");
			dos.println("hasmanager = no");
			dos.println("hasagent = no");
			dos.println("hassip = yes");
			dos.println("hasiax = yes");
			dos.println("secret = " + password);
			dos.println("nat = yes");
			dos.println("canreinvite = no");
			dos.println("dtmfmode = rfc2833");
			dos.println("insecure = no");
			dos.println("piclupgroup = 1");
			dos.println("disallow = all");
			dos.println("allow = ulaw,gsm");
			dos.println("macaddress = " + numeroTlf);
			dos.println("autoprov = yes");
			dos.println("label = " + numeroTlf);
			dos.println("linenumber = 1");
			dos.println("LINEKEYS = 1");
		} catch (FileNotFoundException ex) {
			ex.printStackTrace();
		} catch (IOException ex) {
			ex.printStackTrace();
		} finally {
			dos.close();
		}
	}
}
