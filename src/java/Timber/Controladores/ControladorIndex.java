/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Timber.Controladores;

import Timber.DAO.IndexDAO;
import Timber.DAO.TrabajadorDAO;
import Timber.Modelo.Trabajador;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Alba
 */
public class ControladorIndex extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, SQLException {
        response.setContentType("text/html;charset=UTF-8");

        String user = request.getParameter("nombre");    
        String pass = request.getParameter("contrasena");
        HttpSession session = request.getSession();
        TrabajadorDAO trabajador = new TrabajadorDAO();
        IndexDAO con = new IndexDAO();
        List errores = null;
        request.setAttribute("goingTo", "paciente");
        if( con.Autenticacion(user, pass))
        {
            Trabajador emp = trabajador.obtenerTrabajador(user, false);
            session.setAttribute("usuario", emp.getNombre()+" "+emp.getApellido1());
            if(con.esAdministrador(user, pass)){
                session.setAttribute("isAdmin", "true");
                request.getRequestDispatcher("ControladorLanding?goingTo=paciente").include(request, response);    
            }else{
                session.setAttribute("isAdmin", "false");
                response.sendRedirect("ControladorLanding?goingTo=paciente");
            }
        }else {
            errores = new ArrayList();
            errores.add("loginError");
            request.setAttribute("errores", errores);
            request.getRequestDispatcher("index.jsp").include(request, response);
        }

        
    }
    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (SQLException ex) {
            Logger.getLogger(ControladorIndex.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (SQLException ex) {
             Logger.getLogger(ControladorIndex.class.getName()).log(Level.SEVERE,null,ex);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
