package Timber.Controladores;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

/**
 * Clase que tiene como objetivo administrar el fichero de configuraci�n de la aplicaci�n.
 * @author Sergio Ascanio, Andrei Iacob, Aitor L�pez
 * @version 25/05/2015
 */
public class CONF {
    
	/**
	 * Variable File para indicar cual es el fichero de configuraci�n.
	 */
    private static File f = new File("C:" + File.separator +"Program Files" + File.separator + "Yayotech" + File.separator + "yayotech.conf");   
    
    /**
     * M�todo que cambia la IP para acceder al servidor de telefon�a.
     * @param telf Par�metro IP.
     */
    public static void cambiarIPTelf(String telf){
        cambiarArchivo(leerArchivo(), "IP-PHONE","IP-PHONE = " + telf);
    }
    
    /**
     * M�todo que cambia el sonido de la llamada entrante
     * @param ruta Ruta del fichero .wav
     */
    public static void cambiarSonidoEntrante(String ruta){
        cambiarArchivo(leerArchivo(), "INCOMING-SOUND","INCOMING-SOUND = " + ruta);
    }
    
    /**
     * M�todo que cambia el sonido al llamar.
     * @param ruta Ruta del fichero .wav
     */
    public static void cambiarSonidoLlamar(String ruta){
        cambiarArchivo(leerArchivo(), "OUTGOING-SOUND","OUTGOING-SOUND = " + ruta);
    }
    
    /**
     * M�todo que cambia la IP de la base de datos que se quiera utilizar.
     * @param IP Par�metro IP de la base de datos.
     */
    public static void cambiarIPDB(String IP){
        cambiarArchivo(leerArchivo(), "IP-DB","IP-DB = " + IP);
    }
    
    /**
     * M�todo que cambia el puerto que permite la conexi�n a la base de datos.
     * @param PORT Puerto de la base de datos a utilizar.
     */
    public static void cambiarPuertoDB(String PORT){
        cambiarArchivo(leerArchivo(), "PORT-DB","PORT-DB = " + PORT);
    }
    
    /**
     * M�todo que permite introducir el nombre de la base de datos a la cual se quiere acceder.
     * @param nombre Nombre de la base de datos que se quiere utilizar.
     */
    public static void cambiarNameDB(String nombre){
        cambiarArchivo(leerArchivo(), "NAME-DB","NAME-DB = " + nombre);
    }
    
    /**
     * M�todo que cambia la ruta del proceso a ejecutar para que funcionen los comandos del tel�fono.
     * @param ruta Ruta de linphonec.exe
     */
    public static void cambiarProcessB(String ruta){
        cambiarArchivo(leerArchivo(), "PROCESS","PROCESS = " + ruta);
    }
    
    /**
     * M�todo que recoge la IP para la conexi�n al servicio telef�nico.
     * @return M�todo que devuelve la IP del servicio telef�nico.
     */
    public static String recogerIPTelf(){
        String [] aux  = null;
        List<String> contenido = leerArchivo();
        for(int i = 0; i < contenido.size(); i++){
            if(contenido.get(i).contains("IP-PHONE")){
                aux = contenido.get(i).split(" = ");
            } 
        }
        if(aux != null ){
            if(aux.length == 2){
                return aux[1];
            }
            return null;
        }
        return null;
    }
    
    public static String recogerNombreDatabase(){
    	String [] aux  = null;
        List<String> contenido = leerArchivo();
        for(int i = 0; i < contenido.size(); i++){
            if(contenido.get(i).contains("NAME-DB")){
                aux = contenido.get(i).split(" = ");
            } 
        }
        if(aux != null ){
            if(aux.length == 2){
                return aux[1];
            }
            return null;
        }
        return null;
    }
    
    public static String recogerProcess(){
    	String [] aux  = null;
        List<String> contenido = leerArchivo();
        for(int i = 0; i < contenido.size(); i++){
            if(contenido.get(i).contains("PROCESS")){
                aux = contenido.get(i).split(" = ");
            } 
        }
        if(aux != null ){
            if(aux.length == 2){
                return aux[1];
            }
            return null;
        }
        return null;
    }
    
    public static String recogerSonidoEntrante(){
        String [] aux  = null;
        List<String> contenido = leerArchivo();
        for(int i = 0; i < contenido.size(); i++){
            if(contenido.get(i).contains("INCOMING-SOUND")){
                aux = contenido.get(i).split(" = ");
            } 
        }
        if(aux != null ){
            if(aux.length == 2){
                return aux[1];
            }
            return null;
        }
        return null;
    }
    
    public static String recogerSonidoLlamar(){
        String [] aux  = null;
        List<String> contenido = leerArchivo();
        for(int i = 0; i < contenido.size(); i++){
            if(contenido.get(i).contains("OUTGOING-SOUND")){
                aux = contenido.get(i).split(" = ");
            } 
        }
        if(aux != null ){
            if(aux.length == 2){
                return aux[1];
            }
            return null;
        }
        return null;
    }
    
    public static String recogerIPDB(){
        String [] aux  = null;
        List<String> contenido = leerArchivo();
        for(int i = 0; i < contenido.size(); i++){
            if(contenido.get(i).contains("IP-DB")){
                aux = contenido.get(i).split(" = ");
            } 
        }
        if(aux != null ){
            if(aux.length == 2){
                return aux[1];
            }
            return null;
        }
        return null;
    }
    
    public static String recogerPuertoDB(){
        String [] aux  = null;
        List<String> contenido = leerArchivo();
        for(int i = 0; i < contenido.size(); i++){
            if(contenido.get(i).contains("PORT-DB")){
                aux = contenido.get(i).split(" = ");
            } 
        }
        if(aux != null ){
            if(aux.length == 2){
                return aux[1];
            }
            return null;
        }
        return null;
    }
    
    private static void cambiarArchivo(List<String> contenido, String comprobar, String cambiar){
        PrintWriter dos = null;
        try {
            dos = new PrintWriter(new FileWriter(f,false));
            for(int i = 0; i < contenido.size(); i++){
                if(contenido.get(i).contains(comprobar)){
                    dos.println(cambiar);
                } else {
                    dos.println(contenido.get(i));
                }
            }
        } catch (IOException ex) {
            ex.printStackTrace();
        } finally {
            dos.close();
        }
    }
    
    private static List<String> leerArchivo(){
        BufferedReader br = null;
        try {
            List<String> contenido = new ArrayList<>();
            br = new BufferedReader(new FileReader(f));
            String line;
            while((line = br.readLine())!= null){
                    contenido.add(line);
            }
            try {
                br.close();
            } catch (IOException ex) {
                ex.printStackTrace();
            }
            return contenido;
        } catch (FileNotFoundException ex) {
            ex.printStackTrace();
        } catch (IOException ex) {
            ex.printStackTrace();
        } finally {
            try {
                br.close();
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }
        return null;
    }
        
}
