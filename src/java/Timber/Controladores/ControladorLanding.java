/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Timber.Controladores;

import Timber.Modelo.Trabajador;
import Timber.DAO.PacienteDAO;
import Timber.DAO.TrabajadorDAO;
import Timber.Modelo.Paciente;
import java.io.IOException;
import java.sql.SQLException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Alba
 */
@WebServlet(name = "ControladorLanding", urlPatterns = {"/ControladorLanding"})
public class ControladorLanding extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     * @throws java.sql.SQLException
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, SQLException {
        response.setContentType("text/html;charset=UTF-8");
        
        request.setAttribute("pageToGo", "mainPacientes.jsp");
        String goingTo = (String)request.getParameter("goingTo");
        Trabajador emp = (Trabajador)request.getAttribute("usuario");
        request.setAttribute("datosAcceso",emp);
        if (goingTo != null && goingTo.equals("trabajador")) {
            //Visualizamos los trabajadores registrados
            TrabajadorDAO trabajador = new TrabajadorDAO();
            List<Trabajador> trabajadores = trabajador.obtenerTrabajadores();
            request.setAttribute("trabajadores",trabajadores);
            request.setAttribute("pageToGo", "mainTrabajadores.jsp");
            
        } else if (goingTo != null && goingTo.equals("paciente")) {
            
            PacienteDAO paciente = new PacienteDAO();
            List<Paciente> pacientes = paciente.obtenerPacientes();
            request.setAttribute("pacientes",pacientes);
            request.setAttribute("pageToGo", "mainPacientes.jsp");
        }
        
        
        
        request.getRequestDispatcher("landing.jsp").forward(request, response);
        
        
        
    }
    

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (SQLException ex) {
            Logger.getLogger(ControladorLanding.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (SQLException ex) {
            Logger.getLogger(ControladorLanding.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
