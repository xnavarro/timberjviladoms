package Timber.Controladores;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.JOptionPane;

import com.yayotech.vistas.AñadirContacto;
import com.yayotech.vistas.Borrar;
import com.yayotech.vistas.BuscarModificar;
import com.yayotech.vistas.Contactos;
import com.yayotech.vistas.Opciones;
import com.yayotech.vistas.Pestañas;
import com.yayotech.vistas.PestañasModificar;
import com.yayotech.vistas.Vista;

public class ControladorContactos {
	
	public ActionListener buscarPacienteDNI(AñadirContacto ac, Contactos contacto, Vista v, ModeloMariaDB modelo){
		return new buscarPaciente(ac, contacto, v, modelo);
	}
	
	public ActionListener abrirContactos(AñadirContacto ac, Contactos contacto, Vista v,Borrar b, Pestañas p, PestañasModificar pm){
		return new abrirContactos(ac, contacto, v,b, p, pm);
	}
	
	public WindowAdapter cerrarVentanaContactos(AñadirContacto ac){
		return new cerrarVentanaContactos(ac);
	}
	
	public ActionListener addContacto(AñadirContacto ac, Contactos contacto, Vista v , ModeloMariaDB modelo){
		return new addContacto(ac, contacto, v, modelo);
	}
	
	public ActionListener cboFeina(Contactos contacto){
		return new seleccionarFeina(contacto);
	}
	
	public class buscarPaciente implements ActionListener {
		private AñadirContacto ac;
		private Contactos contacto;
		private Vista v;
		private ModeloMariaDB modelo;
		
		public buscarPaciente(AñadirContacto ac, Contactos contacto, Vista v, ModeloMariaDB modelo) {
			this.ac = ac;
			this.modelo = modelo;
			this.contacto = contacto;
			this.v = v;
		}

		@Override
		public void actionPerformed(ActionEvent e) {			
			modelo.buscarDNI(ac.getTxtDniPaciente().getText());
		}
	}	
	
	public class abrirContactos implements ActionListener{
		
		private AñadirContacto ac;
		private Contactos contacto;
		private Vista v;
		private Borrar b;
		private Pestañas p;
		private PestañasModificar pm;
		
		public abrirContactos(AñadirContacto ac, Contactos contacto, Vista v,Borrar b,Pestañas p, PestañasModificar pm) {
			this.ac = ac;
			this.contacto = contacto;
			this.v = v;
			this.b = b;
			this.p = p;
			this.pm = pm;
		}

		@Override
		public void actionPerformed(ActionEvent arg0) {
			contacto.setVisible(false);
			b.setVisible(false);
			ac.setVisible(true);
			p.setVisible(false);
			pm.setVisible(false);
		}
	}
	
	public class cerrarVentanaContactos extends WindowAdapter {
		private AñadirContacto contacto;

		public cerrarVentanaContactos(AñadirContacto contacto) {
			this.contacto = contacto;
		}

		@Override
		public void windowClosing(WindowEvent e) {
			contacto.setVisible(false);
		}
	}
	
	public class seleccionarFeina implements ActionListener {
		private Contactos contacto;
		
		public seleccionarFeina(Contactos ac) {
			this.contacto = ac;
		}
		
		@Override
		public void actionPerformed(ActionEvent arg0) {
			if(contacto.getCboFeina().getSelectedIndex() == 1){
				contacto.getCboHorariFeina().setEnabled(true);
			}else{
				contacto.getCboHorariFeina().setEnabled(false);
			}
		}
	}
	
	public class addContacto implements ActionListener{
		private AñadirContacto ac;
		private Contactos contacto;
		private Vista v;
		private ModeloMariaDB modelo;
				
		public addContacto(AñadirContacto ac, Contactos contacto, Vista v, ModeloMariaDB modelo){
			this.ac = ac;
			this.contacto = contacto;
			this.v = v;
			this.modelo = modelo;
		}
		
		@Override
		public void actionPerformed(ActionEvent arg0) {
			String regexNombrePaciente = "^[\\p{L} .'-]+$";
			String regexInstrucciones = "^[a-zA-Z0-9_]*$";
			String regexPoblacion = "[a-zA-Z]+";
			String regexCodiPostal = "\\d{5}";
			String regexEmail = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
			String regexTelefonos = "\\d{9}";
			
			if (contacto.getTxtNomCognom().getText().length() <= 0) {
				JOptionPane.showMessageDialog(contacto,"Introduce el nombre del contacto." , "Error", JOptionPane.ERROR_MESSAGE);
			} else if (contacto.getCboSexe().getSelectedIndex() == 0) {
				JOptionPane.showMessageDialog(contacto,"Selecciona el sexo del contacto." , "Error", JOptionPane.ERROR_MESSAGE);
			} else if (contacto.getTxtMobil().getText().length() <= 0) {
				JOptionPane.showMessageDialog(contacto,"Introduce el movil del contacto." , "Error", JOptionPane.ERROR_MESSAGE);
			} else if (contacto.getTxtRelacio().getText().length() <= 0) {
				JOptionPane.showMessageDialog(contacto,"Introduce la relaciñn con el paciente." , "Error", JOptionPane.ERROR_MESSAGE);
			} else if (contacto.getTxtPoblacio().getText().length() <= 0) {
				JOptionPane.showMessageDialog(contacto,"Introduce la poblaciñn del contacto." , "Error", JOptionPane.ERROR_MESSAGE);
			} else if (contacto.getTxtCodiPostal().getText().length() <= 0) {
				JOptionPane.showMessageDialog(contacto, "Introduce el codigo postal del contacto." , "Error", JOptionPane.ERROR_MESSAGE);
			} else if (contacto.getTxtDomicili().getText().length() <= 0) {
				JOptionPane.showMessageDialog(contacto, "Introduce el domicilio del contacto." , "Error", JOptionPane.ERROR_MESSAGE);
			} else if (contacto.getTxtAdreca().getText().length() <= 0) {
				JOptionPane.showMessageDialog(contacto, "Introduce la direccion del contacto." , "Error", JOptionPane.ERROR_MESSAGE);
			} else if (contacto.getTxtTelefonContacte().getText().length() <= 0) {
				JOptionPane.showMessageDialog(contacto, "Introduce el telefono del contacto." , "Error", JOptionPane.ERROR_MESSAGE);
			} else if (contacto.getCboFeina().getSelectedIndex() == 0) {
				JOptionPane.showMessageDialog(contacto, "Introduce el apartado de trabajo." , "Error", JOptionPane.ERROR_MESSAGE);
			} else if (contacto.getTxtEmail().getText().length() <= 0) {
				JOptionPane.showMessageDialog(contacto, "Introduce el email del contacto." , "Error", JOptionPane.ERROR_MESSAGE);
			} else if (contacto.getCboClaus().getSelectedIndex() == 0) {
				JOptionPane.showMessageDialog(contacto, "Selecciona el apartado de llaves." , "Error", JOptionPane.ERROR_MESSAGE);
			} else {
				if(contacto.getCboHorariFeina().getSelectedIndex() == 0){
					JOptionPane.showMessageDialog(contacto, "Selecciona el horario de trabajo.", "Error", JOptionPane.ERROR_MESSAGE);
				}else{
					if(!contacto.getTxtNomCognom().getText().matches(regexNombrePaciente)){
						JOptionPane.showMessageDialog(contacto, "Nombre no vñlido." , "Error", JOptionPane.ERROR_MESSAGE);
					}else if(!contacto.getTxtRelacio().getText().matches(regexPoblacion)){
						JOptionPane.showMessageDialog(contacto, "Relaciñn con el paciente no vñlida." , "Error", JOptionPane.ERROR_MESSAGE);
					}else if(!contacto.getTxtPoblacio().getText().matches(regexInstrucciones)){
						JOptionPane.showMessageDialog(contacto, "Poblaciñn no vñlida." , "Error", JOptionPane.ERROR_MESSAGE);
					}else if(!contacto.getTxtCodiPostal().getText().matches(regexCodiPostal)){
						JOptionPane.showMessageDialog(contacto, "Codigo postal no vñlido." , "Error", JOptionPane.ERROR_MESSAGE);
					}else if(!contacto.getTxtAdreca().getText().matches(regexInstrucciones)){
						JOptionPane.showMessageDialog(contacto, "Direcciñn no vñlida." , "Error", JOptionPane.ERROR_MESSAGE);
					}else if(!contacto.getTxtTelefonContacte().getText().matches(regexTelefonos)){
						JOptionPane.showMessageDialog(contacto, "Telefono no vñlido." , "Error", JOptionPane.ERROR_MESSAGE);
					}else if(!contacto.getTxtDomicili().getText().matches(regexInstrucciones)){
						JOptionPane.showMessageDialog(contacto, "Domicilio invalido." , "Error", JOptionPane.ERROR_MESSAGE);
					}else if(!contacto.getTxtMobil().getText().matches(regexTelefonos)){
						JOptionPane.showMessageDialog(contacto, "Telefono movil no vñlido." , "Error", JOptionPane.ERROR_MESSAGE);
					}else if(!contacto.getTxtEmail().getText().matches(regexEmail)){
						JOptionPane.showMessageDialog(contacto, "Email no vñlido." , "Error", JOptionPane.ERROR_MESSAGE);
					}else{
						String codiPacient = modelo.buscarCodiPacient(ac.getTxtDniPaciente().getText());
						modelo.insertarContacte(codiPacient, contacto.getTxtNomCognom().getText(), contacto.getCboSexe().getSelectedItem().toString(), contacto.getTxtRelacio().getText(), contacto.getTxtPoblacio().getText(), contacto.getTxtCodiPostal().getText(), contacto.getTxtAdreca().getText(), contacto.getTxtMobil().getText(), contacto.getTxtDomicili().getText(), contacto.getCboFeina().getSelectedItem().toString(), contacto.getCboHorariFeina().getSelectedItem().toString(), contacto.getTxtTelefonContacte().getText(), contacto.getTxtEmail().getText(), contacto.getCboClaus().getSelectedItem().toString());
					}
				}
			}
		}
	}
}
