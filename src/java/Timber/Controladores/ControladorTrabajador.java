/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Timber.Controladores;

import Timber.DAO.PersonUtils;
import Timber.DAO.TrabajadorDAO;
import Timber.Modelo.Trabajador;
import com.sun.corba.se.impl.naming.cosnaming.NamingUtils;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Yo
 */
@WebServlet(name = "ControladorTrabajador", urlPatterns = {"/ControladorTrabajador"})
public class ControladorTrabajador extends HttpServlet {

    ArrayList errores = null;

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, SQLException {

        //Gestión de la botonera
        String accion = (String) request.getParameter("accion");
        String goingTo = (String) request.getParameter("goingTo");
        System.out.println("Vamos a borrar trabajador");
        TrabajadorDAO trabajadorDAO = new TrabajadorDAO();
        Trabajador trab = new Trabajador();

        if (accion != null && accion.equals("borrar")) {
            System.out.println("borrar!!");
            trab.setDni((String) request.getParameter("dni"));
            trabajadorDAO.borrarTrabajador(trab);
            System.out.println("Trabajador Borrado");
        } else if (accion != null && (accion.equals("consultar") || accion.equals("modificar"))) {
            HashMap mapValores = setearCampos((String) request.getParameter("dni"));
            request.setAttribute("info", mapValores);
            if (accion.equals("consultar")) {
                request.setAttribute("readOnly", "readonly");
            }
        } else if (accion != null && accion.equals("alta")) {
            trab = validadorCampos(request, trab);
            Trabajador aux = trabajadorDAO.obtenerTrabajador(trab.getDni(), true);
            if (errores.isEmpty()) {
                if (aux != null) {
                    errores.add("dniExiste");
                } else {
                    aux = trabajadorDAO.obtenerTrabajador(trab.getUsuario(), false);
                    if (aux != null) {
                        errores.add("userExiste");
                    }
                }
                if (errores.isEmpty()) {
                    trabajadorDAO.insertarTrabajador(trab);
                    request.setAttribute("dadoDeAlta", "true");
                } else {
                    request.setAttribute("errores", errores);
                }
            } else {
                request.setAttribute("errores", errores);
            }
        } else if (accion != null && accion.equals("modificando")) {
            trab.setDni((String) request.getParameter("dni"));
            trab = trabajadorDAO.obtenerTrabajador(trab.getDni(), true);

            Trabajador aux = validadorCampos(request, trab);
            if (errores.isEmpty()) {
                aux = trabajadorDAO.obtenerTrabajador(aux.getDni(), true);
                if (!trab.getDni().equals(aux.getDni())) {
                    if (aux != null) {
                        errores.add("dniExiste");
                    } else {
                        aux = trabajadorDAO.obtenerTrabajador(trab.getUsuario(), false);
                        if (!trab.getDni().equals(aux.getDni())) {
                            if (aux != null) {
                                errores.add("userExiste");
                            }
                        }
                    }
                }
                if (errores.isEmpty()) {
                    long idEmpleado = trab.getIdEmpleado();
                    trabajadorDAO.updateTrabajador(validadorCampos(request, trab), idEmpleado);
                    request.setAttribute("pageToGo", "mainTrabajadores.jsp");
                    request.getRequestDispatcher("ControladorLanding?goingTo=trabajador").forward(request, response);
                } else {
                    request.setAttribute("errores", errores);
                    HashMap mapValores = setearCampos((String) request.getParameter("dni"));
                    request.setAttribute("info", mapValores);
                }
            } else {
                request.setAttribute("errores", errores);
                HashMap mapValores = setearCampos((String) request.getParameter("dni"));
                request.setAttribute("info", mapValores);
            }

        }
        request.getRequestDispatcher("gesttrabajadores.jsp").forward(request, response);

    }

    public HashMap setearCampos(String dni) throws SQLException {
        Trabajador trab = new Trabajador();
        TrabajadorDAO trabajadorDAO = new TrabajadorDAO();
        trab.setDni(dni);
        trab = trabajadorDAO.obtenerTrabajador(trab.getDni(), true);
        HashMap mapValores = new HashMap();
        mapValores.put("nombre", trab.getNombre());
        mapValores.put("apellido1", trab.getApellido1());
        mapValores.put("apellido2", trab.getApellido2());
        mapValores.put("fechaNacimiento", trab.getFechaNacimiento());
        mapValores.put("dni", trab.getDni());
        mapValores.put("esAdmin", trab.getEsAdmin());
        mapValores.put("usuario", trab.getUsuario());
        mapValores.put("contrasena", trab.getContrasenya());
        return mapValores;
    }

    public Trabajador validadorCampos(HttpServletRequest request, Trabajador trab) {
        errores = new ArrayList();
        String nombre = (String) request.getParameter("nombre");
        String apellido1 = (String) request.getParameter("apellido1");
        String apellido2 = (String) request.getParameter("apellido2");
        String fechaNacimiento = (String) request.getParameter("fechaNacimiento");
        String dni = (String) request.getParameter("dni");
        String usuario = (String) request.getParameter("usuario");
        String contrasena = (String) request.getParameter("contrasena");
        String esAdmin = (String) request.getParameter("esAdmin");
        if (trab == null) {
            trab = new Trabajador();
        }
        if (nombre != null && !nombre.equals("")) {
            if (PersonUtils.validadorCampoTexto(nombre)) {
                trab.setNombre(nombre);
            } else {
                errores.add("nombreError");
            }
        } else {
            errores.add(0, "errorGeneral");
        }
        if (apellido1 != null && !apellido1.equals("")) {
            if (PersonUtils.validadorCampoTexto(apellido1)) {
                trab.setApellido1(apellido1);
            } else {
                errores.add("apellido1Error");
            }
        } else {
            errores.add(0, "errorGeneral");
        }
        if (apellido2 != null && !apellido2.equals("")) {
            if (PersonUtils.validadorCampoTexto(apellido1)) {
                trab.setApellido2(apellido2);
            } else {
                errores.add("apellido2Error");
            }
        } else {
            errores.add(0, "errorGeneral");
        }
        if (fechaNacimiento != null && !fechaNacimiento.equals("")) {
            trab.setFechaNacimiento(fechaNacimiento);
        } else {
            errores.add(0, "errorGeneral");
        }
        if (dni != null && !dni.equals("")) {
            if (PersonUtils.isNifNie(dni)) {
                trab.setDni(dni);
            } else {
                errores.add("dniError");
            }
        } else {
            errores.add(0, "errorGeneral");
        }
        if (usuario != null && !usuario.equals("")) {
            trab.setUsuario(usuario);
        } else {
            errores.add(0, "errorGeneral");
        }

        if (contrasena != null && !contrasena.equals("")) {
            trab.setContrasenya(contrasena);
        } else {
            errores.add(0, "errorGeneral");
        }

        if (esAdmin == null) {
            trab.setEsAdmin("NO");
        } else {
            trab.setEsAdmin("SI");
        }

        return trab;
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (SQLException ex) {
            Logger.getLogger(ControladorTrabajador.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (SQLException ex) {
            Logger.getLogger(ControladorTrabajador.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
