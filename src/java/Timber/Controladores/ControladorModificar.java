package Timber.Controladores;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.ArrayList;

import javax.swing.JOptionPane;

import Timber.Controladores.Controlador.seleccionarDispositius;
import Timber.Controladores.Controlador.seleccionarFeina;
import Timber.Controladores.Controlador.seleccionarProducteSuport;
import com.yayotech.vistas.AñadirContacto;
import com.yayotech.vistas.Borrar;
import com.yayotech.vistas.BuscarModificar;
import com.yayotech.vistas.Contactos;
import com.yayotech.vistas.InicioSesion;
import com.yayotech.vistas.Opciones;
import com.yayotech.vistas.Pestañas;
import com.yayotech.vistas.PestañasModificar;
import com.yayotech.vistas.Vista;


public class ControladorModificar {
	String codiPacient ="";
	String direccion="";
	public ActionListener buscarPorTelefono(Vista v, Pestañas p, Borrar b, BuscarModificar modificar, PestañasModificar pm, Opciones o, ModeloMariaDB modelo){
		return new modificarPaciente(v, p, b, modificar, pm, o, modelo);
	}
	
	public ActionListener updatePacienteContacto(PestañasModificar pm, ModeloMariaDB modelo, BuscarModificar bm){
		return new updatePacienteContacto(pm, modelo, bm);
	}
	
	public ActionListener buscarPacienteDNI(PestañasModificar pm, BuscarModificar bm, Vista v, ModeloMariaDB modelo){
		return new buscarPaciente(pm, bm, v, modelo);
	}
	
	public ActionListener updateDependenciaInforme(PestañasModificar pm, ModeloMariaDB modelo, BuscarModificar bm){
		return new updateDependenciaInforme(pm, modelo, bm);
	}
	
	public ActionListener updateFitxaSanitaria(PestañasModificar pm, ModeloMariaDB modelo, BuscarModificar bm){
		return new updateFitxaSanitaria(pm, modelo, bm);
	}
	
	public ActionListener activarHorarioTrabajo(PestañasModificar p){
		return new seleccionarFeina(p);
	}
	
	public ActionListener updateServeiTeleasistencia(PestañasModificar pm, ModeloMariaDB modelo, BuscarModificar bm){
		return new updateServeiTeleasistencia(pm, modelo, bm);
	}
	
	public ActionListener seleccionarCentre(PestañasModificar pm) {
		return new seleccionarCentre(pm);
	}
	
	public ActionListener producteSuport(ModeloMariaDB modelo, PestañasModificar p) {
		return new seleccionarProducteSuport(modelo, p);
	}
	
	public ActionListener seleccionarDispositius(PestañasModificar p) {
		return new seleccionarDispositius(p);
	}
	

	public class seleccionarProducteSuport implements ActionListener {

		private ModeloMariaDB	modelo;
		private PestañasModificar		p;

		public seleccionarProducteSuport(ModeloMariaDB modelo, PestañasModificar p) {
			this.modelo = modelo;
			this.p = p;
		}

		@Override
		public void actionPerformed(ActionEvent arg0) {
			if (p.getRdbtnProducteSuport().isSelected()) {
				p.getChkCadiraRodes().setEnabled(true);
				p.getChkCaminador().setEnabled(true);
				p.getChkMuleta().setEnabled(true);
			} else if (p.getRdbtnAjuda().isSelected()
					|| p.getRdbtnAutonom().isSelected()
					|| p.getRdbtnEnllitament().isSelected()) {
				p.getChkCadiraRodes().setEnabled(false);
				p.getChkCaminador().setEnabled(false);
				p.getChkMuleta().setEnabled(false);
				p.getChkCadiraRodes().setSelected(false);
				p.getChkCaminador().setSelected(false);
				p.getChkMuleta().setSelected(false);
			}
		}
	}
	
	public class buscarPaciente implements ActionListener {
		private PestañasModificar pm;
		private BuscarModificar bm;
		private Vista v;
		private ModeloMariaDB modelo;
		
		public buscarPaciente(PestañasModificar pm, BuscarModificar bm, Vista v, ModeloMariaDB modelo) {
			this.pm = pm;
			this.modelo = modelo;
			this.bm = bm;
			this.v = v;
		}

		@Override
		public void actionPerformed(ActionEvent e) {	
			modelo.setBm(bm);
			modelo.buscarDNIModificar(bm.getTxtDNI().getText());
			
			ArrayList<String> array = new ArrayList<String>(); 
			String codiPacient = modelo.buscarCodiPacient(bm.getTxtDNI().getText());
			
			if(bm.getCboBuscarModificar().getSelectedIndex() == 1){
				//BUSCAR DATOS PARA MODIFICAR PACIENTE
				array = modelo.buscarPacienteDNI(bm.getTxtDNI().getText());
				pm.getTxtEdat().setText(array.get(0));
				if(array.get(1).equalsIgnoreCase("Home")){
					pm.getCboSexe().setSelectedIndex(1);
				}else if(array.get(1).equalsIgnoreCase("Dona")){
					pm.getCboSexe().setSelectedIndex(2);
				}
				pm.getTxtNom().setText(array.get(2));
				pm.getTxtCognoms().setText(array.get(3));
				//FALTA DATA NAIX
				pm.getTxtDni().setText(array.get(5));
				//FALTA ADRECA
				pm.getTxtInstruccions().setText(array.get(7));
				pm.getTxtPoblacio().setText(array.get(8));
				pm.getTxtCodiPostal().setText(array.get(9));
				pm.getTxtTelefon1().setText(array.get(10));
				pm.getTxtTelefon2().setText(array.get(11));
				pm.getTxtEmail().setText(array.get(12));
				
				
				//BUSCAR DATOS PARA MODIFICAR CONTACTO PACIENTE
				array = new ArrayList<String>(); 
				array = modelo.buscarContactoDNI(codiPacient);
				//POR SI DA NULL!!!!!!!!!!!!
				//pm.getTxtNomContacte().setText(array.get(0));
				pm.getTxtRelacio().setText(array.get(1));	
				pm.getTxtPoblacioContacte().setText(array.get(2));
				pm.getTxtCodiPostalContacte().setText(array.get(3));
				pm.getTxtAdrecaContacte().setText(array.get(4));	
				pm.getTxtTelefonContacte().setText(array.get(5));
				pm.getTxtDomiciliContacte().setText(array.get(6));
				if(array.get(7).equalsIgnoreCase("SI")){
					pm.getCboFeina().setSelectedIndex(1);
				}else if(array.get(7).equalsIgnoreCase("NO")){
					pm.getCboFeina().setSelectedIndex(2);
				}
				
				if(array.get(8).equalsIgnoreCase("MATñ")){
					pm.getCboHorariFeina().setSelectedIndex(1);
				}else if(array.get(8).equalsIgnoreCase("TARDA")){
					pm.getCboHorariFeina().setSelectedIndex(2);
				}else if(array.get(8).equalsIgnoreCase("NIT")){
					pm.getCboHorariFeina().setSelectedIndex(3);
				}
				
				pm.getTxtTelefonContacte().setText(array.get(9));
				pm.getTxtEmailContacte().setText(array.get(10));
				if(array.get(11).equalsIgnoreCase("SI")){
					pm.getCboClaus().setSelectedIndex(1);
				}else if(array.get(11).equalsIgnoreCase("NO")){
					pm.getCboClaus().setSelectedIndex(2);
				}
				
		
				//BUSCAR DATOS PARA MODIFICAR DEPENDENCIA PACIENTE
				array = new ArrayList<String>(); 
				array = modelo.buscarDependenciaDNI(codiPacient);
				//FALTA DEAMBULACIO
				if(array.get(1).equalsIgnoreCase("1")){
					pm.getCboGrauDependencia().setSelectedIndex(1);
				}else if(array.get(1).equalsIgnoreCase("2")){
					pm.getCboGrauDependencia().setSelectedIndex(2);
				}else if(array.get(1).equalsIgnoreCase("3")){
					pm.getCboGrauDependencia().setSelectedIndex(3);
				}
				
				if(array.get(2).equalsIgnoreCase("1")){
					pm.getCboNivellDependencia().setSelectedIndex(1);
				}else if(array.get(2).equalsIgnoreCase("2")){
					pm.getCboNivellDependencia().setSelectedIndex(2);
				}
				
				if(array.get(3).equalsIgnoreCase("SI")){
					pm.getCboPrestacioEconomica().setSelectedIndex(1);
				}else if(array.get(3).equalsIgnoreCase("NO")){
					pm.getCboPrestacioEconomica().setSelectedIndex(2);
				}
				

				//BUSCAR DATOS PARA MODIFICAR INFORME SOCIAL PACIENTE
				array = new ArrayList<String>(); 
				array = modelo.buscarInformeSocialDNI(codiPacient);
				if(array.get(0).equalsIgnoreCase("Solo/a")){
					pm.getCboSituacioConvivencial().setSelectedIndex(1);
				}else if(array.get(0).equalsIgnoreCase("Acompañado/a")){
					pm.getCboSituacioConvivencial().setSelectedIndex(2);
				}
				
				if(array.get(1).equalsIgnoreCase("Mañanas")){
					pm.getCboHoresSol().setSelectedIndex(1);
				}else if(array.get(1).equalsIgnoreCase("Tardes")){
					pm.getCboHoresSol().setSelectedIndex(2);
				}else if(array.get(1).equalsIgnoreCase("Noches")){
					pm.getCboHoresSol().setSelectedIndex(3);
				}
				
				if(array.get(2).equalsIgnoreCase("Buena")){
					pm.getCboRelacioFamilia().setSelectedIndex(1);
				}else if(array.get(2).equalsIgnoreCase("Aceptable")){
					pm.getCboRelacioFamilia().setSelectedIndex(2);
				}else if(array.get(2).equalsIgnoreCase("Mala")){
					pm.getCboRelacioFamilia().setSelectedIndex(3);
				}else if(array.get(2).equalsIgnoreCase("Sin relaciñn")){
					pm.getCboRelacioFamilia().setSelectedIndex(4);
				}
				
				if(array.get(3).equalsIgnoreCase("Diñriamente")){
					pm.getCboVisites().setSelectedIndex(1);
				}else if(array.get(3).equalsIgnoreCase("Semanalmente")){
					pm.getCboVisites().setSelectedIndex(2);
				}else if(array.get(3).equalsIgnoreCase("Mensualmente")){
					pm.getCboVisites().setSelectedIndex(3);
				}else if(array.get(3).equalsIgnoreCase("Poco")){
					pm.getCboVisites().setSelectedIndex(4);
				}else if(array.get(3).equalsIgnoreCase("Nunca")){
					pm.getCboVisites().setSelectedIndex(5);
				}
				//FALTA COMPATIBILITAT
				
				
				//BUSCAR DATOS PARA MODIFICAR FICHA SANITARIA PACIENTE
				array = new ArrayList<String>(); 
				array = modelo.buscarFitxaSanitariaDNI(codiPacient);
				pm.getTxtEstatura().setText(array.get(0));
				pm.getTxtPes().setText(array.get(1));
				if(array.get(2).equalsIgnoreCase("Buen estado")){
					pm.getCboEstatActual().setSelectedIndex(1);
				}else if(array.get(2).equalsIgnoreCase("Enfermo")){
					pm.getCboEstatActual().setSelectedIndex(2);
				}
				pm.getTxtPatologia().setText(array.get(3));
				pm.getTxtAlergies().setText(array.get(4));
				//FALTAN SISTEMAS AFECTADOS CUERPO
				pm.getTxtAreaMalalties().setText(array.get(6));
				pm.getTxtAreaMedicacio().setText(array.get(7));
				if(array.get(8).equalsIgnoreCase("Buena")){
					pm.getCboCapacitatCognitiva().setSelectedIndex(1);
				}else if(array.get(8).equalsIgnoreCase("Regular")){
					pm.getCboCapacitatCognitiva().setSelectedIndex(2);
				}else if(array.get(8).equalsIgnoreCase("Mala")){
					pm.getCboCapacitatCognitiva().setSelectedIndex(3);
				}
				pm.getTxtDificultats().setText(array.get(9));
				pm.getTxtVacunes().setText(array.get(10));
				//FALTA CENTRO SANITARIO
				
				
				array = new ArrayList<String>(); 
				array = modelo.buscarServeiTeleasitenciaDNI(codiPacient);
				//BUSCAR DATOS PARA MODIFICAR SERVICIO TELEASISTENCIA PACIENTE
				pm.getTxtNomContractant().setText(array.get(0));
				pm.getTxtCognomsContractant().setText(array.get(1));
				pm.getTxtDNIContractant().setText(array.get(2));
				pm.getTxtTelefonContractant().setText(array.get(3));
				pm.getTxtProvinciaContractant().setText(array.get(4));
				pm.getTxtCodiPostalContractant().setText(array.get(5));
				//FALTA SERVEIS BASICS
				//FALTA MODALITATS
				//FALTA PRESTACIONSADICIONALS

			}
		}
	}	
	
	public class seleccionarFeina implements ActionListener {
		private PestañasModificar p;
		
		public seleccionarFeina(PestañasModificar p) {
			this.p = p;
		}
		
		@Override
		public void actionPerformed(ActionEvent arg0) {
			if(p.getCboFeina().getSelectedIndex() == 1){
				p.getCboHorariFeina().setEnabled(true);
			}else{
				p.getCboHorariFeina().setEnabled(false);
			}
		}
	}

	public class seleccionarDispositius implements ActionListener {

		private PestañasModificar	p;

		public seleccionarDispositius(PestañasModificar p) {
			this.p = p;
		}

		@Override
		public void actionPerformed(ActionEvent arg0) {
			if (p.getChkAlarmesDomotiques().isSelected()) {
				p.getChkDetectorCaigudes().setEnabled(true);
				p.getChkDetectorFum().setEnabled(true);
				p.getChkDetectorGas().setEnabled(true);
				p.getChkAperturaNevera().setEnabled(true);
				p.getChkSensorsMoviment().setEnabled(true);
			} else {
				p.getChkDetectorCaigudes().setEnabled(false);
				p.getChkDetectorFum().setEnabled(false);
				p.getChkDetectorGas().setEnabled(false);
				p.getChkAperturaNevera().setEnabled(false);
				p.getChkSensorsMoviment().setEnabled(false);
				p.getChkDetectorCaigudes().setSelected(false);
				p.getChkDetectorFum().setSelected(false);
				p.getChkDetectorGas().setSelected(false);
				p.getChkAperturaNevera().setSelected(false);
				p.getChkSensorsMoviment().setSelected(false);
			}
		}
	}
	
	
	public class seleccionarCentre implements ActionListener {

		private PestañasModificar	p;
		public seleccionarCentre(PestañasModificar p) {
			this.p = p;
		}

		@Override
		public void actionPerformed(ActionEvent arg0) {
			if (p.getRdbtnCentrePublic().isSelected()) {
				p.getTxtCAP().setEnabled(true);
				p.getTxtTelefonCAP().setEnabled(true);
				p.getTxtMetge().setEnabled(true);
				p.getTxtHospital().setEnabled(true);
			} else {
				p.getTxtCAP().setEnabled(false);
				p.getTxtTelefonCAP().setEnabled(false);
				p.getTxtMetge().setEnabled(false);
				p.getTxtHospital().setEnabled(false);
				p.getTxtCAP().setText("");
				p.getTxtTelefonCAP().setText("");
				p.getTxtHospital().setText("");
				p.getTxtMetge().setText("");
			}
			if (p.getRdbtnPrivat().isSelected()) {
				p.getTxtMutua().setEnabled(true);
				p.getTxtHospital2().setEnabled(true);
			} else {
				p.getTxtMutua().setEnabled(false);
				p.getTxtHospital2().setEnabled(false);
				p.getTxtMutua().setText("");
				p.getTxtHospital2().setText("");
			}
		}
	}
	
	
	public class updatePacienteContacto implements ActionListener{
		PestañasModificar p;
		ModeloMariaDB modelo;
		BuscarModificar bm;
		
		public updatePacienteContacto(PestañasModificar pm, ModeloMariaDB modelo, BuscarModificar bm) {
			this.p = pm;
			this.modelo = modelo;
			this.bm = bm;
		}

		@Override
		public void actionPerformed(ActionEvent e) {
			String regexEdad = "\\d{3}";
			String regexNombrePaciente = "^[\\p{L} .'-]+$";
			String regexApellidoPaciente= "^[\\p{L} .'-]+$";
			String regexDireccion = "^[\\p{L} .'-]+$"; 
			String regexNumerosDireccion = "\\d{1,4}";
			String regexInstrucciones = "^[a-zA-Z0-9_]*$";
			String regexPoblacion = "[a-zA-Z]+";
			String regexCodiPostal = "\\d{5}";
			String regexEmail = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
			String regexTelefonos = "\\d{9}";
			String regexEstatura = "[0-9]+";
			String regexBanco1 = "\\d{4}";
			String regexBanco3 = "\\d{2}";
			String regexBanco4 = "\\d{10}";
			
			if (p.getCboSexe().getSelectedIndex() == 0) {
				JOptionPane.showMessageDialog(p,
						"Selecciona el sexo del paciente." , "Error", JOptionPane.ERROR_MESSAGE);
			} else if (p.getTxtEdat().getText().length() <= 0) {
				JOptionPane.showMessageDialog(p,
						"Introduce la edad del paciente." , "Error", JOptionPane.ERROR_MESSAGE);
			} else if (p.getTxtNom().getText().length() <= 0) {
				JOptionPane.showMessageDialog(p,
						"Introduce el nombre del paciente." , "Error", JOptionPane.ERROR_MESSAGE);
			} else if (p.getTxtCognoms().getText().length() <= 0) {
				JOptionPane.showMessageDialog(p,
						"Introduce los apellidos del paciente." , "Error", JOptionPane.ERROR_MESSAGE);
			} else if (p.getCboAnyNaix().getSelectedIndex() == 0) {
				JOptionPane.showMessageDialog(p,
						"Selecciona el año de nacimiento." , "Error", JOptionPane.ERROR_MESSAGE);
			} else if (p.getCboMesNaix().getSelectedIndex() == 0) {
				JOptionPane.showMessageDialog(p,
						"Selecciona el mes de nacimiento." , "Error", JOptionPane.ERROR_MESSAGE) ;
			} else if (p.getCboDiaNaix().getSelectedIndex() == 0) {
				JOptionPane.showMessageDialog(p,
						"Selecciona el dia de nacimiento." , "Error", JOptionPane.ERROR_MESSAGE);
			} else if (p.getTxtDni().getText().length() <= 0) {
				JOptionPane.showMessageDialog(p,
						"Introduce el DNI del paciente." , "Error", JOptionPane.ERROR_MESSAGE);
			} else if (p.getCboTipoVia().getSelectedIndex() == 0) {
				JOptionPane.showMessageDialog(p, "Selecciona el tipo de via." , "Error", JOptionPane.ERROR_MESSAGE);
			} else if (p.getTxtAdreca().getText().length() <= 0) {
				JOptionPane.showMessageDialog(p,
						"Introduce la direcciñn del paciente." , "Error", JOptionPane.ERROR_MESSAGE);
			} else if (p.getTxtNumeroAdreca().getText().length() <= 0) {
				JOptionPane.showMessageDialog(p,
						"Introduce el nñmero de la vivienda del paciente." , "Error", JOptionPane.ERROR_MESSAGE);
			} else if (p.getTxtPisAdreca().getText().length() <= 0) {
				JOptionPane.showMessageDialog(p,
						"Introduce el nñmero del piso del paciente." , "Error", JOptionPane.ERROR_MESSAGE);
			} else if (p.getTxtPortaCarrer().getText().length() <= 0) {
				JOptionPane.showMessageDialog(p,
						"Introduce el nñmero de puerta del paciente." , "Error", JOptionPane.ERROR_MESSAGE);
			} else if (p.getTxtInstruccions().getText().length() <= 0) {
				JOptionPane.showMessageDialog(p, "Introduce las instrucciones." , "Error", JOptionPane.ERROR_MESSAGE);
			} else if (p.getTxtPoblacio().getText().length() <= 0) {
				JOptionPane.showMessageDialog(p,
						"Introduce la poblacion del paciente." , "Error", JOptionPane.ERROR_MESSAGE);
			} else if (p.getTxtCodiPostal().getText().length() <= 0) {
				JOptionPane.showMessageDialog(p,
						"Introduce el codigo postal del paciente." , "Error", JOptionPane.ERROR_MESSAGE);
			} else if (p.getTxtTelefon1().getText().length() <= 0) {
				JOptionPane.showMessageDialog(p,
						"Introduce el telefono 1 del paciente." , "Error", JOptionPane.ERROR_MESSAGE);
			} else if (p.getTxtTelefon2().getText().length() <= 0) {
				JOptionPane.showMessageDialog(p,
						"Introduce el telefono 2 del paciente." , "Error", JOptionPane.ERROR_MESSAGE);
			} else if (p.getTxtEmail().getText().length() <= 0) {
				JOptionPane.showMessageDialog(p,
						"Introduce el email del paciente." , "Error", JOptionPane.ERROR_MESSAGE);
			} else if (p.getTxtNomContacte().getText().length() <= 0) {
				JOptionPane.showMessageDialog(p,
						"Introduce el nombre del contacto." , "Error", JOptionPane.ERROR_MESSAGE);
			} else if (p.getCboSexeContacte().getSelectedIndex() == 0) {
				JOptionPane.showMessageDialog(p,
						"Selecciona el sexo del contacto." , "Error", JOptionPane.ERROR_MESSAGE);
			} else if (p.getTxtMobilContacte().getText().length() <= 0) {
				JOptionPane.showMessageDialog(p,
						"Introduce el movil del contacto." , "Error", JOptionPane.ERROR_MESSAGE);
			} else if (p.getTxtRelacio().getText().length() <= 0) {
				JOptionPane.showMessageDialog(p,
						"Introduce la relaciñn con el paciente." , "Error", JOptionPane.ERROR_MESSAGE);
			} else if (p.getTxtPoblacioContacte().getText().length() <= 0) {
				JOptionPane.showMessageDialog(p,
						"Introduce la poblaciñn del contacto." , "Error", JOptionPane.ERROR_MESSAGE);
			} else if (p.getTxtCodiPostalContacte().getText().length() <= 0) {
				JOptionPane.showMessageDialog(p,
						"Introduce el codigo postal del contacto." , "Error", JOptionPane.ERROR_MESSAGE);
			} else if (p.getTxtDomiciliContacte().getText().length() <= 0) {
				JOptionPane.showMessageDialog(p,
						"Introduce el domicilio del contacto." , "Error", JOptionPane.ERROR_MESSAGE);
			} else if (p.getTxtAdrecaContacte().getText().length() <= 0) {
				JOptionPane.showMessageDialog(p,
						"Introduce la direccion del contacto." , "Error", JOptionPane.ERROR_MESSAGE);
			} else if (p.getTxtTelefonContacte().getText().length() <= 0) {
				JOptionPane.showMessageDialog(p,
						"Introduce el telefono del contacto." , "Error", JOptionPane.ERROR_MESSAGE);
			} else if (p.getCboFeina().getSelectedIndex() == 0) {
				JOptionPane.showMessageDialog(p,
						"Introduce el apartado de trabajo." , "Error", JOptionPane.ERROR_MESSAGE);
			} else if (p.getTxtEmailContacte().getText().length() <= 0) {
				JOptionPane.showMessageDialog(p,
						"Introduce el email del contacto." , "Error", JOptionPane.ERROR_MESSAGE);
			} else if (p.getCboClaus().getSelectedIndex() == 0) {
				JOptionPane.showMessageDialog(p,
						"Selecciona el apartado de llaves." , "Error", JOptionPane.ERROR_MESSAGE);
			} else {
				if(!p.getTxtNom().getText().matches(regexNombrePaciente)){
					JOptionPane.showMessageDialog(p, "Nombre no valido." , "Error", JOptionPane.ERROR_MESSAGE);
				}else if(!p.getTxtCognoms().getText().matches(regexApellidoPaciente)){
					JOptionPane.showMessageDialog(p, "Apellidos invalidos." , "Error", JOptionPane.ERROR_MESSAGE);
				}else if(!p.getTxtEdat().getText().matches(regexEdad) && !(Integer.parseInt(p.getTxtEdat().getText()) < 120)){
					JOptionPane.showMessageDialog(p, "Edad invalida." , "Error", JOptionPane.ERROR_MESSAGE);
				}else if(!p.getTxtAdreca().getText().matches(regexDireccion)){
					JOptionPane.showMessageDialog(p, "Direcciñn incorrecta." , "Error", JOptionPane.ERROR_MESSAGE);
				}else if(!p.getTxtNumeroAdreca().getText().matches(regexNumerosDireccion)){
					JOptionPane.showMessageDialog(p, "Numero direcciñn incorrecto." , "Error", JOptionPane.ERROR_MESSAGE);
				}else if(!p.getTxtPisAdreca().getText().matches(regexNumerosDireccion)){
					JOptionPane.showMessageDialog(p, "Nñmero de piso incorrecto." , "Error", JOptionPane.ERROR_MESSAGE);
				}else if(!p.getTxtPortaCarrer().getText().matches(regexNumerosDireccion)){
					JOptionPane.showMessageDialog(p, "Puerta incorrecta." , "Error", JOptionPane.ERROR_MESSAGE);
				}else if(!p.getTxtInstruccions().getText().matches(regexInstrucciones)){
					JOptionPane.showMessageDialog(p, "Instrucciones no validas." , "Error", JOptionPane.ERROR_MESSAGE);
				}else if(!p.getTxtPoblacio().getText().matches(regexInstrucciones)){
					JOptionPane.showMessageDialog(p, "Poblaciñn no vñlida.." , "Error", JOptionPane.ERROR_MESSAGE);
				}else if(!p.getTxtCodiPostal().getText().matches(regexCodiPostal)){
					JOptionPane.showMessageDialog(p, "Cñdigo postal no vñlido." , "Error", JOptionPane.ERROR_MESSAGE);
				}else if(!p.getTxtTelefon1().getText().matches(regexTelefonos)){
					JOptionPane.showMessageDialog(p, "Telñfono no vñlido." , "Error", JOptionPane.ERROR_MESSAGE);
				}else if(!p.getTxtTelefon2().getText().matches(regexTelefonos)){
					JOptionPane.showMessageDialog(p, "Telñfono no vñlido." , "Error", JOptionPane.ERROR_MESSAGE);
				}else if(!p.getTxtEmail().getText().matches(regexEmail)){
					JOptionPane.showMessageDialog(p, "Email no vñlido." , "Error", JOptionPane.ERROR_MESSAGE);
				}else{
					if(modelo.isNifNie(p.getTxtDni().getText()) == true){
						codiPacient = modelo.buscarCodiPacient(bm.getTxtDNI().getText());
						direccion = p.getCboTipoVia().getSelectedItem().toString()+ "/" + p.getTxtAdreca().getText() + " Nñ "+ p.getTxtNumeroAdreca().getText() + " " + p.getTxtPisAdreca().getText() + "ñ - "+ p.getTxtPortaCarrer().getText() + "a";
						modelo.updatePaciente(Integer.parseInt(p.getTxtEdat().getText()),p.getCboSexe().getSelectedItem().toString(), p.getTxtNom().getText(), p.getTxtCognoms().getText(), Integer.parseInt(p.getCboAnyNaix().getSelectedItem().toString()), Integer.parseInt(p.getCboMesNaix().getSelectedItem().toString()),Integer.parseInt(p.getCboDiaNaix().getSelectedItem().toString()), p.getTxtDni().getText(), direccion, p.getTxtInstruccions().getText(), p.getTxtPoblacio().getText(), p.getTxtCodiPostal().getText(), p.getTxtTelefon1().getText(), p.getTxtTelefon2().getText(), p.getTxtEmail().getText(), codiPacient);
					}else{
						JOptionPane.showMessageDialog(p, "DNI no valido.");
					}		
				}
				
				if(!p.getTxtNomContacte().getText().matches(regexNombrePaciente)){
					JOptionPane.showMessageDialog(p, "Nombre no vñlido." , "Error", JOptionPane.ERROR_MESSAGE);
				}else if(!p.getTxtRelacio().getText().matches(regexPoblacion)){
					JOptionPane.showMessageDialog(p, "Relaciñn con el paciente no vñlida." , "Error", JOptionPane.ERROR_MESSAGE);
				}else if(!p.getTxtPoblacioContacte().getText().matches(regexInstrucciones)){
					JOptionPane.showMessageDialog(p, "Poblaciñn no vñlida." , "Error", JOptionPane.ERROR_MESSAGE);
				}else if(!p.getTxtCodiPostalContacte().getText().matches(regexCodiPostal)){
					JOptionPane.showMessageDialog(p, "Codigo postal no vñlido." , "Error", JOptionPane.ERROR_MESSAGE);
				}else if(!p.getTxtAdrecaContacte().getText().matches(regexInstrucciones)){
					JOptionPane.showMessageDialog(p, "Direcciñn no vñlida." , "Error", JOptionPane.ERROR_MESSAGE);
				}else if(!p.getTxtTelefonContacte().getText().matches(regexTelefonos)){
					JOptionPane.showMessageDialog(p, "Telefono no vñlido." , "Error", JOptionPane.ERROR_MESSAGE);
				}else if(!p.getTxtDomiciliContacte().getText().matches(regexInstrucciones)){
					JOptionPane.showMessageDialog(p, "Domicilio invalido." , "Error", JOptionPane.ERROR_MESSAGE);
				}else if(!p.getTxtMobilContacte().getText().matches(regexTelefonos)){
					JOptionPane.showMessageDialog(p, "Telefono movil no vñlido." , "Error", JOptionPane.ERROR_MESSAGE);
				}else if(!p.getTxtEmail().getText().matches(regexEmail)){
					JOptionPane.showMessageDialog(p, "Email contacto no vñlido." , "Error", JOptionPane.ERROR_MESSAGE);
				}else{
					codiPacient = modelo.buscarCodiPacient(bm.getTxtDNI().getText());
					modelo.updateContacto(p.getTxtNomContacte().getText(), p.getCboSexeContacte().getSelectedItem().toString(), p.getTxtRelacio().getText(), p.getTxtPoblacioContacte().getText(), p.getTxtCodiPostalContacte().getText(), p.getTxtAdrecaContacte().getText(), p.getTxtTelefonContacte().getText(), p.getTxtDomiciliContacte().getText(), p.getCboFeina().getSelectedItem().toString(), p.getCboHorariFeina().getSelectedItem().toString(), p.getTxtMobilContacte().getText(), p.getTxtEmailContacte().getText(), p.getCboClaus().getSelectedItem().toString(), codiPacient);
				}
			}
		}
	}
	
	public class updateDependenciaInforme implements ActionListener {
		PestañasModificar p;
		ModeloMariaDB modelo;
		BuscarModificar bm;
		
		public updateDependenciaInforme(PestañasModificar pm, ModeloMariaDB modelo, BuscarModificar bm) {
			this.p = pm;
			this.modelo = modelo;
			this.bm = bm;
		}
		
		@Override
		public void actionPerformed(ActionEvent arg0) {
			String deambulacio= "";			
			if(p.getRdbtnProducteSuport().isSelected()){
				if(p.getChkMuleta().isSelected() && p.getChkCaminador().isSelected() && p.getChkCadiraRodes().isSelected()){
					deambulacio = p.getRdbtnProducteSuport().getText() + "-" + p.getChkMuleta().getText() + "/" + p.getChkCaminador().getText() + "/" + p.getChkCadiraRodes().getText();
				}else if(p.getChkMuleta().isSelected() && p.getChkCaminador().isSelected()){
					deambulacio = p.getRdbtnProducteSuport().getText() + "-" + p.getChkMuleta().getText() + "/" + p.getChkCaminador().getText(); 
				}else if(p.getChkMuleta().isSelected() && p.getChkCadiraRodes().isSelected()){
					deambulacio = p.getRdbtnProducteSuport().getText() + "-" + p.getChkMuleta().getText() + "/" + p.getChkCadiraRodes().getText();
				}else if(p.getChkCaminador().isSelected() && p.getChkCadiraRodes().isSelected()){
					deambulacio = p.getRdbtnProducteSuport().getText() + "-" + p.getChkCaminador().getText() + "/" + p.getChkCadiraRodes().getText();
		 	    }else if(p.getChkMuleta().isSelected()){
					deambulacio = p.getRdbtnProducteSuport().getText() + "-" + p.getChkMuleta().getText();
				}else if(p.getChkCaminador().isSelected()){
					deambulacio = p.getRdbtnProducteSuport().getText() + "-" + p.getChkCaminador().getText();
				}else if(p.getChkCadiraRodes().isSelected()){
					deambulacio = p.getRdbtnProducteSuport().getText() + "-" + p.getChkCadiraRodes().getText();
				}
			}else if(p.getRdbtnAjuda().isSelected()){
				deambulacio = p.getRdbtnAjuda().getText();
			}else if(p.getRdbtnAutonom().isSelected()){
				deambulacio = p.getRdbtnAutonom().getText();
			}else if(p.getRdbtnEnllitament().isSelected()){
				deambulacio = p.getRdbtnEnllitament().getText();
			}
			
			
			//Recuperamos la compatibilidad con otros servicios
			String compatibilitat="";
			if(p.getChkCentreDia().isSelected()){
				compatibilitat += p.getChkCentreDia().getText();
			}
			if(p.getChkCentreNit().isSelected()){
				compatibilitat += p.getChkCentreNit().getText();
			}
			if(p.getChkAjudaDomicili().isSelected()){
				compatibilitat += p.getChkAjudaDomicili().getText();
			}

			if (p.getCboGrauDependencia().getSelectedIndex() != 0
					&& p.getCboNivellDependencia().getSelectedIndex() != 0
					&& p.getCboPrestacioEconomica().getSelectedIndex() != 0
					&& p.getCboSituacioConvivencial().getSelectedIndex() != 0
					&& p.getCboHoresSol().getSelectedIndex() != 0
					&& p.getCboRelacioFamilia().getSelectedIndex() != 0
					&& p.getCboVisites().getSelectedIndex() != 0) {
				if (!p.getRdbtnAjuda().isSelected()
						&& !p.getRdbtnAutonom().isSelected()
						&& !p.getRdbtnEnllitament().isSelected()) {
					if (p.getRdbtnProducteSuport().isSelected()) {
						if (p.getChkMuleta().isSelected()
								|| p.getChkCadiraRodes().isSelected()
								|| p.getChkCaminador().isSelected()) {
							String codiPacient = modelo.buscarCodiPacient(bm.getTxtDNI().getText());
							modelo.updateDependencia(deambulacio, p.getCboGrauDependencia().getSelectedItem().toString(), p.getCboNivellDependencia().getSelectedItem().toString(), p.getCboPrestacioEconomica().getSelectedItem().toString(), codiPacient);
							modelo.updateInformeSocial(p.getCboSituacioConvivencial().getSelectedItem().toString(), p.getCboHoresSol().getSelectedItem().toString(), p.getCboRelacioFamilia().getSelectedItem().toString(), p.getCboVisites().getSelectedItem().toString(), compatibilitat, codiPacient);
						} else {
							JOptionPane.showMessageDialog(p, "Introduce todos los datos." , "Error", JOptionPane.ERROR_MESSAGE);
						}
					}
				} else {
					String codiPacient = modelo.buscarCodiPacient(bm.getTxtDNI().getText());
					modelo.updateDependencia(deambulacio, p.getCboGrauDependencia().getSelectedItem().toString(), p.getCboNivellDependencia().getSelectedItem().toString(), p.getCboPrestacioEconomica().getSelectedItem().toString(), codiPacient);
					modelo.updateInformeSocial(p.getCboSituacioConvivencial().getSelectedItem().toString(), p.getCboHoresSol().getSelectedItem().toString(), p.getCboRelacioFamilia().getSelectedItem().toString(), p.getCboVisites().getSelectedItem().toString(), compatibilitat, codiPacient);
				}
			} else JOptionPane.showMessageDialog(p,"Introduce todos los datos." , "Error", JOptionPane.ERROR_MESSAGE);
		}
	}

	public class updateFitxaSanitaria implements ActionListener {
		PestañasModificar p;
		ModeloMariaDB modelo;
		BuscarModificar bm;
		
		public updateFitxaSanitaria(PestañasModificar pm, ModeloMariaDB modelo, BuscarModificar bm) {
			this.p = pm;
			this.modelo = modelo;
			this.bm = bm;
		}
		
		@Override
		public void actionPerformed(ActionEvent arg0) {
			String regexNombrePaciente = "^[\\p{L} .'-]+$";
			String regexInstrucciones = "^[a-zA-Z0-9_]*$";
			String regexTelefonos = "\\d{9}";
			String regexEstatura = "[0-9]+";
			
			if(p.getTxtEstatura().getText().length() <= 0){
				JOptionPane.showMessageDialog(p, "Introduce la estatura." , "Error", JOptionPane.ERROR_MESSAGE);
			}else if(p.getTxtPes().getText().length() <= 0){
				JOptionPane.showMessageDialog(p, "Introduce el peso." , "Error", JOptionPane.ERROR_MESSAGE);
			}else if(p.getCboEstatActual().getSelectedIndex() == 0){
				JOptionPane.showMessageDialog(p, "Introduce el estado actual." , "Error", JOptionPane.ERROR_MESSAGE);
			}else if(p.getTxtPatologia().getText().length() <= 0){
				JOptionPane.showMessageDialog(p, "Introduce la patologia." , "Error", JOptionPane.ERROR_MESSAGE);
			}else if(!(p.getChkOsteomuscular().isSelected() || p.getChkSistNervios().isSelected() || p.getChkRespiratori().isSelected() || p.getChkCardiovascular().isSelected() || p.getChkDigestiu().isSelected() || p.getChkReproductorExcretor().isSelected())){
				JOptionPane.showMessageDialog(p, "Introduce un sistema del cuerpo humano." , "Error", JOptionPane.ERROR_MESSAGE);
			}else if(p.getTxtAreaMalalties().getText().length() <= 0){
				JOptionPane.showMessageDialog(p, "Introduce las enfermedades." , "Error", JOptionPane.ERROR_MESSAGE);
			}else if(p.getTxtAreaMedicacio().getText().length() <= 0){
				JOptionPane.showMessageDialog(p, "Introduce la medicacion." , "Error", JOptionPane.ERROR_MESSAGE);
			}else if(p.getCboCapacitatCognitiva().getSelectedIndex() == 0){
				JOptionPane.showMessageDialog(p, "Selecciona la capacidad cognitiva." , "Error", JOptionPane.ERROR_MESSAGE);
			}else if(p.getTxtVacunes().getText().length() <= 0){
				JOptionPane.showMessageDialog(p, "Introduce las vacunas." , "Error", JOptionPane.ERROR_MESSAGE);
			}else if(!p.getRdbtnCentrePublic().isSelected() && !p.getRdbtnPrivat().isSelected()){
				JOptionPane.showMessageDialog(p, "Selecciona algun centro." , "Error", JOptionPane.ERROR_MESSAGE);
			}else if(p.getRdbtnCentrePublic().isSelected() && (p.getTxtHospital().getText().length() <= 0	&& p.getTxtMetge().getText().length() <= 0
					&& p.getTxtCAP().getText().length() <= 0 && p.getTxtTelefonCAP().getText().length() <= 0)){
				JOptionPane.showMessageDialog(p, "Introduce los datos del centro publico." , "Error", JOptionPane.ERROR_MESSAGE);
			}else if(p.getRdbtnPrivat().isSelected() && (p.getTxtMutua().getText().length() <= 0 && p.getTxtHospital2().getText().length() <= 0)){
				JOptionPane.showMessageDialog(p, "Introduce los datos del centro privado." , "Error", JOptionPane.ERROR_MESSAGE);
			}else{
				String sist="";
				if(p.getChkOsteomuscular().isSelected()){
					sist+=p.getChkOsteomuscular().getText();
				}
				if(p.getChkReproductorExcretor().isSelected()){
					sist+=p.getChkReproductorExcretor().getText();
				}
				if(p.getChkRespiratori().isSelected()){
					sist+=p.getChkRespiratori().getText();
				}
				if(p.getChkDigestiu().isSelected()){
					sist+=p.getChkDigestiu().getText();
				}
				if(p.getChkCardiovascular().isSelected()){
					sist+=p.getChkCardiovascular().getText();
				}
				if(p.getChkSistNervios().isSelected()){
					sist+=p.getChkSistNervios().getText();
				}
				
				String centreSanitari="";
				if(p.getRdbtnCentrePublic().isSelected()){
					centreSanitari = p.getTxtCAP().getText() + "/" + p.getTxtTelefonCAP().getText() + "/" + p.getTxtMetge().getText() + "/" + p.getTxtHospital().getText();
				}else if(p.getRdbtnPrivat().isSelected()){
					centreSanitari = p.getTxtMutua().getText() + "/" + p.getTxtHospital2().getText();
				}
				
				
				if(!p.getTxtEstatura().getText().matches(regexEstatura)){
					JOptionPane.showMessageDialog(p, "Estatura invñlida." , "Error", JOptionPane.ERROR_MESSAGE);
				}else if(!p.getTxtPes().getText().matches(regexEstatura)){
					JOptionPane.showMessageDialog(p, "Peso invñlido." , "Error", JOptionPane.ERROR_MESSAGE);
				}else if(!p.getTxtPatologia().getText().matches(regexInstrucciones)){
					JOptionPane.showMessageDialog(p, "Patologia no vñlida." , "Error", JOptionPane.ERROR_MESSAGE);
				}else if(!p.getTxtAlergies().getText().matches(regexInstrucciones)){
					JOptionPane.showMessageDialog(p, "Alergias no validas." , "Error", JOptionPane.ERROR_MESSAGE);
				}else if(!p.getTxtAreaMalalties().getText().matches(regexInstrucciones)){
					JOptionPane.showMessageDialog(p, "Enfermedades no vñlidas." , "Error", JOptionPane.ERROR_MESSAGE);
				}else if(!p.getTxtAreaMedicacio().getText().matches(regexInstrucciones)){
					JOptionPane.showMessageDialog(p, "Campo de medicaciones no vñlidas." , "Error", JOptionPane.ERROR_MESSAGE);
				}else if(!p.getTxtDificultats().getText().matches(regexInstrucciones)){
					JOptionPane.showMessageDialog(p, "Campo de dificultades no vñlido." , "Error", JOptionPane.ERROR_MESSAGE);
				}else if(!p.getTxtVacunes().getText().matches(regexInstrucciones)){
					JOptionPane.showMessageDialog(p, "Campo de vacunas no vñlido." , "Error", JOptionPane.ERROR_MESSAGE);
				}else if(!p.getTxtCAP().getText().matches(regexInstrucciones)){
					JOptionPane.showMessageDialog(p, "Nombre CAP no vñlido." , "Error", JOptionPane.ERROR_MESSAGE);
				}else if(!p.getTxtTelefonCAP().getText().matches(regexTelefonos)){
					JOptionPane.showMessageDialog(p, "Telefono CAP no vñlido." , "Error", JOptionPane.ERROR_MESSAGE);
				}else if(!p.getTxtMetge().getText().matches(regexNombrePaciente)){
					JOptionPane.showMessageDialog(p, "Nombre del mñdico no vñlido." , "Error", JOptionPane.ERROR_MESSAGE);
				}else if(!p.getTxtHospital().getText().matches(regexInstrucciones)){
					JOptionPane.showMessageDialog(p, "Nombre hospital pñblico no vñlido." , "Error", JOptionPane.ERROR_MESSAGE);
				}else if(!p.getTxtMutua().getText().matches(regexInstrucciones)){
					JOptionPane.showMessageDialog(p, "Nombre mñtua no vñlido." , "Error", JOptionPane.ERROR_MESSAGE);
				}else if(!p.getTxtHospital2().getText().matches(regexInstrucciones)){
					JOptionPane.showMessageDialog(p, "Nombre hospital privado no vñlido." , "Error", JOptionPane.ERROR_MESSAGE);
				}else{
					String codiPacient = modelo.buscarCodiPacient(bm.getTxtDNI().getText());
					modelo.updateFitxaSanitaria(Float.parseFloat(p.getTxtEstatura().getText()), Float.parseFloat(p.getTxtPes().getText()), p.getCboEstatActual().getSelectedItem().toString(), p.getTxtPatologia().getText(), p.getTxtAlergies().getText(), sist, p.getTxtAreaMalalties().getText(), p.getTxtAreaMedicacio().getText(), p.getCboCapacitatCognitiva().getSelectedItem().toString(), p.getTxtDificultats().getText(), p.getTxtVacunes().getText(), centreSanitari, codiPacient);
				}
			}
		}
	}
	
	public class updateServeiTeleasistencia implements ActionListener {
		PestañasModificar p;
		ModeloMariaDB modelo;
		BuscarModificar bm;
		
		public updateServeiTeleasistencia(PestañasModificar pm, ModeloMariaDB modelo, BuscarModificar bm) {
			this.p = pm;
			this.modelo = modelo;
			this.bm = bm;
		}
		
		@Override
		public void actionPerformed(ActionEvent arg0) {
			String regexNombrePaciente = "^[\\p{L} .'-]+$";
			String regexInstrucciones = "^[a-zA-Z0-9_]*$";
			String regexCodiPostal = "\\d{5}";
			String regexTelefonos = "\\d{9}";
			String regexBanco1 = "\\d{4}";
			String regexBanco3 = "\\d{2}";
			String regexBanco4 = "\\d{10}";
			
			if(p.getCboAnyDataAlta().getSelectedIndex() == 0){
				JOptionPane.showMessageDialog(p, "Introduce el año de alta." , "Error", JOptionPane.ERROR_MESSAGE);
			}else if(p.getCboMesDataAlta().getSelectedIndex() == 0){
				JOptionPane.showMessageDialog(p, "Introduce el mes de alta." , "Error", JOptionPane.ERROR_MESSAGE);
			}else if(p.getCboDiaDataAlta().getSelectedIndex() == 0){
				JOptionPane.showMessageDialog(p, "Introduce el dia de alta." , "Error", JOptionPane.ERROR_MESSAGE);
			}else if(!(p.getChkUnicaQuota().isSelected() || p.getChkDiposit().isSelected() || p.getChkManteniment().isSelected())){				
				JOptionPane.showMessageDialog(p, "Selecciona una cuota del servicio." , "Error", JOptionPane.ERROR_MESSAGE);
			}else if(p.getTxtBanc1().getText().length()<=0 || p.getTxtBanc2().getText().length()<=0 || p.getTxtBanc3().getText().length()<=0 || p.getTxtBanc4().getText().length()<=0){
				JOptionPane.showMessageDialog(p, "Introduce los datos del banco." , "Error", JOptionPane.ERROR_MESSAGE);
			}else if(p.getTxtNomContractant().getText().length()<=0){
				JOptionPane.showMessageDialog(p, "Introduce el nombre del contractante." , "Error", JOptionPane.ERROR_MESSAGE);
			}else if(p.getTxtCognomsContractant().getText().length()<=0){
				JOptionPane.showMessageDialog(p, "Introuce los apellidos del contractante." , "Error", JOptionPane.ERROR_MESSAGE);
			}else if(p.getTxtTelefonContractant().getText().length()<=0){
				JOptionPane.showMessageDialog(p, "Introduce el telefono de contractante." , "Error", JOptionPane.ERROR_MESSAGE);
			}else if(p.getTxtCodiPostalContractant().getText().length()<=0){
				JOptionPane.showMessageDialog(p, "Introduce el codigo postal del contractante." , "Error", JOptionPane.ERROR_MESSAGE);
			}else if(p.getTxtProvinciaContractant().getText().length()<=0){
				JOptionPane.showMessageDialog(p, "Introduce la provincia del contractante." , "Error", JOptionPane.ERROR_MESSAGE);
			}else if(p.getTxtDNIContractant().getText().length()<=0){
				JOptionPane.showMessageDialog(p, "Introduce el DNI del contractante." , "Error", JOptionPane.ERROR_MESSAGE);
			}else if(!(p.getChkDispositiuFixe().isSelected() || p.getChkDispositiuInalambric().isSelected())){
				JOptionPane.showMessageDialog(p, "Selecciona un servicio basico a contratar." , "Error", JOptionPane.ERROR_MESSAGE);
			}else if(!(p.getRdbtnUnitatMovil().isSelected() || p.getRdbtnNoUnitatMovil().isSelected())){
				JOptionPane.showMessageDialog(p, "Selecciona una modalidad del STA a contractar." , "Error", JOptionPane.ERROR_MESSAGE);
			}else {
				//Quotes servei
				String quotes="";
				if(p.getChkUnicaQuota().isSelected()){
					quotes = p.getChkUnicaQuota().getText();
				}else if(p.getChkDiposit().isSelected()){
					quotes = p.getChkDiposit().getText();
				}else if(p.getChkManteniment().isSelected()){
					quotes = p.getChkManteniment().getText();
				}
				
				
				//DadesBanc
				String dadesBanc = p.getTxtBanc1().getText() + "-" + p.getTxtBanc2().getText() + "-" + p.getTxtBanc3().getText() + "-" + p.getTxtBanc4().getText();
						
				//Serveis Basics
				String serveisBasics="";
				if(p.getChkDispositiuFixe().isSelected()){
					serveisBasics +=p.getChkDispositiuFixe().getText();
				}
				if(p.getChkDispositiuInalambric().isSelected()){
					serveisBasics +=p.getChkDispositiuInalambric().getText();
				}
				
				//Modalitats
				String modalitats="";
				if(p.getRdbtnUnitatMovil().isSelected()){
					modalitats += p.getRdbtnUnitatMovil().getText();
				}
				if(p.getRdbtnNoUnitatMovil().isSelected()){
					modalitats += p.getRdbtnNoUnitatMovil().getText();
				}
				
				//PrestacionsAdicionals
				String prestacionsAdicionals="";
				if(p.getChkAgendesSeguiment().isSelected()){
					prestacionsAdicionals += p.getChkAgendesSeguiment().getText();
				}
				if(p.getChkActivitats().isSelected()){
					prestacionsAdicionals += p.getChkActivitats().getText();
				}
				if(p.getChkCustodiaClaus().isSelected()){
					prestacionsAdicionals += p.getChkCustodiaClaus().getText();
				}
				if(p.getChkTelelocalitzacio().isSelected()){
					prestacionsAdicionals += p.getChkTelelocalitzacio().getText();
				}
				if(p.getChkAlarmesDomotiques().isSelected()){
					
					prestacionsAdicionals += p.getChkAlarmesDomotiques().getText() + "-";
					
					if(p.getChkSensorsMoviment().isSelected()){
						prestacionsAdicionals += p.getChkSensorsMoviment().getText();
					}
					if(p.getChkAperturaNevera().isSelected()){
						prestacionsAdicionals += p.getChkAperturaNevera().getText();
					}
					if(p.getChkDetectorCaigudes().isSelected()){
						prestacionsAdicionals += p.getChkDetectorCaigudes().getText();
					}
					if(p.getChkDetectorGas().isSelected()){
						prestacionsAdicionals += p.getChkDetectorGas().getText();
					}
					if(p.getChkDetectorFum().isSelected()){
						prestacionsAdicionals += p.getChkDetectorFum().getText();
					}
				}
				
				if(!p.getTxtBanc1().getText().matches(regexBanco1)){
					JOptionPane.showMessageDialog(p, "Datos banco invalidos." , "Error", JOptionPane.ERROR_MESSAGE);
				}else if(!p.getTxtBanc2().getText().matches(regexBanco1)){
					JOptionPane.showMessageDialog(p, "Datos banco invalidos." , "Error", JOptionPane.ERROR_MESSAGE);
				}else if(!p.getTxtBanc3().getText().matches(regexBanco3)){
					JOptionPane.showMessageDialog(p, "Datos banco invalidos." , "Error", JOptionPane.ERROR_MESSAGE);
				}else if(!p.getTxtBanc4().getText().matches(regexBanco4)){
					JOptionPane.showMessageDialog(p, "Datos banco invalidos." , "Error", JOptionPane.ERROR_MESSAGE);
				}else if(!p.getTxtNomContractant().getText().matches(regexNombrePaciente)){
					JOptionPane.showMessageDialog(p, "Nombre contractante no vñlido." , "Error", JOptionPane.ERROR_MESSAGE);
				}else if(!p.getTxtCognomsContractant().getText().matches(regexNombrePaciente)){
					JOptionPane.showMessageDialog(p, "Apellidos contractante no vñlidos." , "Error", JOptionPane.ERROR_MESSAGE);
				}else if(!p.getTxtProvinciaContractant().getText().matches(regexInstrucciones)){
					JOptionPane.showMessageDialog(p, "Provincia contractante no vñlida." , "Error", JOptionPane.ERROR_MESSAGE);
				}else if(!p.getTxtTelefonContractant().getText().matches(regexTelefonos)){
					JOptionPane.showMessageDialog(p, "Telefono invñlido." , "Error", JOptionPane.ERROR_MESSAGE);
				}else if(!p.getTxtCodiPostalContractant().getText().matches(regexCodiPostal)){
					JOptionPane.showMessageDialog(p, "Cñdigo postal incorrecto." , "Error", JOptionPane.ERROR_MESSAGE);
				}else{
					if(modelo.isNifNie(p.getTxtDNIContractant().getText()) == true){
						String codiPacient = modelo.buscarCodiPacient(bm.getTxtDNI().getText());
						modelo.updateServeiTeleasistencia(Integer.parseInt(p.getCboAnyDataAlta().getSelectedItem().toString()), Integer.parseInt(p.getCboMesDataAlta().getSelectedItem().toString()), Integer.parseInt(p.getCboDiaDataAlta().getSelectedItem().toString()), quotes, dadesBanc, p.getTxtNomContractant().getText(), p.getTxtCognomsContractant().getText(), p.getTxtDNIContractant().getText(), p.getTxtTelefonContacte().getText(), p.getTxtProvinciaContractant().getText(), p.getTxtCodiPostalContractant().getText(), serveisBasics, modalitats, prestacionsAdicionals, codiPacient);
					}else{
						JOptionPane.showMessageDialog(p, "DNI no valido.");
					}
				}
			}
		}
	}
	

	public class modificarPaciente implements ActionListener {

		private Pestañas p;
		private Borrar b;
		private BuscarModificar modificar;
		private PestañasModificar pm;
		private Opciones o;
		private Vista v;
		private ModeloMariaDB modelo;
		

		public modificarPaciente(Vista v, Pestañas p, Borrar b, BuscarModificar modificar, PestañasModificar pm, Opciones o, ModeloMariaDB modelo) {
			this.v = v;
			this.p = p;
			this.b = b;
			this.modificar = modificar;
			this.modelo = modelo;
			this.pm = pm;
			this.o = o;
		}

		@Override
		public void actionPerformed(ActionEvent e) {			
			modificar.setVisible(false);
			pm.setVisible(true);
			v.setEnabled(true);
		}
	}
}
	
