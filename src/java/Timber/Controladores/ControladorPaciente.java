/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Timber.Controladores;

import Timber.DAO.PacienteDAO;
import Timber.DAO.PersonUtils;
import Timber.Modelo.Contacto;
import Timber.Modelo.Dependencia;
import Timber.Modelo.FichaSanitaria;
import Timber.Modelo.InformeSocial;
import Timber.Modelo.Paciente;
import Timber.Modelo.ServicioTeleasistencia;
import java.io.IOException;
import java.sql.Date;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Alba
 */
@WebServlet(name = "ControladorPaciente", urlPatterns = {"/ControladorPaciente"})
public class ControladorPaciente extends HttpServlet {

    ArrayList errores = new ArrayList();
    static Paciente paciente = new Paciente();

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, SQLException {

        //Gestión de la botonera
        String accion = (String) request.getParameter("accion");
        String tab = (String) request.getParameter("tab");
        String goingTo = (String) request.getParameter("goingTo");
        PacienteDAO pacienteDAO = new PacienteDAO();
        Paciente pac = new Paciente();

        if (accion != null && accion.equals("borrar")) {
            pac.setDni((String) request.getParameter("dni"));
            pacienteDAO.borrarPaciente(pac);
        } else if (accion != null && (accion.equals("consultar") || accion.equals("modificar"))) {
            HashMap mapValores = setearCampos((String) request.getParameter("dni"));
            request.setAttribute("info", mapValores);
            if (accion.equals("consultar")) {
                request.setAttribute("readOnly", "readonly");
            }
        } else if (accion != null && accion.equals("alta")) {
            if (tab != null && tab.equals("datpers")) {
                paciente = validadorDatosPersonales(request, paciente);
                Paciente aux = pacienteDAO.obtenerPaciente(pac);
                request.setAttribute("tab", "datpers");
                if (errores.isEmpty()) {
                    if (aux.getDni() != null) {
                        errores.add("dniExiste");
                    }
                    if (errores.isEmpty()) {
                        request.setAttribute("tab", "infdep");
                        pacienteDAO.insertarPaciente(paciente);
                    } else {
                        request.setAttribute("errores", errores);
                    }
                } else {
                    request.setAttribute("errores", errores);
                }
            } else if (tab != null && tab.equals("infdep")) {
                //paciente = validadorInformeDependecia(request, paciente, new Dependencia(), new InformeSocial());
                
                    request.setAttribute("tab", "fichsan");
                if (errores.isEmpty()) {
                } else {
                    request.setAttribute("errores", errores);
                }

            } else if (tab != null && tab.equals("fichsan")) {
                //paciente = validadorFichaSanitaria(request, paciente, new FichaSanitaria());
                
                    request.setAttribute("tab", "teleas");
                if (errores.isEmpty()) {
                } else {
                    request.setAttribute("errores", errores);
                }

            } else if (tab != null && tab.equals("teleas")) {
                //paciente = validadorServicioTA(request, paciente, new ServicioTeleasistencia());
                
                        tab = null;
                        
                if (errores.isEmpty()) {
                    /*if (pacienteDAO.obtenerDniContratador(paciente.getServicio().getDniContratador())) {
                        errores.add("dniExiste");
                    }*/
                    if (errores.isEmpty()) {
                    } else {
                        request.setAttribute("errores", errores);
                    }
                } else {
                    request.setAttribute("errores", errores);
                }
            }
        } else if (accion != null && accion.equals("modificando")) {
            pac.setDni((String) request.getParameter("dni"));
            pac = pacienteDAO.obtenerPaciente(pac);

            Paciente aux = validadorDatosPersonales(request, pac);
            if (errores.isEmpty()) {
                aux = pacienteDAO.obtenerPaciente(pac);
                if (!pac.getDni().equals(aux.getDni())) {
                    if (aux != null) {
                        errores.add("dniExiste");
                    }
                }
                if (errores.isEmpty()) {
                    long idPaciente = pac.getIdPaciente();
                    pacienteDAO.updatePaciente(validadorDatosPersonales(request, pac), idPaciente);
                    request.setAttribute("pageToGo", "mainTrabajadores.jsp");
                    request.getRequestDispatcher("ControladorLanding?goingTo=trabajador").forward(request, response);
                } else {
                    request.setAttribute("errores", errores);
                    HashMap mapValores = setearCampos((String) request.getParameter("dni"));
                    request.setAttribute("info", mapValores);
                }
            } else {
                request.setAttribute("errores", errores);
                HashMap mapValores = setearCampos((String) request.getParameter("dni"));
                request.setAttribute("info", mapValores);
            }

        }
        if (tab == null) {
            request.setAttribute("tab", "datpers");
        }
        request.getRequestDispatcher("gestpacientes.jsp").forward(request, response);

    }

    private HashMap setearCampos(String dni) throws SQLException {
        Paciente pac = new Paciente();
        pac.setDni(dni);
        PacienteDAO pacienteDAO = new PacienteDAO();
        HashMap mapValores = new HashMap();

        pac = pacienteDAO.obtenerPaciente(pac);

        mapValores.put("nombre", pac.getNombre());

        return mapValores;
    }

    private Paciente validadorContacto(HttpServletRequest request, Paciente pac, Contacto cont) {
        errores = new ArrayList();
        String nombre = (String) request.getParameter("nombre");
        String apellido1 = (String) request.getParameter("apellido1");
        String apellido2 = (String) request.getParameter("apellido2");
        String sexo = (String) request.getParameter("sexo");
        String relacionConPaciente = (String) request.getParameter("relacionConPaciente");
        String direccion = (String) request.getParameter("direccion");
        String poblacion = (String) request.getParameter("poblacion");
        String cPostal = (String) request.getParameter("cPostal");
        String email = (String) request.getParameter("email");
        String tlf = (String) request.getParameter("tlf");
        String trabajo = (String) request.getParameter("trabajo");
        String horarioDesde = (String) request.getParameter("horarioDesde");
        String horarioHasta = (String) request.getParameter("horarioHasta");
        String tieneLlaves = (String) request.getParameter("tieneLlaves");

        cont.setIdPaciente(pac.getIdPaciente());

        if (nombre != null && !nombre.equals("")) {
            if (PersonUtils.validadorCampoTexto(nombre)) {
                cont.setNombre(nombre);
            } else {
                errores.add("nombreError");
            }
        } else {
            errores.add(0, "errorGeneral");
        }
        if (apellido1 != null && !apellido1.equals("")) {
            if (PersonUtils.validadorCampoTexto(apellido1)) {
                cont.setApellido1(apellido1);
            } else {
                errores.add("apellido1Error");
            }
        } else {
            errores.add(0, "errorGeneral");
        }
        if (apellido2 != null && !apellido2.equals("")) {
            if (PersonUtils.validadorCampoTexto(apellido1)) {
                cont.setApellido2(apellido2);
            } else {
                errores.add("apellido2Error");
            }
        } else {
            errores.add(0, "errorGeneral");
        }

        if (sexo != null && !sexo.equals("")) {
            cont.setSexo(sexo);
        } else {
            errores.add(0, "errorGeneral");
        }

        if (direccion != null && !direccion.equals("")) {
            cont.setDireccion(direccion);
        } else {
            errores.add(0, "errorGeneral");
        }

        if (poblacion != null && !poblacion.equals("")) {
            if (PersonUtils.validadorCampoTexto(poblacion)) {
                cont.setPoblacion(poblacion);
            } else {
                errores.add("poblacionError");
            }
        } else {
            errores.add(0, "errorGeneral");
        }

        if (cPostal != null && !cPostal.equals("")) {
            if (PersonUtils.validadorCampoNumerico(cPostal)) {
                cont.setcPostal(cPostal);
            }
        } else {
            errores.add(0, "errorGeneral");
        }

        if (email != null && !email.equals("")) {
            if (PersonUtils.validadorCampoEmail(email)) {
                cont.setEmail(email);
            } else {
                errores.add("emailError");
            }
        }

        if (tlf != null && !tlf.equals("")) {
            if (PersonUtils.validadorCampoNumerico(tlf)) {
                cont.setTelefono(tlf);
            } else {
                errores.add("fijoError");
            }
        }

        cont.setRelacionPaciente(relacionConPaciente);

        if (trabajo != null) {
            cont.setTrabajo("SI");
            if (horarioDesde != null && horarioHasta != null) {
                cont.setHorarioTrabajo(horarioDesde + "-" + horarioHasta);
            } else {
                errores.add("errorGeneral");
            }
        } else {
            cont.setTrabajo("NO");
        }

        if (tieneLlaves != null) {
            cont.setLlaves("SI");
        } else {
            cont.setLlaves("NO");
        }
        ArrayList contactos = pac.getContactos();
        contactos.add(cont);
        pac.setContactos(contactos);

        return pac;
    }

    private Paciente validadorServicioTA(HttpServletRequest request, Paciente pac, ServicioTeleasistencia sta) {
        errores = new ArrayList();
        String fechaAltaSTA = (String) request.getParameter("fechaAltaSTA");
        String cuotaUnica = (String) request.getParameter("cuotaUnica");
        String cuotaDeposito = (String) request.getParameter("cuotaDeposito");
        String cuotaMensual = (String) request.getParameter("cuotaMensual");
        String entidadBancaria = (String) request.getParameter("entidadBancaria");
        String sucursalBancaria = (String) request.getParameter("sucursalBancaria");
        String controlBancario = (String) request.getParameter("controlBancario");
        String cuentaBancaria = (String) request.getParameter("cuentaBancaria");
        String nombre = (String) request.getParameter("nombre");
        String apellido1 = (String) request.getParameter("apellido1");
        String apellido2 = (String) request.getParameter("apellido2");
        String dni = (String) request.getParameter("dni");
        String poblacion = (String) request.getParameter("poblacion");
        String cPostal = (String) request.getParameter("cPostal");
        String tlf = (String) request.getParameter("tlf");
        String tipoDispositivo = (String) request.getParameter("tipoDispositivo");
        String unidadMobil = (String) request.getParameter("unidadMobil");
        String agendaSeguimiento = (String) request.getParameter("prestacionAdicional1");
        String actividadesComplementarias = (String) request.getParameter("prestacionAdicional2");
        String custodiaLlaves = (String) request.getParameter("prestacionAdicional3");
        String telelocalizacion = (String) request.getParameter("prestacionAdicional4");
        String alarmasDomoticas = (String) request.getParameter("prestacionAdicional5");
        String sensoresMovimientos = (String) request.getParameter("prestacionAdicional51");
        String aberturaNevera = (String) request.getParameter("prestacionAdicional52");
        String deteccionCaidas = (String) request.getParameter("prestacionAdicional53");
        String deteccionGas = (String) request.getParameter("prestacionAdicional54");
        String deteccionHumo = (String) request.getParameter("prestacionAdicional55");

        if (fechaAltaSTA != null && !fechaAltaSTA.equals("")) {
            int año = Integer.parseInt(fechaAltaSTA.split("-")[0]);
            int mes = Integer.parseInt(fechaAltaSTA.split("-")[1]);
            int dia = Integer.parseInt(fechaAltaSTA.split("-")[2]);
            sta.setFecha(new Date(año, mes, dia));
        } else {
            errores.add(0, "errorGeneral");
        }

        if (cuotaUnica != null || cuotaDeposito != null || cuotaMensual != null) {
            sta.setCuotasServicio(cuotaUnica + ":" + cuotaDeposito + ":" + cuotaMensual);
        }

        if (entidadBancaria != null && !entidadBancaria.equals("") && sucursalBancaria != null && !sucursalBancaria.equals("") && controlBancario != null && !controlBancario.equals("") && cuentaBancaria != null && !cuentaBancaria.equals("")) {
            if (PersonUtils.validadorCampoNumerico(entidadBancaria) && PersonUtils.validadorCampoNumerico(sucursalBancaria) && PersonUtils.validadorCampoNumerico(controlBancario) && PersonUtils.validadorCampoNumerico(cuentaBancaria)) {
                sta.setDatosBanco(entidadBancaria + "-" + sucursalBancaria + "-" + controlBancario + "-" + cuentaBancaria);
            } else {
                errores.add("cuentaError");
            }
        } else {
            errores.add(0, "errorGeneral");
        }

        if (nombre != null && !nombre.equals("")) {
            if (PersonUtils.validadorCampoTexto(nombre)) {
                sta.setNombreContratador(nombre);
            } else {
                errores.add("nombreError");
            }
        } else {
            errores.add(0, "errorGeneral");
        }
        if (apellido1 != null && !apellido1.equals("")) {
            if (PersonUtils.validadorCampoTexto(apellido1)) {
                sta.setApellido1Contratador(apellido1);
            } else {
                errores.add("apellido1Error");
            }
        } else {
            errores.add(0, "errorGeneral");
        }
        if (apellido2 != null && !apellido2.equals("")) {
            if (PersonUtils.validadorCampoTexto(apellido1)) {
                sta.setApellido2Contratador(apellido2);
            } else {
                errores.add("apellido2Error");
            }
        } else {
            errores.add(0, "errorGeneral");
        }

        if (dni != null && !dni.equals("")) {
            if (PersonUtils.isNifNie(dni)) {
                sta.setDniContratador(dni);
            } else {
                errores.add("dniError");
            }
        } else {
            errores.add(0, "errorGeneral");
        }

        if (poblacion != null && !poblacion.equals("")) {
            if (PersonUtils.validadorCampoTexto(poblacion)) {
                sta.setProvinciaContratador(poblacion);
            } else {
                errores.add("poblacionError");
            }
        } else {
            errores.add(0, "errorGeneral");
        }

        if (cPostal != null && !cPostal.equals("")) {
            if (PersonUtils.validadorCampoNumerico(cPostal)) {
                sta.setCpostalContratador(cPostal);
            }
        } else {
            errores.add(0, "errorGeneral");
        }

        if (tlf != null && !tlf.equals("")) {
            if (PersonUtils.validadorCampoNumerico(tlf)) {
                pac.setFijo(Integer.parseInt(tlf));
            } else {
                errores.add("fijoError");
            }
        } else {
            errores.add(0, "errorGeneral");
        }

        sta.setServiciosBasicos(tipoDispositivo);
        sta.setModalidades(unidadMobil);

        String prestacionesAdicionales = "";
        if (agendaSeguimiento != null || actividadesComplementarias != null || custodiaLlaves != null || telelocalizacion != null) {
            prestacionesAdicionales = agendaSeguimiento + ":" + actividadesComplementarias + ":" + custodiaLlaves + ":" + telelocalizacion;
            sta.setPrestacionesAdicionales(prestacionesAdicionales);
        }

        if (alarmasDomoticas != null) {
            if (sensoresMovimientos != null || aberturaNevera != null || deteccionCaidas != null || deteccionGas != null || deteccionHumo != null) {
                prestacionesAdicionales += alarmasDomoticas + ":" + sensoresMovimientos + ":" + aberturaNevera + ":" + deteccionCaidas + ":" + deteccionGas + ":" + deteccionHumo;
            } else {
                errores.add("errorGeneral");
            }
        }
        pac.setServicio(sta);

        return pac;
    }

    private Paciente validadorFichaSanitaria(HttpServletRequest request, Paciente pac, FichaSanitaria ficha) {
        errores = new ArrayList();
        String altura = (String) request.getParameter("altura");
        String peso = (String) request.getParameter("peso");
        String estadoActual = (String) request.getParameter("estadoActual");
        String patologias = (String) request.getParameter("patologias");
        String alergias = (String) request.getParameter("alergias");
        String osteomuscular = (String) request.getParameter("osteomuscular");
        String cardiovascular = (String) request.getParameter("cardiovascular");
        String respiratorio = (String) request.getParameter("respiratorio");
        String digestivo = (String) request.getParameter("digestivo");
        String nervioso = (String) request.getParameter("nervioso");
        String reproductorExcretor = (String) request.getParameter("reproductorExcretor");
        String cognitiva = (String) request.getParameter("cognitiva");
        String enfermedades = (String) request.getParameter("enfermedades");
        String medicacion = (String) request.getParameter("medicacion");
        String deficiencias = (String) request.getParameter("deficiencias");
        String vacunas = (String) request.getParameter("vacunas");
        String centroSanitario = (String) request.getParameter("centroSanitario");
        String cap = (String) request.getParameter("cap");
        String capTlf = (String) request.getParameter("capTlf");
        String medico = (String) request.getParameter("medico");
        String hospital = (String) request.getParameter("hospital");
        String mutua = (String) request.getParameter("mutua");
        String hospitalPrivado = (String) request.getParameter("hospitalPrivado");

        if (altura != null && !altura.equals("")) {
            if (PersonUtils.validadorCampoDecimal(altura)) {
                ficha.setEstatura(Double.parseDouble(altura));
            } else {
                errores.add("alturaError");
            }
        } else {
            errores.add(0, "errorGeneral");
        }

        if (peso != null && !peso.equals("")) {
            ficha.setPeso(Double.parseDouble(peso));
        } else {
            errores.add(0, "errorGeneral");
        }

        ficha.setEstadoActual(estadoActual);

        if (patologias != null && !patologias.equals("")) {
            if (PersonUtils.validadorCampoTexto(patologias)) {
                ficha.setPatologia(patologias);
            } else {
                errores.add("patologiaError");
            }
        } else {
            errores.add(0, "errorGeneral");
        }

        if (alergias != null && !alergias.equals("")) {
            if (PersonUtils.validadorCampoTexto(alergias)) {
                ficha.setAlergias(alergias);
            } else {
                errores.add("alergiaError");
            }
        } else {
            errores.add(0, "errorGeneral");
        }

        if (osteomuscular != null || cardiovascular != null || respiratorio != null || digestivo != null || nervioso != null || reproductorExcretor != null) {
            ficha.setSistemasAfectados(osteomuscular + ":" + cardiovascular + ":" + respiratorio + ":" + digestivo + ":" + nervioso + ":" + reproductorExcretor);
        } else {
            ficha.setSistemasAfectados("Ninguno");
        }

        ficha.setCapacidadCognitiva(cognitiva);

        if (enfermedades != null && !enfermedades.equals("")) {
            if (PersonUtils.validadorCampoTexto(enfermedades)) {
                ficha.setEnfermedades(enfermedades);
            } else {
                errores.add("enfermedadError");
            }
        } else {
            errores.add(0, "errorGeneral");
        }

        if (medicacion != null && !medicacion.equals("")) {
            ficha.setMedicacion(medicacion);
        } else {
            errores.add(0, "errorGeneral");
        }

        if (deficiencias != null && !deficiencias.equals("")) {
            ficha.setDificultades(deficiencias);
        } else {
            errores.add(0, "errorGeneral");
        }

        if (vacunas != null && !vacunas.equals("")) {
            ficha.setVacunas(vacunas);
        } else {
            errores.add(0, "errorGeneral");
        }

        if (centroSanitario != null) {
            if (centroSanitario.equalsIgnoreCase("privado")) {
                if (mutua == null || hospitalPrivado == null) {
                    errores.add(0, "errorGeneral");
                } else {
                    ficha.setCentroSanitario(mutua + ":" + hospitalPrivado);
                }
            } else {
                if (cap == null || capTlf == null || medico == null || hospital == null) {
                    errores.add(0, "errorGeneral");
                } else {
                    ficha.setCentroSanitario(cap + ":" + capTlf + ":" + medico + ":" + hospital);
                }
            }
        } else {
            errores.add(0, "errorGeneral");
        }
        pac.setFicha(ficha);
        return pac;

    }

    private Paciente validadorInformeDependecia(HttpServletRequest request, Paciente pac, Dependencia dep, InformeSocial inf) {
        errores = new ArrayList();
        String deambulacion = (String) request.getParameter("deambulacio");
        String baston = (String) request.getParameter("baston");
        String andador = (String) request.getParameter("andador");
        String silla = (String) request.getParameter("silla");
        String gradoDependecia = (String) request.getParameter("gradoDependecia");
        String gradoDependeciaLegal = (String) request.getParameter("gradoDependeciaLegal");
        String prestacionEconomica = (String) request.getParameter("prestacionEconomica");
        String situacionConvivencia = (String) request.getParameter("situacionConvivencia");
        String horarioSolo = (String) request.getParameter("horarioSolo");
        String relacion = (String) request.getParameter("relacion");
        String visitas = (String) request.getParameter("visitas");
        String ayudaDomicilio = (String) request.getParameter("ayudaDomicilio");
        String centroDia = (String) request.getParameter("centroDia");
        String centroNoche = (String) request.getParameter("centroNoche");

        dep.setIdPaciente(pac.getIdPaciente());
        if (deambulacion != null && !deambulacion.equals("")) {
            if (deambulacion.equalsIgnoreCase("Autónomo con soporte")) {
                if (baston != null || andador != null || silla != null) {
                    dep.setDeambulacion(deambulacion + ":" + baston + ":" + andador + ":" + silla);
                } else {
                    errores.add(0, "errorGeneral");
                }
            } else {
                dep.setDeambulacion(deambulacion);
            }
        } else {
            errores.add(0, "errorGeneral");
        }

        dep.setGradoDependencia(gradoDependecia);
        dep.setNivelDependencia(gradoDependeciaLegal);
        dep.setPrestacionEconomica(prestacionEconomica);

        inf.setIdPaciente(pac.getIdPaciente());
        inf.setConvivencia(situacionConvivencia);
        inf.setHorarioSol(horarioSolo);
        inf.setRelacionFamilia(relacion);
        inf.setVisitas(visitas);
        inf.setCompatibilidad("Compatibilidad con otros servicios:" + ayudaDomicilio + "," + centroDia + "," + centroNoche);

        pac.setDependencia(dep);
        pac.setInforme(inf);

        return pac;
    }

    private Paciente validadorDatosPersonales(HttpServletRequest request, Paciente pac) {
        errores = new ArrayList();
        String nombre = (String) request.getParameter("nombre");
        String apellido1 = (String) request.getParameter("apellido1");
        String apellido2 = (String) request.getParameter("apellido2");
        String sexo = (String) request.getParameter("sexo");
        String fechaNacimiento = (String) request.getParameter("fechaNacimiento");
        String edad = (String) request.getParameter("edad");
        String dni = (String) request.getParameter("dni");
        String direccion = (String) request.getParameter("direccion");
        String poblacion = (String) request.getParameter("poblacion");
        String cPostal = (String) request.getParameter("cPostal");
        String referencias = (String) request.getParameter("masInfo");
        String email = (String) request.getParameter("email");
        String fijo = (String) request.getParameter("fijo");
        String mobil = (String) request.getParameter("mobil");
        if (pac == null) {
            pac = new Paciente();
        }
        if (nombre != null && !nombre.equals("")) {
            if (PersonUtils.validadorCampoTexto(nombre)) {
                pac.setNombre(nombre);
            } else {
                errores.add("nombreError");
            }
        } else {
            errores.add(0, "errorGeneral");
        }
        if (apellido1 != null && !apellido1.equals("")) {
            if (PersonUtils.validadorCampoTexto(apellido1)) {
                pac.setApellido1(apellido1);
            } else {
                errores.add("apellido1Error");
            }
        } else {
            errores.add(0, "errorGeneral");
        }
        if (apellido2 != null && !apellido2.equals("")) {
            if (PersonUtils.validadorCampoTexto(apellido1)) {
                pac.setApellido2(apellido2);
            } else {
                errores.add("apellido2Error");
            }
        } else {
            errores.add(0, "errorGeneral");
        }
        if (fechaNacimiento != null && !fechaNacimiento.equals("")) {
            int año = Integer.parseInt(fechaNacimiento.split("-")[0]);
            int mes = Integer.parseInt(fechaNacimiento.split("-")[1]);
            int dia = Integer.parseInt(fechaNacimiento.split("-")[2]);
            pac.setFechaNacimiento(new Date(año, mes, dia));
        } else {
            errores.add(0, "errorGeneral");
        }
        if (dni != null && !dni.equals("")) {
            if (PersonUtils.isNifNie(dni)) {
                pac.setDni(dni);
            } else {
                errores.add("dniError");
            }
        } else {
            errores.add(0, "errorGeneral");
        }
        if (edad != null && !edad.equals("")) {
            if (PersonUtils.validadorCampoNumerico(edad)) {
                pac.setEdad(Integer.parseInt(edad));
            } else {
                errores.add("edadError");
            }
        } else {
            errores.add(0, "errorGeneral");
        }

        if (sexo != null && !sexo.equals("")) {
            pac.setSexo(sexo);
        } else {
            errores.add(0, "errorGeneral");
        }

        if (direccion != null && !direccion.equals("")) {
            pac.setDireccion(direccion);
        } else {
            errores.add(0, "errorGeneral");
        }

        if (poblacion != null && !poblacion.equals("")) {
            if (PersonUtils.validadorCampoTexto(poblacion)) {
                pac.setPoblacion(poblacion);
            } else {
                errores.add("poblacionError");
            }
        } else {
            errores.add(0, "errorGeneral");
        }

        if (cPostal != null && !cPostal.equals("")) {
            if (PersonUtils.validadorCampoNumerico(cPostal)) {
                pac.setcPostal(cPostal);
            }
        } else {
            errores.add(0, "errorGeneral");
        }

        if (referencias != null && !referencias.equals("")) {
            pac.setReferencias(referencias);
        }

        if (email != null && !email.equals("")) {
            if (PersonUtils.validadorCampoEmail(email)) {
                pac.setEmail(email);
            } else {
                errores.add("emailError");
            }
        }

        if (fijo != null && !fijo.equals("")) {
            if (PersonUtils.validadorCampoNumerico(fijo)) {
                pac.setFijo(Integer.parseInt(fijo));
            } else {
                errores.add("fijoError");
            }
        }

        if (mobil != null && !mobil.equals("")) {
            if (PersonUtils.validadorCampoNumerico(mobil)) {
                pac.setMobil(Integer.parseInt(mobil));
            } else {
                errores.add("mobilError");
            }
        }

        return pac;
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (SQLException ex) {
            Logger.getLogger(ControladorPaciente.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (SQLException ex) {
            Logger.getLogger(ControladorPaciente.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
