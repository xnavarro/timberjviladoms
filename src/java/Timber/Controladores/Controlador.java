package Timber.Controladores;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.File;
import java.util.ArrayList;

import javax.swing.JOptionPane;

import com.yayotech.vistas.AñadirContacto;
import com.yayotech.vistas.Borrar;
import com.yayotech.vistas.BuscarModificar;
import com.yayotech.vistas.Contactos;
import com.yayotech.vistas.InicioSesion;
import com.yayotech.vistas.Opciones;
import com.yayotech.vistas.Pestañas;
import com.yayotech.vistas.PestañasModificar;
import com.yayotech.vistas.Vista;

public class Controlador {
	String direccion="", nomContra="",cognomContra="", dniContra="",contraseña="",contraseñaLowerCase="",codiPacient="";

	public ActionListener ventanaOpciones(PestañasModificar pm, Pestañas p, Borrar b, Opciones o,  BuscarModificar modificar, Vista v){
		return new ventanaOpciones(pm, p, b, o, modificar, v);
	}
	
	public ActionListener enProceso(Vista v){
		return new enProceso(v);
	}

	public ActionListener home(PestañasModificar pm, Pestañas p, AñadirContacto ac, Borrar b, Opciones o, BuscarModificar bm, Contactos contacto){
		return new home(pm, p, ac, b, o, bm, contacto);
	}
	
	public ActionListener cambiarDatosDB(Opciones o){
		return new cambiarDatosDB(o);
	}
	public ActionListener modificarIPTelefono(BajaPaciente baja, CuentaPaciente alta, Opciones o){
		return new modificarIPTelefono(baja, alta, o);
	}
	
	public ActionListener buscarModificar(BuscarModificar modificar, Vista v, Borrar b, Pestañas p, Opciones o){
		return new buscarModificar(modificar, v, b, p, o);
	}
	
	public WindowAdapter cerrarBuscarModificar(Vista v, BuscarModificar modificar){
		return new cerrarBuscarModificar(v, modificar);
	}
	
	public ActionListener examinar(Opciones o, ModeloMariaDB modelo) {
		return new examinar(o, modelo);
	}
	
	public ActionListener examinar2(Opciones o, ModeloMariaDB modelo) {
		return new examinar2(o, modelo);
	}
	
	public ActionListener iniciarSesion(ModeloMariaDB modelo,InicioSesion inicio, Vista v) {
		return new inicioSesion(modelo, inicio, v);
	}
	
	public ActionListener darBaja(Borrar b, ModeloMariaDB modelo) {
		return new darBaja(b, modelo);
	}
	
	public ActionListener buscarDni(Borrar b, ModeloMariaDB modelo){
		return new buscarPacienteDarBaja(b, modelo);
	}

	public ActionListener salirBorrar(Borrar b, ModeloMariaDB modelo){
		return new salirBorrar(b, modelo);
	}
	
	public ActionListener salirInicioSesion(ModeloMariaDB modelo,
			InicioSesion inicio, Vista v) {
		return new salirInicioSesion(modelo, inicio, v);
	}
	
	public ActionListener activarHorarioTrabajo(Pestañas p){
		return new seleccionarFeina(p);
	}

	public WindowAdapter salirAplicacion(InicioSesion inicio, Vista v) {
		return new salirAplicacion(inicio,v);
	}

	public ActionListener mostrarPestañas(PestañasModificar pm, Pestañas p, Borrar b, BuscarModificar modificar, Opciones o) {
		return new insertarPaciente(pm, p, b, modificar, o);
	}

	public WindowAdapter salirVentana(ModeloMariaDB modelo, Vista v) {
		return new salirVentana(modelo, v);
	}
	
	public WindowAdapter salirOpciones(Opciones o, Vista v){
		return new cerrarOpciones(o, v);
	}

	public ActionListener limpiarPestaña1(Pestañas p) {
		return new limpiarDatosPestaña1(p);
	}

	public ActionListener limpiarPestaña2(Pestañas p) {
		return new limpiarDatosPestaña2(p);
	}

	public ActionListener limpiarPestaña3(Pestañas p) {
		return new limpiarDatosPestaña3(p);
	}

	public ActionListener limpiarPestaña4(Pestañas p) {
		return new limpiarDatosPestaña4(p);
	}
	
	public ActionListener producteSuport(ModeloMariaDB modelo, Pestañas p) {
		return new seleccionarProducteSuport(modelo, p);
	}

	public ActionListener pasarPestaña1(Pestañas p, ModeloMariaDB modelo) {
		return new siguiente1(p, modelo);
	}

	public ActionListener pasarPestaña2(Pestañas p, ModeloMariaDB modelo) {
		return new siguiente2(p, modelo);
	}

	public ActionListener pasarPestaña3(Pestañas p) {
		return new siguiente3(p);
	}

	public ActionListener introducirTodo(ModeloMariaDB modelo, Pestañas p, AñadirContacto ac, Borrar b, Opciones o, BuscarModificar bm, Contactos contacto) {
		return new introduirTot(modelo, p, ac, b, o, bm, contacto);
	}

	public ActionListener seleccionarCentre(Pestañas p) {
		return new seleccionarCentre(p);
	}

	public ActionListener seleccionarDispositius(Pestañas p) {
		return new seleccionarDispositius(p);
	}
	
	public ActionListener salir(Pestañas p, ModeloMariaDB modelo){
		return new sortirAplicacio(p, modelo);
	}

	public ActionListener borrar(PestañasModificar pm, Borrar b, Pestañas p, BuscarModificar modificar, Opciones o ){
		return new borrarPaciente(pm, b, p, modificar, o);
	}
	
	public class inicioSesion implements ActionListener {

		private ModeloMariaDB	modelo;
		private InicioSesion	inicio;
		private Vista			v;

		public inicioSesion(ModeloMariaDB modelo, InicioSesion inicio, Vista v) {
			this.modelo = modelo;
			this.inicio = inicio;
			this.v = v;
		}

		@Override
		public void actionPerformed(ActionEvent e) {
			v.getServicio().unregister();
			if (modelo.conectarDB(inicio.getTxtUsuari().getText(), inicio
					.getTxtPassword().getText())) {
				v.setEnabled(true);
				v.setVisible(true);
				v.getPhone().setVisible(true);
				inicio.setVisible(false);
				if(modelo.isAdmin(inicio.getTxtUsuari().getText(), inicio.getTxtPassword().getText())) {
					v.getMntmAddEmployee().setVisible(true);
					v.getMntmDeleteEmployee().setVisible(true);
					v.getMntmOpciones().setVisible(true);
					v.addListenerOpciones();
					v.setGLUE();
				}
				v.getServicio().registrarse(inicio.getTxtUsuari().getText(), inicio.getTxtPassword().getText());
			} else {
				inicio.setTxtUsuari();
				inicio.setTxtPassword();
				JOptionPane.showMessageDialog(inicio,
						"Los datos introducidos son incorrectos.");
			}
		}
	}

	public class salirInicioSesion implements ActionListener {

		private ModeloMariaDB	modelo;
		private InicioSesion	inicio;
		private Vista v;

		public salirInicioSesion(ModeloMariaDB modelo, InicioSesion inicio, Vista v) {
			this.modelo = modelo;
			this.v = v;
			this.inicio = inicio;
		}

		@Override
		public void actionPerformed(ActionEvent arg0) {
			int result = JOptionPane.showConfirmDialog(inicio,
					"ñSeguro que quieres salir?", "Confirmar Salir",
					JOptionPane.YES_NO_OPTION);
			if (result == JOptionPane.YES_OPTION) {
				v.getServicio().getProcces().destroy();
				try {
					Thread.sleep(1500);
				} catch (InterruptedException e1) {
					e1.printStackTrace();
				}
				System.exit(0);
			}
		}
	}

	public class salirAplicacion extends WindowAdapter {

		private InicioSesion	inicio;
		private Vista v;

		public salirAplicacion(InicioSesion inicio, Vista v) {
			this.inicio = inicio;
			this.v = v;
		}

		@Override
		public void windowClosing(WindowEvent e) {
			int result = JOptionPane.showConfirmDialog(inicio,
					"ñSeguro que quieres salir?", "Confirmar Salir",
					JOptionPane.YES_NO_OPTION);
			if (result == JOptionPane.YES_OPTION) {
				v.getServicio().getProcces().destroy();
				try {
					Thread.sleep(1500);
				} catch (InterruptedException e1) {
					e1.printStackTrace();
				}
				System.exit(0);
			}
		}
	}

	public class salirVentana extends WindowAdapter {

		private Vista			v;
		private ModeloMariaDB	modelo;

		public salirVentana(ModeloMariaDB modelo, Vista v) {
			this.v = v;
			this.modelo = modelo;
		}

		@Override
		public void windowClosing(WindowEvent e) {
			int result = JOptionPane.showConfirmDialog(v,
					"ñSeguro que quieres salir?", "Confirmar Salir",
					JOptionPane.YES_NO_OPTION);
			if (result == JOptionPane.YES_OPTION) {
				modelo.cerrarDB();
				v.getServicio().getProcces().destroy();
				try {
					Thread.sleep(1500);
				} catch (InterruptedException e1) {
					e1.printStackTrace();
				}
				System.exit(0);
			}
		}
	}

	public class insertarPaciente implements ActionListener {
		private PestañasModificar pm;
		private Pestañas p;
		private Borrar b;
		private BuscarModificar modificar;
		private Opciones o;

		public insertarPaciente(PestañasModificar pm, Pestañas p, Borrar b, BuscarModificar modificar, Opciones o) {
			this.p = p;
			this.b = b;
			this.pm = pm;
			this.modificar = modificar;
			this.o = o;
		}

		@Override
		public void actionPerformed(ActionEvent e) {
			b.setVisible(false);
			pm.setVisible(false);
			p.setVisible(true);
			modificar.setVisible(false);
			o.setVisible(false);
		}
	}
	
	public class borrarPaciente implements ActionListener {
		private Borrar b;
		private Pestañas p;
		private BuscarModificar modificar;
		private Opciones o;
		private PestañasModificar pm;
		
		public borrarPaciente(PestañasModificar pm, Borrar b, Pestañas p,  BuscarModificar modificar, Opciones o) {
			this.b = b;
			this.pm = pm;
			this.p = p;
			this.modificar = modificar;
			this.o = o;
		}
		
		@Override
		public void actionPerformed(ActionEvent e) {
			p.setVisible(false);
			b.setVisible(true);
			pm.setVisible(false);
			modificar.setVisible(false);
			o.setVisible(false);
		}
	}

	public class limpiarDatosPestaña1 implements ActionListener {

		private Pestañas	p;

		public limpiarDatosPestaña1(Pestañas p) {
			this.p = p;
		}

		@Override
		public void actionPerformed(ActionEvent arg0) {
			p.getTxtNomContacte().setText("");
			p.getCboSexeContacte().setSelectedIndex(0);
			p.getTxtMobilContacte().setText("");
			p.getTxtRelacio().setText("");
			p.getTxtPoblacioContacte().setText("");
			p.getTxtCodiPostalContacte().setText("");
			p.getTxtDomiciliContacte().setText("");
			p.getTxtAdrecaContacte().setText("");
			p.getTxtTelefonContacte().setText("");
			p.getCboFeina().setSelectedIndex(0);
			p.getCboHorariFeina().setSelectedIndex(0);
			p.getTxtEmailContacte().setText("");
			p.getCboClaus().setSelectedIndex(0);
			p.getTxtEdat().setText("");
			p.getCboSexe().setSelectedIndex(0);
			p.getTxtNom().setText("");
			p.getTxtCognoms().setText("");
			p.getCboAnyNaix().setSelectedIndex(0);
			p.getCboDiaNaix().setSelectedIndex(0);
			p.getCboMesNaix().setSelectedIndex(0);
			p.getTxtDni().setText("");
			p.getCboTipoVia().setSelectedIndex(0);
			p.getTxtAdreca().setText("");
			p.getTxtNumeroAdreca().setText("");
			p.getTxtPisAdreca().setText("");
			p.getTxtPortaCarrer().setText("");
			p.getTxtInstruccions().setText("");
			p.getTxtPoblacio().setText("");
			p.getTxtCodiPostal().setText("");
			p.getTxtTelefon1().setText("");
			p.getTxtTelefon2().setText("");
			p.getTxtEmail().setText("");
		}
	}

	public class limpiarDatosPestaña2 implements ActionListener {

		private Pestañas	p;

		public limpiarDatosPestaña2(Pestañas p) {
			this.p = p;
		}

		@Override
		public void actionPerformed(ActionEvent e) {
			p.getGroup1().clearSelection();
			p.getChkMuleta().setSelected(false);
			p.getChkCaminador().setSelected(false);
			p.getChkCadiraRodes().setSelected(false);
			p.getCboPrestacioEconomica().setSelectedIndex(0);
			p.getCboGrauDependencia().setSelectedIndex(0);
			p.getCboVisites().setSelectedIndex(0);
			p.getCboRelacioFamilia().setSelectedIndex(0);
			p.getCboNivellDependencia().setSelectedIndex(0);
			p.getCboSituacioConvivencial().setSelectedIndex(0);
			p.getCboHoresSol().setSelectedIndex(0);
			p.getChkCentreDia().setSelected(false);
			p.getChkCentreNit().setSelected(false);
			p.getChkAjudaDomicili().setSelected(false);
			p.getChkCaminador().setEnabled(false);
			p.getChkCadiraRodes().setEnabled(false);
			p.getChkMuleta().setEnabled(false);
		}
	}

	public class limpiarDatosPestaña3 implements ActionListener {

		private Pestañas	p;

		public limpiarDatosPestaña3(Pestañas p) {
			this.p = p;
		}

		@Override
		public void actionPerformed(ActionEvent arg0) {
			p.getGroup2().clearSelection();
			p.getChkOsteomuscular().setSelected(false);
			p.getChkSistNervios().setSelected(false);
			p.getChkReproductorExcretor().setSelected(false);
			p.getChkRespiratori().setSelected(false);
			p.getChkDigestiu().setSelected(false);
			p.getChkCardiovascular().setSelected(false);
			p.getTxtEstatura().setText("");
			p.getTxtPes().setText("");
			p.getCboEstatActual().setSelectedIndex(0);
			p.getTxtPatologia().setText("");
			p.getTxtAlergies().setText("");
			p.getTxtVacunes().setText("");
			p.getTxtDificultats().setText("");
			p.getTxtAreaMalalties().setText("");
			p.getTxtAreaMedicacio().setText("");
			p.getCboCapacitatCognitiva().setSelectedIndex(0);
			p.getTxtCAP().setText("");
			p.getTxtTelefonCAP().setText("");
			p.getTxtHospital().setText("");
			p.getTxtHospital2().setText("");
			p.getTxtMetge().setText("");
			p.getTxtMutua().setText("");
			p.getTxtMutua().setEnabled(false);
			p.getTxtHospital2().setEnabled(false);
			p.getTxtHospital().setEnabled(false);
			p.getTxtCAP().setEnabled(false);
			p.getTxtTelefonCAP().setEnabled(false);
			p.getTxtMetge().setEnabled(false);
		}
	}
	
	public class limpiarDatosPestaña4 implements ActionListener{
		private Pestañas p;
		
		public limpiarDatosPestaña4(Pestañas p) {
			this.p = p;
		}
		
		@Override
		public void actionPerformed(ActionEvent arg0) {
			p.getCboAnyDataAlta().setSelectedIndex(0);
			p.getCboMesDataAlta().setSelectedIndex(0);
			p.getCboDiaDataAlta().setSelectedIndex(0);
			p.getChkDiposit().setSelected(false);
			p.getChkManteniment().setSelected(false);
			p.getChkUnicaQuota().setSelected(false);
			p.getTxtBanc1().setText("");
			p.getTxtBanc2().setText("");
			p.getTxtBanc3().setText("");
			p.getTxtBanc4().setText("");
			p.getTxtNomContractant().setText("");
			p.getTxtCognomsContractant().setText("");
			p.getTxtDNIContractant().setText("");
			p.getTxtCodiPostalContractant().setText("");
			p.getTxtProvinciaContractant().setText("");
			p.getTxtTelefonContractant().setText("");
			p.getChkDispositiuFixe().setSelected(false);
			p.getChkDispositiuInalambric().setSelected(false);
			p.getGroup4().clearSelection();
			p.getChkAgendesSeguiment().setSelected(false);
			p.getChkActivitats().setSelected(false);
			p.getChkCustodiaClaus().setSelected(false);
			p.getChkTelelocalitzacio().setSelected(false);
			p.getChkAlarmesDomotiques().setSelected(false);
			p.getChkSensorsMoviment().setSelected(false);
			p.getChkDetectorCaigudes().setSelected(false);
			p.getChkDetectorFum().setSelected(false);
			p.getChkDetectorGas().setSelected(false);
			p.getChkAperturaNevera().setSelected(false);
		}
	}

	public class seleccionarProducteSuport implements ActionListener {

		private ModeloMariaDB	modelo;
		private Pestañas		p;

		public seleccionarProducteSuport(ModeloMariaDB modelo, Pestañas p) {
			this.modelo = modelo;
			this.p = p;
		}

		@Override
		public void actionPerformed(ActionEvent arg0) {
			if (p.getRdbtnProducteSuport().isSelected()) {
				p.getChkCadiraRodes().setEnabled(true);
				p.getChkCaminador().setEnabled(true);
				p.getChkMuleta().setEnabled(true);
			} else if (p.getRdbtnAjuda().isSelected()
					|| p.getRdbtnAutonom().isSelected()
					|| p.getRdbtnEnllitament().isSelected()) {
				p.getChkCadiraRodes().setEnabled(false);
				p.getChkCaminador().setEnabled(false);
				p.getChkMuleta().setEnabled(false);
				p.getChkCadiraRodes().setSelected(false);
				p.getChkCaminador().setSelected(false);
				p.getChkMuleta().setSelected(false);
			}
		}
	}
	
	public class seleccionarFeina implements ActionListener {
		private Pestañas p;
		
		public seleccionarFeina(Pestañas p) {
			this.p = p;
		}
		
		@Override
		public void actionPerformed(ActionEvent arg0) {
			if(p.getCboFeina().getSelectedIndex() == 1){
				p.getCboHorariFeina().setEnabled(true);
			}else{
				p.getCboHorariFeina().setEnabled(false);
			}
		}
	}

	public class seleccionarDispositius implements ActionListener {

		private Pestañas	p;

		public seleccionarDispositius(Pestañas p) {
			this.p = p;
		}

		@Override
		public void actionPerformed(ActionEvent arg0) {
			if (p.getChkAlarmesDomotiques().isSelected()) {
				p.getChkDetectorCaigudes().setEnabled(true);
				p.getChkDetectorFum().setEnabled(true);
				p.getChkDetectorGas().setEnabled(true);
				p.getChkAperturaNevera().setEnabled(true);
				p.getChkSensorsMoviment().setEnabled(true);
			} else {
				p.getChkDetectorCaigudes().setEnabled(false);
				p.getChkDetectorFum().setEnabled(false);
				p.getChkDetectorGas().setEnabled(false);
				p.getChkAperturaNevera().setEnabled(false);
				p.getChkSensorsMoviment().setEnabled(false);
				p.getChkDetectorCaigudes().setSelected(false);
				p.getChkDetectorFum().setSelected(false);
				p.getChkDetectorGas().setSelected(false);
				p.getChkAperturaNevera().setSelected(false);
				p.getChkSensorsMoviment().setSelected(false);
			}
		}
	}

	public class seleccionarCentre implements ActionListener {

		private Pestañas	p;

		public seleccionarCentre(Pestañas p) {
			this.p = p;
		}

		@Override
		public void actionPerformed(ActionEvent arg0) {
			if (p.getRdbtnCentrePublic().isSelected()) {
				p.getTxtCAP().setEnabled(true);
				p.getTxtTelefonCAP().setEnabled(true);
				p.getTxtMetge().setEnabled(true);
				p.getTxtHospital().setEnabled(true);
			} else {
				p.getTxtCAP().setEnabled(false);
				p.getTxtTelefonCAP().setEnabled(false);
				p.getTxtMetge().setEnabled(false);
				p.getTxtHospital().setEnabled(false);
				p.getTxtCAP().setText("");
				p.getTxtTelefonCAP().setText("");
				p.getTxtHospital().setText("");
				p.getTxtMetge().setText("");
			}
			if (p.getRdbtnPrivat().isSelected()) {
				p.getTxtMutua().setEnabled(true);
				p.getTxtHospital2().setEnabled(true);
			} else {
				p.getTxtMutua().setEnabled(false);
				p.getTxtHospital2().setEnabled(false);
				p.getTxtMutua().setText("");
				p.getTxtHospital2().setText("");
			}
		}
	}

	public class siguiente1 implements ActionListener {

		private Pestañas		p;
		private ModeloMariaDB	modelo;

		public siguiente1(Pestañas p, ModeloMariaDB modelo) {
			this.p = p;
			this.modelo = modelo;
		}

		@Override
		public void actionPerformed(ActionEvent arg0) {
			String regexEdad = "\\d{2,3}";
			String regexNombrePaciente = "^[\\p{L} .'-]+$";
			String regexApellidoPaciente= "^[\\p{L} .'-]+$";
			String regexDireccion = "^[\\p{L} .'-]+$"; 
			String regexNumerosDireccion = "\\d{1,4}";
			String regexInstrucciones = "^[a-zA-Z0-9_]*$";
			String regexPoblacion = "[a-zA-Z]+";
			String regexCodiPostal = "\\d{5}";
			String regexEmail = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
			String regexTelefonos = "\\d{9}";
			
			if (p.getCboSexe().getSelectedIndex() == 0) {
				JOptionPane.showMessageDialog(p,
						"Selecciona el sexo del paciente." , "Error", JOptionPane.ERROR_MESSAGE);
			} else if (p.getTxtEdat().getText().length() <= 0) {
				JOptionPane.showMessageDialog(p,
						"Introduce la edad del paciente." , "Error", JOptionPane.ERROR_MESSAGE);
			} else if (p.getTxtNom().getText().length() <= 0) {
				JOptionPane.showMessageDialog(p,
						"Introduce el nombre del paciente." , "Error", JOptionPane.ERROR_MESSAGE);
			} else if (p.getTxtCognoms().getText().length() <= 0) {
				JOptionPane.showMessageDialog(p,
						"Introduce los apellidos del paciente." , "Error", JOptionPane.ERROR_MESSAGE);
			} else if (p.getCboAnyNaix().getSelectedIndex() == 0) {
				JOptionPane.showMessageDialog(p,
						"Selecciona el año de nacimiento." , "Error", JOptionPane.ERROR_MESSAGE);
			} else if (p.getCboMesNaix().getSelectedIndex() == 0) {
				JOptionPane.showMessageDialog(p,
						"Selecciona el mes de nacimiento." , "Error", JOptionPane.ERROR_MESSAGE) ;
			} else if (p.getCboDiaNaix().getSelectedIndex() == 0) {
				JOptionPane.showMessageDialog(p,
						"Selecciona el dia de nacimiento." , "Error", JOptionPane.ERROR_MESSAGE);
			} else if (p.getTxtDni().getText().length() <= 0) {
				JOptionPane.showMessageDialog(p,
						"Introduce el DNI del paciente." , "Error", JOptionPane.ERROR_MESSAGE);
			} else if (p.getCboTipoVia().getSelectedIndex() == 0) {
				JOptionPane.showMessageDialog(p, "Selecciona el tipo de via." , "Error", JOptionPane.ERROR_MESSAGE);
			} else if (p.getTxtAdreca().getText().length() <= 0) {
				JOptionPane.showMessageDialog(p,
						"Introduce la direcciñn del paciente." , "Error", JOptionPane.ERROR_MESSAGE);
			} else if (p.getTxtNumeroAdreca().getText().length() <= 0) {
				JOptionPane.showMessageDialog(p,
						"Introduce el nñmero de la vivienda del paciente." , "Error", JOptionPane.ERROR_MESSAGE);
			} else if (p.getTxtPisAdreca().getText().length() <= 0) {
				JOptionPane.showMessageDialog(p,
						"Introduce el nñmero del piso del paciente." , "Error", JOptionPane.ERROR_MESSAGE);
			} else if (p.getTxtPortaCarrer().getText().length() <= 0) {
				JOptionPane.showMessageDialog(p,
						"Introduce el nñmero de puerta del paciente." , "Error", JOptionPane.ERROR_MESSAGE);
			} else if (p.getTxtInstruccions().getText().length() <= 0) {
				JOptionPane
						.showMessageDialog(p, "Introduce las instrucciones." , "Error", JOptionPane.ERROR_MESSAGE);
			} else if (p.getTxtPoblacio().getText().length() <= 0) {
				JOptionPane.showMessageDialog(p,
						"Introduce la poblacion del paciente." , "Error", JOptionPane.ERROR_MESSAGE);
			} else if (p.getTxtCodiPostal().getText().length() <= 0) {
				JOptionPane.showMessageDialog(p,
						"Introduce el codigo postal del paciente." , "Error", JOptionPane.ERROR_MESSAGE);
			} else if (p.getTxtTelefon1().getText().length() <= 0) {
				JOptionPane.showMessageDialog(p,
						"Introduce el telefono 1 del paciente." , "Error", JOptionPane.ERROR_MESSAGE);
			} else if (p.getTxtTelefon2().getText().length() <= 0) {
				JOptionPane.showMessageDialog(p,
						"Introduce el telefono 2 del paciente." , "Error", JOptionPane.ERROR_MESSAGE);
			} else if (p.getTxtEmail().getText().length() <= 0) {
				JOptionPane.showMessageDialog(p,
						"Introduce el email del paciente." , "Error", JOptionPane.ERROR_MESSAGE);
			} else if (p.getTxtNomContacte().getText().length() <= 0) {
				JOptionPane.showMessageDialog(p,
						"Introduce el nombre del contacto." , "Error", JOptionPane.ERROR_MESSAGE);
			} else if (p.getCboSexeContacte().getSelectedIndex() == 0) {
				JOptionPane.showMessageDialog(p,
						"Selecciona el sexo del contacto." , "Error", JOptionPane.ERROR_MESSAGE);
			} else if (p.getTxtMobilContacte().getText().length() <= 0) {
				JOptionPane.showMessageDialog(p,
						"Introduce el movil del contacto." , "Error", JOptionPane.ERROR_MESSAGE);
			} else if (p.getTxtRelacio().getText().length() <= 0) {
				JOptionPane.showMessageDialog(p,
						"Introduce la relaciñn con el paciente." , "Error", JOptionPane.ERROR_MESSAGE);
			} else if (p.getTxtPoblacioContacte().getText().length() <= 0) {
				JOptionPane.showMessageDialog(p,
						"Introduce la poblaciñn del contacto." , "Error", JOptionPane.ERROR_MESSAGE);
			} else if (p.getTxtCodiPostalContacte().getText().length() <= 0) {
				JOptionPane.showMessageDialog(p,
						"Introduce el codigo postal del contacto." , "Error", JOptionPane.ERROR_MESSAGE);
			} else if (p.getTxtDomiciliContacte().getText().length() <= 0) {
				JOptionPane.showMessageDialog(p,
						"Introduce el domicilio del contacto." , "Error", JOptionPane.ERROR_MESSAGE);
			} else if (p.getTxtAdrecaContacte().getText().length() <= 0) {
				JOptionPane.showMessageDialog(p,
						"Introduce la direccion del contacto." , "Error", JOptionPane.ERROR_MESSAGE);
			} else if (p.getTxtTelefonContacte().getText().length() <= 0) {
				JOptionPane.showMessageDialog(p,
						"Introduce el telefono del contacto." , "Error", JOptionPane.ERROR_MESSAGE);
			} else if (p.getCboFeina().getSelectedIndex() == 0) {
				JOptionPane.showMessageDialog(p,
						"Introduce el apartado de trabajo." , "Error", JOptionPane.ERROR_MESSAGE);
			} else if (p.getTxtEmailContacte().getText().length() <= 0) {
				JOptionPane.showMessageDialog(p,
						"Introduce el email del contacto." , "Error", JOptionPane.ERROR_MESSAGE);
			} else if (p.getCboClaus().getSelectedIndex() == 0) {
				JOptionPane.showMessageDialog(p,
						"Selecciona el apartado de llaves." , "Error", JOptionPane.ERROR_MESSAGE);
			} else {
				//Insertar Paciente
				if(!p.getTxtNom().getText().matches(regexNombrePaciente)){
					JOptionPane.showMessageDialog(p, "Nombre no valido." , "Error", JOptionPane.ERROR_MESSAGE);
				}else if(!p.getTxtCognoms().getText().matches(regexApellidoPaciente)){
					JOptionPane.showMessageDialog(p, "Apellidos invalidos." , "Error", JOptionPane.ERROR_MESSAGE);
				}else if(!p.getTxtEdat().getText().matches(regexEdad)){
					JOptionPane.showMessageDialog(p, "Edad invalida." , "Error", JOptionPane.ERROR_MESSAGE);
				}else if(!p.getTxtAdreca().getText().matches(regexNombrePaciente)){
					JOptionPane.showMessageDialog(p, "Direcciñn incorrecta." , "Error", JOptionPane.ERROR_MESSAGE);
				}else if(!p.getTxtNumeroAdreca().getText().matches(regexNumerosDireccion)){
					JOptionPane.showMessageDialog(p, "Numero direcciñn incorrecto." , "Error", JOptionPane.ERROR_MESSAGE);
				}else if(!p.getTxtPisAdreca().getText().matches(regexNumerosDireccion)){
					JOptionPane.showMessageDialog(p, "Nñmero de piso incorrecto." , "Error", JOptionPane.ERROR_MESSAGE);
				}else if(!p.getTxtPortaCarrer().getText().matches(regexNumerosDireccion)){
					JOptionPane.showMessageDialog(p, "Puerta incorrecta." , "Error", JOptionPane.ERROR_MESSAGE);
				}else if(!p.getTxtInstruccions().getText().matches(regexNombrePaciente)){
					JOptionPane.showMessageDialog(p, "Instrucciones no validas." , "Error", JOptionPane.ERROR_MESSAGE);
				}else if(!p.getTxtPoblacio().getText().matches(regexInstrucciones)){
					JOptionPane.showMessageDialog(p, "Poblaciñn no vñlida.." , "Error", JOptionPane.ERROR_MESSAGE);
				}else if(!p.getTxtCodiPostal().getText().matches(regexCodiPostal)){
					JOptionPane.showMessageDialog(p, "Cñdigo postal no vñlido." , "Error", JOptionPane.ERROR_MESSAGE);
				}else if(!p.getTxtTelefon1().getText().matches(regexTelefonos)){
					JOptionPane.showMessageDialog(p, "Telñfono no vñlido." , "Error", JOptionPane.ERROR_MESSAGE);
				}else if(!p.getTxtTelefon2().getText().matches(regexTelefonos)){
					JOptionPane.showMessageDialog(p, "Telñfono no vñlido." , "Error", JOptionPane.ERROR_MESSAGE);
				}else if(!p.getTxtEmail().getText().matches(regexEmail)){
					JOptionPane.showMessageDialog(p, "Email no vñlido." , "Error", JOptionPane.ERROR_MESSAGE);
				}else if(!p.getTxtNomContacte().getText().matches(regexNombrePaciente)){
					JOptionPane.showMessageDialog(p, "Nombre no vñlido." , "Error", JOptionPane.ERROR_MESSAGE);
				}else if(!p.getTxtRelacio().getText().matches(regexPoblacion)){
					JOptionPane.showMessageDialog(p, "Relaciñn con el paciente no vñlida." , "Error", JOptionPane.ERROR_MESSAGE);
				}else if(!p.getTxtPoblacioContacte().getText().matches(regexInstrucciones)){
					JOptionPane.showMessageDialog(p, "Poblaciñn no vñlida." , "Error", JOptionPane.ERROR_MESSAGE);
				}else if(!p.getTxtCodiPostalContacte().getText().matches(regexCodiPostal)){
					JOptionPane.showMessageDialog(p, "Codigo postal no vñlido." , "Error", JOptionPane.ERROR_MESSAGE);
				}else if(!p.getTxtAdrecaContacte().getText().matches(regexNombrePaciente)){
					JOptionPane.showMessageDialog(p, "Direcciñn contacto no vñlida." , "Error", JOptionPane.ERROR_MESSAGE);
				}else if(!p.getTxtTelefonContacte().getText().matches(regexTelefonos)){
					JOptionPane.showMessageDialog(p, "Telefono contacto no vñlido." , "Error", JOptionPane.ERROR_MESSAGE);
				}else if(!p.getTxtDomiciliContacte().getText().matches(regexInstrucciones)){
					JOptionPane.showMessageDialog(p, "Domicilio contacto invalido." , "Error", JOptionPane.ERROR_MESSAGE);
				}else if(!p.getTxtMobilContacte().getText().matches(regexTelefonos)){
					JOptionPane.showMessageDialog(p, "Telefono movil contacto no vñlido." , "Error", JOptionPane.ERROR_MESSAGE);
				}else if(!p.getTxtEmailContacte().getText().matches(regexEmail)){
					JOptionPane.showMessageDialog(p, "Email contacto no vñlido." , "Error", JOptionPane.ERROR_MESSAGE);
				}else{
					if(modelo.isNifNie(p.getTxtDni().getText()) == true){
						p.setSelectedIndex(1);
					}else{
						JOptionPane.showMessageDialog(p, "DNI no valido.");
					}
				}
			}
		}
	}

	public class siguiente2 implements ActionListener {

		private Pestañas		p;
		private ModeloMariaDB	modelo;

		public siguiente2(Pestañas p, ModeloMariaDB modelo) {
			this.p = p;
			this.modelo = modelo;
		}

		@Override
		public void actionPerformed(ActionEvent e) {
			if (p.getCboGrauDependencia().getSelectedIndex() != 0
					&& p.getCboNivellDependencia().getSelectedIndex() != 0
					&& p.getCboPrestacioEconomica().getSelectedIndex() != 0
					&& p.getCboSituacioConvivencial().getSelectedIndex() != 0
					&& p.getCboHoresSol().getSelectedIndex() != 0
					&& p.getCboRelacioFamilia().getSelectedIndex() != 0
					&& p.getCboVisites().getSelectedIndex() != 0) {
				if (!p.getRdbtnAjuda().isSelected()
						&& !p.getRdbtnAutonom().isSelected()
						&& !p.getRdbtnEnllitament().isSelected()) {
					if (p.getRdbtnProducteSuport().isSelected()) {
						if (p.getChkMuleta().isSelected()
								|| p.getChkCadiraRodes().isSelected()
								|| p.getChkCaminador().isSelected()) {
							p.setSelectedIndex(2);
						} else {
							JOptionPane.showMessageDialog(p, "Introduce todos los datos." , "Error", JOptionPane.ERROR_MESSAGE);
						}
					}
				} else {
					p.setSelectedIndex(2);
				}

			} else JOptionPane.showMessageDialog(p,"Introduce todos los datos." , "Error", JOptionPane.ERROR_MESSAGE);
		}
	}

	public class siguiente3 implements ActionListener {
		private Pestañas p;
		
		public siguiente3(Pestañas p) {
			this.p = p;
		}

		@Override
		public void actionPerformed(ActionEvent e) {
			String regexNombrePaciente = "^[\\p{L} .'-]+$";
			String regexInstrucciones = "^[a-zA-Z0-9_]*$";
			String regexTelefonos = "\\d{9}";
			String regexEstatura = "[0-9]+";
			
			if(p.getTxtEstatura().getText().length() <= 0){
				JOptionPane.showMessageDialog(p, "Introduce la estatura." , "Error", JOptionPane.ERROR_MESSAGE);
			}else if(p.getTxtPes().getText().length() <= 0){
				JOptionPane.showMessageDialog(p, "Introduce el peso." , "Error", JOptionPane.ERROR_MESSAGE);
			}else if(p.getCboEstatActual().getSelectedIndex() == 0){
				JOptionPane.showMessageDialog(p, "Introduce el estado actual." , "Error", JOptionPane.ERROR_MESSAGE);
			}else if(p.getTxtPatologia().getText().length() <= 0){
				JOptionPane.showMessageDialog(p, "Introduce la patologia." , "Error", JOptionPane.ERROR_MESSAGE);
			}else if(!(p.getChkOsteomuscular().isSelected() || p.getChkSistNervios().isSelected() || p.getChkRespiratori().isSelected() || p.getChkCardiovascular().isSelected() || p.getChkDigestiu().isSelected() || p.getChkReproductorExcretor().isSelected())){
				JOptionPane.showMessageDialog(p, "Introduce un sistema del cuerpo humano." , "Error", JOptionPane.ERROR_MESSAGE);
			}else if(p.getTxtAreaMalalties().getText().length() <= 0){
				JOptionPane.showMessageDialog(p, "Introduce las enfermedades." , "Error", JOptionPane.ERROR_MESSAGE);
			}else if(p.getTxtAreaMedicacio().getText().length() <= 0){
				JOptionPane.showMessageDialog(p, "Introduce la medicacion." , "Error", JOptionPane.ERROR_MESSAGE);
			}else if(p.getCboCapacitatCognitiva().getSelectedIndex() == 0){
				JOptionPane.showMessageDialog(p, "Selecciona la capacidad cognitiva." , "Error", JOptionPane.ERROR_MESSAGE);
			}else if(p.getTxtVacunes().getText().length() <= 0){
				JOptionPane.showMessageDialog(p, "Introduce las vacunas." , "Error", JOptionPane.ERROR_MESSAGE);
			}else if(!p.getRdbtnCentrePublic().isSelected() && !p.getRdbtnPrivat().isSelected()){
				JOptionPane.showMessageDialog(p, "Selecciona algun centro." , "Error", JOptionPane.ERROR_MESSAGE);
			}else if(p.getRdbtnCentrePublic().isSelected() && (p.getTxtHospital().getText().length() <= 0	&& p.getTxtMetge().getText().length() <= 0
					&& p.getTxtCAP().getText().length() <= 0 && p.getTxtTelefonCAP().getText().length() <= 0)){
				JOptionPane.showMessageDialog(p, "Introduce los datos del centro publico." , "Error", JOptionPane.ERROR_MESSAGE);
			}else if(p.getRdbtnPrivat().isSelected() && (p.getTxtMutua().getText().length() <= 0 && p.getTxtHospital2().getText().length() <= 0)){
				JOptionPane.showMessageDialog(p, "Introduce los datos del centro privado." , "Error", JOptionPane.ERROR_MESSAGE);
			}else{
				if(!p.getTxtEstatura().getText().matches(regexEstatura)){
					JOptionPane.showMessageDialog(p, "Estatura invñlida." , "Error", JOptionPane.ERROR_MESSAGE);
				}else if(!p.getTxtPes().getText().matches(regexEstatura)){
					JOptionPane.showMessageDialog(p, "Peso invñlido." , "Error", JOptionPane.ERROR_MESSAGE);
				}else if(!p.getTxtPatologia().getText().matches(regexNombrePaciente)){
					JOptionPane.showMessageDialog(p, "Patologia no vñlida." , "Error", JOptionPane.ERROR_MESSAGE);
				}else if(!p.getTxtAlergies().getText().matches(regexNombrePaciente)){
					JOptionPane.showMessageDialog(p, "Alergias no validas." , "Error", JOptionPane.ERROR_MESSAGE);
				}else if(!p.getTxtAreaMalalties().getText().matches(regexNombrePaciente)){
					JOptionPane.showMessageDialog(p, "Enfermedades no vñlidas." , "Error", JOptionPane.ERROR_MESSAGE);
				}else if(!p.getTxtAreaMedicacio().getText().matches(regexNombrePaciente)){
					JOptionPane.showMessageDialog(p, "Campo de medicaciones no vñlidas." , "Error", JOptionPane.ERROR_MESSAGE);
				}else if(!p.getTxtDificultats().getText().matches(regexNombrePaciente)){
					JOptionPane.showMessageDialog(p, "Campo de dificultades no vñlido." , "Error", JOptionPane.ERROR_MESSAGE);
				}else if(!p.getTxtVacunes().getText().matches(regexNombrePaciente)){
					JOptionPane.showMessageDialog(p, "Campo de vacunas no vñlido." , "Error", JOptionPane.ERROR_MESSAGE);
				}else if(p.getRdbtnCentrePublic().isSelected()){
					if(!p.getTxtCAP().getText().matches(regexNombrePaciente)){
						JOptionPane.showMessageDialog(p, "Nombre CAP no vñlido." , "Error", JOptionPane.ERROR_MESSAGE);
					}else if(!p.getTxtTelefonCAP().getText().matches(regexTelefonos)){
						JOptionPane.showMessageDialog(p, "Telefono CAP no vñlido." , "Error", JOptionPane.ERROR_MESSAGE);
					}else if(!p.getTxtMetge().getText().matches(regexNombrePaciente)){
						JOptionPane.showMessageDialog(p, "Nombre del mñdico no vñlido." , "Error", JOptionPane.ERROR_MESSAGE);
					}else if(!p.getTxtHospital().getText().matches(regexNombrePaciente)){
						JOptionPane.showMessageDialog(p, "Nombre hospital pñblico no vñlido." , "Error", JOptionPane.ERROR_MESSAGE);
					}else{
						p.setSelectedIndex(3);
					}
				}else if(p.getRdbtnPrivat().isSelected()){
					if(!p.getTxtMutua().getText().matches(regexInstrucciones)){
						JOptionPane.showMessageDialog(p, "Nombre mñtua no vñlido." , "Error", JOptionPane.ERROR_MESSAGE);
					}else if(!p.getTxtHospital2().getText().matches(regexInstrucciones)){
						JOptionPane.showMessageDialog(p, "Nombre hospital privado no vñlido." , "Error", JOptionPane.ERROR_MESSAGE);
					}else{
						p.setSelectedIndex(3);
					}
				}
			}
		}
	}

	public class introduirTot implements ActionListener {
		private ModeloMariaDB	modelo;
		private Pestañas p;
		private AñadirContacto ac;
		private Borrar b;
		private Opciones o;
		private BuscarModificar bm;
		private Contactos contacto;

		public introduirTot(ModeloMariaDB modelo, Pestañas p, AñadirContacto ac, Borrar b, Opciones o, BuscarModificar bm, Contactos contacto) {
			this.p = p;
			this.ac = ac;
			this.b = b;
			this.o = o;
			this.bm = bm;
			this.contacto = contacto;
			this.modelo = modelo;
		}

		@Override
		public void actionPerformed(ActionEvent arg0) {
			if(p.getCboAnyDataAlta().getSelectedIndex() == 0){
				JOptionPane.showMessageDialog(p, "Introduce el año de alta." , "Error", JOptionPane.ERROR_MESSAGE);
			}else if(p.getCboMesDataAlta().getSelectedIndex() == 0){
				JOptionPane.showMessageDialog(p, "Introduce el mes de alta." , "Error", JOptionPane.ERROR_MESSAGE);
			}else if(p.getCboDiaDataAlta().getSelectedIndex() == 0){
				JOptionPane.showMessageDialog(p, "Introduce el dia de alta." , "Error", JOptionPane.ERROR_MESSAGE);
			}else if(!(p.getChkUnicaQuota().isSelected() || p.getChkDiposit().isSelected() || p.getChkManteniment().isSelected())){				
				JOptionPane.showMessageDialog(p, "Selecciona una cuota del servicio." , "Error", JOptionPane.ERROR_MESSAGE);
			}else if(p.getTxtBanc1().getText().length()<=0 || p.getTxtBanc2().getText().length()<=0 || p.getTxtBanc3().getText().length()<=0 || p.getTxtBanc4().getText().length()<=0){
				JOptionPane.showMessageDialog(p, "Introduce los datos del banco." , "Error", JOptionPane.ERROR_MESSAGE);
			}else if(p.getTxtNomContractant().getText().length()<=0){
				JOptionPane.showMessageDialog(p, "Introduce el nombre del contractante." , "Error", JOptionPane.ERROR_MESSAGE);
			}else if(p.getTxtCognomsContractant().getText().length()<=0){
				JOptionPane.showMessageDialog(p, "Introuce los apellidos del contractante." , "Error", JOptionPane.ERROR_MESSAGE);
			}else if(p.getTxtTelefonContractant().getText().length()<=0){
				JOptionPane.showMessageDialog(p, "Introduce el telefono de contractante." , "Error", JOptionPane.ERROR_MESSAGE);
			}else if(p.getTxtCodiPostalContractant().getText().length()<=0){
				JOptionPane.showMessageDialog(p, "Introduce el codigo postal del contractante." , "Error", JOptionPane.ERROR_MESSAGE);
			}else if(p.getTxtProvinciaContractant().getText().length()<=0){
				JOptionPane.showMessageDialog(p, "Introduce la provincia del contractante." , "Error", JOptionPane.ERROR_MESSAGE);
			}else if(p.getTxtDNIContractant().getText().length()<=0){
				JOptionPane.showMessageDialog(p, "Introduce el DNI del contractante." , "Error", JOptionPane.ERROR_MESSAGE);
			}else if(!(p.getChkDispositiuFixe().isSelected() || p.getChkDispositiuInalambric().isSelected())){
				JOptionPane.showMessageDialog(p, "Selecciona un servicio basico a contratar." , "Error", JOptionPane.ERROR_MESSAGE);
			}else if(!(p.getRdbtnUnitatMovil().isSelected() || p.getRdbtnNoUnitatMovil().isSelected())){
				JOptionPane.showMessageDialog(p, "Selecciona una modalidad del STA a contractar." , "Error", JOptionPane.ERROR_MESSAGE);
			}else {
				String deambulacio= "";			
				if(p.getRdbtnProducteSuport().isSelected()){
					if(p.getChkMuleta().isSelected() && p.getChkCaminador().isSelected() && p.getChkCadiraRodes().isSelected()){
						deambulacio = p.getRdbtnProducteSuport().getText() + "-" + p.getChkMuleta().getText() + "/" + p.getChkCaminador().getText() + "/" + p.getChkCadiraRodes().getText();
					}else if(p.getChkMuleta().isSelected() && p.getChkCaminador().isSelected()){
						deambulacio = p.getRdbtnProducteSuport().getText() + "-" + p.getChkMuleta().getText() + "/" + p.getChkCaminador().getText(); 
					}else if(p.getChkMuleta().isSelected() && p.getChkCadiraRodes().isSelected()){
						deambulacio = p.getRdbtnProducteSuport().getText() + "-" + p.getChkMuleta().getText() + "/" + p.getChkCadiraRodes().getText();
					}else if(p.getChkCaminador().isSelected() && p.getChkCadiraRodes().isSelected()){
						deambulacio = p.getRdbtnProducteSuport().getText() + "-" + p.getChkCaminador().getText() + "/" + p.getChkCadiraRodes().getText();
			 	    }else if(p.getChkMuleta().isSelected()){
						deambulacio = p.getRdbtnProducteSuport().getText() + "-" + p.getChkMuleta().getText();
					}else if(p.getChkCaminador().isSelected()){
						deambulacio = p.getRdbtnProducteSuport().getText() + "-" + p.getChkCaminador().getText();
					}else if(p.getChkCadiraRodes().isSelected()){
						deambulacio = p.getRdbtnProducteSuport().getText() + "-" + p.getChkCadiraRodes().getText();
					}
				}else if(p.getRdbtnAjuda().isSelected()){
					deambulacio = p.getRdbtnAjuda().getText();
				}else if(p.getRdbtnAutonom().isSelected()){
					deambulacio = p.getRdbtnAutonom().getText();
				}else if(p.getRdbtnEnllitament().isSelected()){
					deambulacio = p.getRdbtnEnllitament().getText();
				}
				
				
				//Recuperamos la compatibilidad con otros servicios
				String compatibilitat="";
				if(p.getChkCentreDia().isSelected()){
					compatibilitat += p.getChkCentreDia().getText();
				}
				if(p.getChkCentreNit().isSelected()){
					compatibilitat += p.getChkCentreNit().getText();
				}
				if(p.getChkAjudaDomicili().isSelected()){
					compatibilitat += p.getChkAjudaDomicili().getText();
				}
				
				//Sistemas cuerpo
				String sist="";
				if(p.getChkOsteomuscular().isSelected()){
					sist+=p.getChkOsteomuscular().getText();
				}
				if(p.getChkReproductorExcretor().isSelected()){
					sist+=p.getChkReproductorExcretor().getText();
				}
				if(p.getChkRespiratori().isSelected()){
					sist+=p.getChkRespiratori().getText();
				}
				if(p.getChkDigestiu().isSelected()){
					sist+=p.getChkDigestiu().getText();
				}
				if(p.getChkCardiovascular().isSelected()){
					sist+=p.getChkCardiovascular().getText();
				}
				if(p.getChkSistNervios().isSelected()){
					sist+=p.getChkSistNervios().getText();
				}

				
				//Centre sanitari
				String centreSanitari="";
				if(p.getRdbtnCentrePublic().isSelected()){
					centreSanitari = p.getTxtCAP().getText() + "/" + p.getTxtTelefonCAP().getText() + "/" + p.getTxtMetge().getText() + "/" + p.getTxtHospital().getText();
				}else if(p.getRdbtnPrivat().isSelected()){
					centreSanitari = p.getTxtMutua().getText() + "/" + p.getTxtHospital2().getText();
				}
				
				//Quotes servei
				String quotes="";
				if(p.getChkUnicaQuota().isSelected()){
					quotes = p.getChkUnicaQuota().getText();
				}else if(p.getChkDiposit().isSelected()){
					quotes = p.getChkDiposit().getText();
				}else if(p.getChkManteniment().isSelected()){
					quotes = p.getChkManteniment().getText();
				}
				
				//DadesBanc
				String dadesBanc = p.getTxtBanc1().getText() + "-" + p.getTxtBanc2().getText() + "-" + p.getTxtBanc3().getText() + "-" + p.getTxtBanc4().getText();
						
				//Serveis Basics
				String serveisBasics="";
				if(p.getChkDispositiuFixe().isSelected()){
					serveisBasics +=p.getChkDispositiuFixe().getText();
				}
				if(p.getChkDispositiuInalambric().isSelected()){
					serveisBasics +=p.getChkDispositiuInalambric().getText();
				}
				
				//Modalitats
				String modalitats="";
				if(p.getRdbtnUnitatMovil().isSelected()){
					modalitats += p.getRdbtnUnitatMovil().getText();
				}
				if(p.getRdbtnNoUnitatMovil().isSelected()){
					modalitats += p.getRdbtnNoUnitatMovil().getText();
				}
				
				
				//PrestacionsAdicionals
				String prestacionsAdicionals="";
				if(p.getChkAgendesSeguiment().isSelected()){
					prestacionsAdicionals += p.getChkAgendesSeguiment().getText();
				}
				if(p.getChkActivitats().isSelected()){
					prestacionsAdicionals += p.getChkActivitats().getText();
				}
				if(p.getChkCustodiaClaus().isSelected()){
					prestacionsAdicionals += p.getChkCustodiaClaus().getText();
				}
				if(p.getChkTelelocalitzacio().isSelected()){
					prestacionsAdicionals += p.getChkTelelocalitzacio().getText();
				}
				if(p.getChkAlarmesDomotiques().isSelected()){
					
					prestacionsAdicionals += p.getChkAlarmesDomotiques().getText() + "-";
					
					if(p.getChkSensorsMoviment().isSelected()){
						prestacionsAdicionals += p.getChkSensorsMoviment().getText();
					}
					if(p.getChkAperturaNevera().isSelected()){
						prestacionsAdicionals += p.getChkAperturaNevera().getText();
					}
					if(p.getChkDetectorCaigudes().isSelected()){
						prestacionsAdicionals += p.getChkDetectorCaigudes().getText();
					}
					if(p.getChkDetectorGas().isSelected()){
						prestacionsAdicionals += p.getChkDetectorGas().getText();
					}
					if(p.getChkDetectorFum().isSelected()){
						prestacionsAdicionals += p.getChkDetectorFum().getText();
					}
				}
				
				String regexNombrePaciente = "^[\\p{L} .'-]+$";
				String regexInstrucciones = "^[a-zA-Z0-9_]*$";
				String regexCodiPostal = "\\d{5}";
				String regexTelefonos = "\\d{9}";
				String regexBanco1 = "\\d{4}";
				String regexBanco3 = "\\d{2}";
				String regexBanco4 = "\\d{10}";
				
				direccion = p.getCboTipoVia().getSelectedItem().toString()+ "/" + p.getTxtAdreca().getText() + " Nñ "+ p.getTxtNumeroAdreca().getText() + " " + p.getTxtPisAdreca().getText() + "ñ - "+ p.getTxtPortaCarrer().getText() + "a";
				//Insertar Paciente
				modelo.insertarPacient(Integer.parseInt(p.getTxtEdat().getText()),p.getCboSexe().getSelectedItem().toString(), p.getTxtNom().getText(), p.getTxtCognoms().getText(), Integer.parseInt(p.getCboAnyNaix().getSelectedItem().toString()), Integer.parseInt(p.getCboMesNaix().getSelectedItem().toString()),Integer.parseInt(p.getCboDiaNaix().getSelectedItem().toString()), p.getTxtDni().getText(), direccion, p.getTxtInstruccions().getText(), p.getTxtPoblacio().getText(), p.getTxtCodiPostal().getText(), p.getTxtTelefon1().getText(), p.getTxtTelefon2().getText(), p.getTxtEmail().getText());
				codiPacient = modelo.buscarCodiPacient(p.getTxtDni().getText());
				//Insertar Contacto
				modelo.insertarContacte(codiPacient, p.getTxtNomContacte().getText(), p.getCboSexeContacte().getSelectedItem().toString(), p.getTxtRelacio().getText(), p.getTxtPoblacioContacte().getText(), p.getTxtCodiPostalContacte().getText(), p.getTxtAdrecaContacte().getText(), p.getTxtTelefonContacte().getText(), p.getTxtDomiciliContacte().getText(), p.getCboFeina().getSelectedItem().toString(), p.getCboHorariFeina().getSelectedItem().toString(), p.getTxtMobilContacte().getText(), p.getTxtEmailContacte().getText(), p.getCboClaus().getSelectedItem().toString());
				//Insertar Dependencia
				modelo.insertarDependenciaPacient(codiPacient, deambulacio, p.getCboGrauDependencia().getSelectedItem().toString(), p.getCboNivellDependencia().getSelectedItem().toString(), p.getCboPrestacioEconomica().getSelectedItem().toString());
				//Insertar Informe Social
				modelo.insertarInformeSocial(codiPacient, p.getCboSituacioConvivencial().getSelectedItem().toString(), p.getCboHoresSol().getSelectedItem().toString(), p.getCboRelacioFamilia().getSelectedItem().toString(), p.getCboVisites().getSelectedItem().toString(), compatibilitat);
				//Insertar Ficha Sanitaria
				modelo.insertarFitxaSanitaria(codiPacient, Integer.parseInt(p.getTxtEstatura().getText()), Integer.parseInt(p.getTxtPes().getText()), p.getCboEstatActual().getSelectedItem().toString(), p.getTxtPatologia().getText(), p.getTxtAlergies().getText(), sist, p.getTxtAreaMalalties().getText(), p.getTxtAreaMedicacio().getText(), p.getCboCapacitatCognitiva().getSelectedItem().toString(), p.getTxtDificultats().getText(), p.getTxtVacunes().getText(), centreSanitari);
				
				//Insertar Servei Teleasistñncia
				if(!p.getTxtBanc1().getText().matches(regexBanco1)){
					JOptionPane.showMessageDialog(p, "Datos banco invalidos." , "Error", JOptionPane.ERROR_MESSAGE);
				}else if(!p.getTxtBanc2().getText().matches(regexBanco1)){
					JOptionPane.showMessageDialog(p, "Datos banco invalidos." , "Error", JOptionPane.ERROR_MESSAGE);
				}else if(!p.getTxtBanc3().getText().matches(regexBanco3)){
					JOptionPane.showMessageDialog(p, "Datos banco invalidos." , "Error", JOptionPane.ERROR_MESSAGE);
				}else if(!p.getTxtBanc4().getText().matches(regexBanco4)){
					JOptionPane.showMessageDialog(p, "Datos banco invalidos." , "Error", JOptionPane.ERROR_MESSAGE);
				}else if(!p.getTxtNomContractant().getText().matches(regexNombrePaciente)){
					JOptionPane.showMessageDialog(p, "Nombre contractante no vñlido." , "Error", JOptionPane.ERROR_MESSAGE);
				}else if(!p.getTxtCognomsContractant().getText().matches(regexNombrePaciente)){
					JOptionPane.showMessageDialog(p, "Apellidos contractante no vñlidos." , "Error", JOptionPane.ERROR_MESSAGE);
				}else if(!p.getTxtProvinciaContractant().getText().matches(regexInstrucciones)){
					JOptionPane.showMessageDialog(p, "Provincia contractante no vñlida." , "Error", JOptionPane.ERROR_MESSAGE);
				}else if(!p.getTxtTelefonContractant().getText().matches(regexTelefonos)){
					JOptionPane.showMessageDialog(p, "Telefono invñlido." , "Error", JOptionPane.ERROR_MESSAGE);
				}else if(!p.getTxtCodiPostalContractant().getText().matches(regexCodiPostal)){
					JOptionPane.showMessageDialog(p, "Cñdigo postal incorrecto." , "Error", JOptionPane.ERROR_MESSAGE);
				}else{
					if(modelo.isNifNie(p.getTxtDNIContractant().getText()) == true){
						modelo.insertarServeiTeleasistencia(codiPacient, Integer.parseInt(p.getCboAnyDataAlta().getSelectedItem().toString()), Integer.parseInt(p.getCboMesDataAlta().getSelectedItem().toString()), Integer.parseInt(p.getCboDiaDataAlta().getSelectedItem().toString()), quotes, dadesBanc, p.getTxtNomContractant().getText(), p.getTxtCognomsContractant().getText(), p.getTxtDNIContractant().getText(), p.getTxtTelefonContacte().getText(), p.getTxtProvinciaContractant().getText(), p.getTxtCodiPostalContractant().getText(), serveisBasics, modalitats, prestacionsAdicionals);
						
						//Metodo que introduce nuevo paciente en linphone.
						nomContra = p.getTxtNom().getText();
						cognomContra = p.getTxtCognoms().getText();
						dniContra = p.getTxtDni().getText();
						contraseña = nomContra.substring(0,3) + cognomContra.substring(0,3) + dniContra.substring(0,3);
						contraseñaLowerCase = contraseña.toLowerCase();
						if(!contraseñaLowerCase.equalsIgnoreCase("") && contraseñaLowerCase != null){
							CuentaPaciente.crearCuenta(p.getTxtDni().getText(), p.getTxtDni().getText(), contraseñaLowerCase);
							JOptionPane.showMessageDialog(p, "Se ha dado de alta correctamente en el servicio telefonico.");
						
							p.setVisible(false);
							ac.setVisible(false);
							b.setVisible(false);
							o.setVisible(false);
							bm.setVisible(false);
							contacto.setVisible(false);
							//Pestaña 1
							p.getTxtNomContacte().setText("");
							p.getCboSexeContacte().setSelectedIndex(0);
							p.getTxtMobilContacte().setText("");
							p.getTxtRelacio().setText("");
							p.getTxtPoblacioContacte().setText("");
							p.getTxtCodiPostalContacte().setText("");
							p.getTxtDomiciliContacte().setText("");
							p.getTxtAdrecaContacte().setText("");
							p.getTxtTelefonContacte().setText("");
							p.getCboFeina().setSelectedIndex(0);
							p.getCboHorariFeina().setSelectedIndex(0);
							p.getTxtEmailContacte().setText("");
							p.getCboClaus().setSelectedIndex(0);
							p.getTxtEdat().setText("");
							p.getCboSexe().setSelectedIndex(0);
							p.getTxtNom().setText("");
							p.getTxtCognoms().setText("");
							p.getCboAnyNaix().setSelectedIndex(0);
							p.getCboDiaNaix().setSelectedIndex(0);
							p.getCboMesNaix().setSelectedIndex(0);
							p.getTxtDni().setText("");
							p.getCboTipoVia().setSelectedIndex(0);
							p.getTxtAdreca().setText("");
							p.getTxtNumeroAdreca().setText("");
							p.getTxtPisAdreca().setText("");
							p.getTxtPortaCarrer().setText("");
							p.getTxtInstruccions().setText("");
							p.getTxtPoblacio().setText("");
							p.getTxtCodiPostal().setText("");
							p.getTxtTelefon1().setText("");
							p.getTxtTelefon2().setText("");
							p.getTxtEmail().setText("");
							//Pestaña 2
							p.getGroup1().clearSelection();
							p.getChkMuleta().setSelected(false);
							p.getChkCaminador().setSelected(false);
							p.getChkCadiraRodes().setSelected(false);
							p.getCboPrestacioEconomica().setSelectedIndex(0);
							p.getCboGrauDependencia().setSelectedIndex(0);
							p.getCboVisites().setSelectedIndex(0);
							p.getCboRelacioFamilia().setSelectedIndex(0);
							p.getCboNivellDependencia().setSelectedIndex(0);
							p.getCboSituacioConvivencial().setSelectedIndex(0);
							p.getCboHoresSol().setSelectedIndex(0);
							p.getChkCentreDia().setSelected(false);
							p.getChkCentreNit().setSelected(false);
							p.getChkAjudaDomicili().setSelected(false);
							p.getChkCaminador().setEnabled(false);
							p.getChkCadiraRodes().setEnabled(false);
							p.getChkMuleta().setEnabled(false);
							//Pestañas 3
							p.getGroup2().clearSelection();
							p.getChkOsteomuscular().setSelected(false);
							p.getChkSistNervios().setSelected(false);
							p.getChkReproductorExcretor().setSelected(false);
							p.getChkRespiratori().setSelected(false);
							p.getChkDigestiu().setSelected(false);
							p.getChkCardiovascular().setSelected(false);
							p.getTxtEstatura().setText("");
							p.getTxtPes().setText("");
							p.getCboEstatActual().setSelectedIndex(0);
							p.getTxtPatologia().setText("");
							p.getTxtAlergies().setText("");
							p.getTxtVacunes().setText("");
							p.getTxtDificultats().setText("");
							p.getTxtAreaMalalties().setText("");
							p.getTxtAreaMedicacio().setText("");
							p.getCboCapacitatCognitiva().setSelectedIndex(0);
							p.getTxtCAP().setText("");
							p.getTxtTelefonCAP().setText("");
							p.getTxtHospital().setText("");
							p.getTxtHospital2().setText("");
							p.getTxtMetge().setText("");
							p.getTxtMutua().setText("");
							p.getTxtMutua().setEnabled(false);
							p.getTxtHospital2().setEnabled(false);
							p.getTxtHospital().setEnabled(false);
							p.getTxtCAP().setEnabled(false);
							p.getTxtTelefonCAP().setEnabled(false);
							p.getTxtMetge().setEnabled(false);
							//Pestaña 4
							p.getCboAnyDataAlta().setSelectedIndex(0);
							p.getCboMesDataAlta().setSelectedIndex(0);
							p.getCboDiaDataAlta().setSelectedIndex(0);
							p.getChkDiposit().setSelected(false);
							p.getChkManteniment().setSelected(false);
							p.getChkUnicaQuota().setSelected(false);
							p.getTxtBanc1().setText("");
							p.getTxtBanc2().setText("");
							p.getTxtBanc3().setText("");
							p.getTxtBanc4().setText("");
							p.getTxtNomContractant().setText("");
							p.getTxtCognomsContractant().setText("");
							p.getTxtDNIContractant().setText("");
							p.getTxtCodiPostalContractant().setText("");
							p.getTxtProvinciaContractant().setText("");
							p.getTxtTelefonContractant().setText("");
							p.getChkDispositiuFixe().setSelected(false);
							p.getChkDispositiuInalambric().setSelected(false);
							p.getGroup4().clearSelection();
							p.getChkAgendesSeguiment().setSelected(false);
							p.getChkActivitats().setSelected(false);
							p.getChkCustodiaClaus().setSelected(false);
							p.getChkTelelocalitzacio().setSelected(false);
							p.getChkAlarmesDomotiques().setSelected(false);
							p.getChkSensorsMoviment().setSelected(false);
							p.getChkDetectorCaigudes().setSelected(false);
							p.getChkDetectorFum().setSelected(false);
							p.getChkDetectorGas().setSelected(false);
							p.getChkAperturaNevera().setSelected(false);
						}else{
							JOptionPane.showMessageDialog(p, "Los datos para dar de alta en el servicio telefonico son incorrectos.");
						}
					}else{
						JOptionPane.showMessageDialog(p, "DNI no valido.");
					}
				}			
			}
		}
	}

	
	public class buscarPacienteDarBaja implements ActionListener {
		private Borrar b;
		private ModeloMariaDB modelo;
		
		
		public buscarPacienteDarBaja(Borrar b, ModeloMariaDB modelo) {
			this.b = b;
			this.modelo = modelo;
		}
		
		@Override
		public void actionPerformed(ActionEvent e) {
			ArrayList<String> array = new ArrayList<String>(); 
			array = modelo.buscarNombresPaciente(b.getTxtDNIBorrar().getText());
			b.getTxtNombre().setText(array.get(0));
			b.getTxtApellidos().setText(array.get(1));
			
		}
	}
	
	public class darBaja implements ActionListener {
		private Borrar b;
		private ModeloMariaDB modelo;
		
		
		public darBaja(Borrar b, ModeloMariaDB modelo) {
			this.b = b;
			this.modelo = modelo;
		}
		
		@Override
		public void actionPerformed(ActionEvent e) {
			modelo.darBaja(b.getTxtDNIBorrar().getText());
			String telefono = modelo.buscarCodiPacient(b.getTxtDNIBorrar().getText());
			BajaPaciente.bajaPaciente(telefono); 
		}
	}
	
	public class sortirAplicacio implements ActionListener {

		private Pestañas		p;
		private ModeloMariaDB	modelo;

		public sortirAplicacio(Pestañas p, ModeloMariaDB modelo) {
			this.p = p;
			this.modelo = modelo;
		}

		@Override
		public void actionPerformed(ActionEvent e) {
			int result = JOptionPane.showConfirmDialog(p,"ñSeguro que quieres salir?", "Confirmar Salir",JOptionPane.YES_NO_OPTION);
			if (result == JOptionPane.YES_OPTION) {
				modelo.cerrarDB();
				System.exit(0);
			}
		}
	}
	
	public class salirBorrar implements ActionListener {
		private Borrar b;
		private ModeloMariaDB modelo;
		
		
		public salirBorrar(Borrar b, ModeloMariaDB modelo) {
			this.b = b;
			this.modelo = modelo;
		}
		@Override
		public void actionPerformed(ActionEvent arg0) {
			int result = JOptionPane.showConfirmDialog(b,"ñSeguro que quieres salir?", "Confirmar Salir",JOptionPane.YES_NO_OPTION);
			if (result == JOptionPane.YES_OPTION) {
				modelo.cerrarDB();
				System.exit(0);
			}
		}
	}
	
	
	public class ventanaOpciones implements ActionListener {
		private Pestañas p;
		private Borrar b;
		private Opciones o;
		private BuscarModificar modificar;
		private Vista v;
		private PestañasModificar pm;
		
		public ventanaOpciones(PestañasModificar pm, Pestañas p, Borrar b, Opciones o, BuscarModificar modificar, Vista v) {
			this.p = p;
			this.b = b;
			this.o = o;
			this.modificar = modificar;
			this.pm = pm;
			this.v = v;
		}

		@Override
		public void actionPerformed(ActionEvent e) {
			p.setVisible(false);
			b.setVisible(false);
			pm.setVisible(false);
		    modificar.setVisible(false);
			o.setVisible(true);
			v.setEnabled(false);
		}
	}
		
	public class cerrarOpciones extends WindowAdapter {
		private Opciones o;
		private Vista v;
		
		
		public cerrarOpciones(Opciones o, Vista v) {
			this.o = o;
			this.v = v;
		}
		
		@Override
		public void windowClosing(WindowEvent e) {
			o.getTxtIpAplicacion().setText("");
			o.getTxtIpTelefono().setText("");
			o.getTxtTonoLlamada().setText("");
			v.setEnabled(true);
			o.setVisible(false);
		}
	}
	
	public class cerrarBuscarModificar extends WindowAdapter {
		private Vista v;
		private BuscarModificar modificar;
		
		public cerrarBuscarModificar(Vista v, BuscarModificar modificar){
			this.v = v;
			this.modificar = modificar;
		}
		
		@Override
		public void windowClosing(WindowEvent e) {
			v.setEnabled(true);
			modificar.setVisible(false);
		}
	}
	
	public class examinar implements ActionListener {
		private Opciones o;
		private ModeloMariaDB modelo;
		
		public examinar(Opciones o, ModeloMariaDB modelo) {
			this.o = o;
			this.modelo = modelo;
		}

		@Override
		public void actionPerformed(ActionEvent e) {
			String ruta = modelo.promptForFile();
			if(ruta.length() > 0){
				o.getTxtTonoLlamada().setText(ruta);
			}
		}	
	}
	
	public class examinar2 implements ActionListener {
		private Opciones o;
		private ModeloMariaDB modelo;
		
	
		public examinar2(Opciones o, ModeloMariaDB modelo) {
			this.o = o;
			this.modelo = modelo;
		}

		@Override
		public void actionPerformed(ActionEvent e) {
			String ruta = modelo.promptForFile();
			if(ruta.length() > 0){
				o.getTxtTonoLlamadaSalida().setText(ruta);
			}
		}	
	}
		
	public class buscarModificar implements ActionListener{
		
		private BuscarModificar modificar;
		private Vista v;
		private Borrar b;
		private Pestañas p;
		private Opciones o;
		
		
		public buscarModificar(BuscarModificar modificar, Vista v, Borrar b, Pestañas p, Opciones o) {
			this.modificar = modificar;
			this.v = v;
			this.b = b;
			this.p = p;
			this.o = o;
		}

		@Override
		public void actionPerformed(ActionEvent arg0) {
			o.setVisible(false);
			p.setVisible(false);
			b.setVisible(false);
			modificar.setVisible(true);
			v.setEnabled(false);
		}
	}
	
	public class home implements ActionListener{
		private Pestañas p;
		private AñadirContacto ac;
		private Borrar b;
		private Opciones o;
		private BuscarModificar bm;
		private Contactos contacto;
		private PestañasModificar pm;
		
		public home(PestañasModificar pm,Pestañas p, AñadirContacto ac, Borrar b, Opciones o, BuscarModificar bm, Contactos contacto) {
			this.p = p;
			this.ac = ac;
			this.b = b;
			this.pm = pm;
			this.o = o;
			this.bm = bm;
			this.contacto = contacto;
		}
		
		@Override
		public void actionPerformed(ActionEvent arg0) {
			p.setVisible(false);
			ac.setVisible(false);
			b.setVisible(false);
			o.setVisible(false);
			bm.setVisible(false);
			contacto.setVisible(false);
			pm.setVisible(false);
		}
	}
	
	public class modificarIPTelefono implements ActionListener{
		private BajaPaciente baja;
		private CuentaPaciente alta;
		private Opciones o;
		
		public modificarIPTelefono(BajaPaciente baja, CuentaPaciente alta, Opciones o) {
			this.baja = baja;
			this.alta = alta;
			this.o = o;
		}

		@Override
		public void actionPerformed(ActionEvent e) {
			String regex = "(\\d{1,3})\\.(\\d{1,3})\\.(\\d{1,3})\\.(\\d{1,3})";
			String input = o.getTxtIpTelefono().getText();
			if(!o.getTxtIpAplicacion().getText().matches(regex) ){
				JOptionPane.showMessageDialog(o, "La IP introducida es incorrecta.");
			}else{
				 	String[] fn = input.split("\\.");
			        String ip1 = fn[0];
			        String ip2 = fn[1];
			        String ip3 = fn[2];
			        String ip4 = fn[3];	
				if(Integer.parseInt(ip1) >= 255 || Integer.parseInt(ip2) >= 255 || Integer.parseInt(ip3) >= 255 || Integer.parseInt(ip4) >= 255){
					JOptionPane.showMessageDialog(o, "El rango de la ip sale de los lñmites(255.255.255.255)");
				}else{
					alta.setIp(o.getTxtIpAplicacion().getText());
					baja.setIp(o.getTxtIpAplicacion().getText());
					if(o.getTxtTonoLlamada().getText().length()>0){
						CONF.cambiarSonidoEntrante(o.getTxtTonoLlamada().getText());
					}
					if(o.getTxtTonoLlamadaSalida().getText().length()>0){
						CONF.cambiarSonidoLlamar(o.getTxtTonoLlamadaSalida().getText());
					}
				}
			}
		}
	}
	
	public class cambiarDatosDB implements ActionListener{
		private Opciones o;
		
		public cambiarDatosDB(Opciones o){
			this.o = o;
		}
		
		@Override
		public void actionPerformed(ActionEvent arg0) {
			String regex = "(\\d{1,3})\\.(\\d{1,3})\\.(\\d{1,3})\\.(\\d{1,3})";
			String input = o.getTxtIpTelefono().getText();
			if(!o.getTxtIpDb().getText().matches(regex) ){
				JOptionPane.showMessageDialog(o, "La IP introducida es incorrecta.");
			}else{
				 	String[] fn = input.split("\\.");
			        String ip1 = fn[0];
			        String ip2 = fn[1];
			        String ip3 = fn[2];
			        String ip4 = fn[3];	
				if(Integer.parseInt(ip1) >= 255 || Integer.parseInt(ip2) >= 255 || Integer.parseInt(ip3) >= 255 || Integer.parseInt(ip4) >= 255){
					JOptionPane.showMessageDialog(o, "El rango de la ip sale de los lñmites(255.255.255.255)");
				}else{
					CONF.cambiarIPDB(o.getTxtIpDb().getText());
					if(o.getTxtPuertoDb().getText().length() == 4){
						CONF.cambiarPuertoDB(o.getTxtPuertoDb().getText());
					}
					if(o.getTxtNombreDb().getText().length()>0){
						CONF.cambiarNameDB(o.getTxtNombreDb().getText());
					}
				}
			}
		}
	}
	
	public class enProceso implements ActionListener {
		private Vista v;
		
		public enProceso(Vista v){
			this.v = v;
		}

		@Override
		public void actionPerformed(ActionEvent e) {
			JOptionPane.showMessageDialog (v, "Este apartado de la aplicaciñn esta en proceso, sentimos la tardanza.", "EN PROCESO", JOptionPane.INFORMATION_MESSAGE);
		}
	}
}
