-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Versión del servidor:         5.5.5-10.0.16-MariaDB - mariadb.org binary distribution
-- SO del servidor:              Win32
-- HeidiSQL Versión:             8.3.0.4694
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Borrar estructura de base de datos de yayotech antigua;
/*
DROP TABLE IF EXISTS `contacte`;
DROP TABLE IF EXISTS `empleats`;
DROP TABLE IF EXISTS `fitxasanitaria`;
DROP TABLE IF EXISTS `pacient`;
DROP TABLE IF EXISTS `serveiteleasistencia`;
*/

-- Borrar todas las tablas, de Timber ( para pruebas con los identificadores).

DROP TABLE IF EXISTS `informesocial`;
DROP TABLE IF EXISTS `servicioteleasistencia`;
DROP TABLE IF EXISTS `fichasanitaria`;
DROP TABLE IF EXISTS `dependencia`;
DROP TABLE IF EXISTS `llamada`;
DROP TABLE IF EXISTS `contacto`;
DROP TABLE IF EXISTS `empleado`;
DROP TABLE IF EXISTS `paciente`;

-- Volcando estructura de base de datos para Timber
CREATE DATABASE IF NOT EXISTS `Timber` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `Timber`;


-- La exportación de datos fue deseleccionada.

-- Volcando estructura para tabla Timber.paciente
CREATE TABLE IF NOT EXISTS `paciente` (
  `id_paciente` int(11) NOT NULL AUTO_INCREMENT,
  `edad` int(3) NOT NULL DEFAULT '0',
  `sexo` varchar(50) NOT NULL,
  `nombre` varchar(50) NOT NULL,
  `apellido1` varchar(50) NOT NULL,
  `apellido2` varchar(50) NOT NULL,
  `fecha_nacimiento` date NOT NULL,
  `dni` varchar(9) NOT NULL,
  `direccion` varchar(50) NOT NULL,
  `referencias` varchar(50) NOT NULL,
  `poblacion` varchar(50) NOT NULL,
  `c_postal` varchar(50) NOT NULL,
  `tlf_fijo` int(9),
  `tlf_mobil` int(9),
  `email` varchar(50) DEFAULT NULL,
  `alta` char(1) NOT NULL DEFAULT 'S',
  PRIMARY KEY (`id_paciente`),
  UNIQUE KEY `dni` (`dni`)
) ENGINE=InnoDB;

-- Volcando estructura para tabla Timber.contacto
CREATE TABLE IF NOT EXISTS `contacto` (
  `id_contacto` int(11) NOT NULL AUTO_INCREMENT,
  `id_paciente` int(11) NOT NULL,
  `nombre` varchar(50) NOT NULL,
  `apellido1` varchar(50) NOT NULL,
  `apellido2` varchar(50) NOT NULL,
  `sexo` varchar(50) NOT NULL,
  `relacion_paciente` varchar(50) NOT NULL,
  `poblacion` varchar(50) NOT NULL,
  `c_postal` varchar(50) NOT NULL,
  `direccion` varchar(50) NOT NULL,
  `telefono` varchar(50) NOT NULL,
  `trabajo` varchar(50) NOT NULL,
  `horario_trabajo` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `llaves` varchar(50) NOT NULL,
  PRIMARY KEY (`id_contacto`,`id_paciente`),
  KEY `FK1_codiPaciente` (`id_paciente`),
  CONSTRAINT `FK1_codiPaciente` FOREIGN KEY (`id_paciente`) REFERENCES `paciente` (`id_paciente`)
) ENGINE=InnoDB;

-- La exportación de datos fue deseleccionada.


-- Volcando estructura para tabla Timber.dependencia
CREATE TABLE IF NOT EXISTS `dependencia` (
  `id_paciente` int(11) NOT NULL,
  `deambulacion` varchar(50) DEFAULT NULL,
  `grado_dependencia` varchar(50) DEFAULT NULL,
  `nivel_dependencia` varchar(50) DEFAULT NULL,
  `prestacion_economica` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id_paciente`),
  CONSTRAINT `FK1_asd` FOREIGN KEY (`id_paciente`) REFERENCES `paciente` (`id_paciente`)
) ENGINE=InnoDB;

-- La exportación de datos fue deseleccionada.


-- Volcando estructura para tabla Timber.empleado
CREATE TABLE IF NOT EXISTS `empleado` (
  `id_empleado` int(11) NOT NULL AUTO_INCREMENT,
  `dni` varchar(9) DEFAULT '',
  `nombre` varchar(50) DEFAULT NULL,
  `apellido1` varchar(50) DEFAULT NULL,
  `apellido2` varchar(50) DEFAULT NULL,
  `fecha_nacimiento` date DEFAULT NULL,
  `esAdmin` varchar(2) DEFAULT NULL,
  `usuario` varchar(50) DEFAULT NULL,
  `contrasenya` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id_empleado`),
  UNIQUE KEY `dni` (`dni`),
  UNIQUE KEY `usuario` (`usuario`)
) ENGINE=InnoDB;

-- La exportación de datos fue deseleccionada.


-- Volcando estructura para tabla Timber.fichasanitaria
CREATE TABLE IF NOT EXISTS `fichasanitaria` (
  `id_paciente` int(11) NOT NULL,
  `estatura` decimal(3,2) NOT NULL,
  `peso` decimal(5,2) NOT NULL,
  `estadoActual` varchar(200) NOT NULL,
  `patologia` varchar(200) NOT NULL,
  `alergias` varchar(200) NOT NULL,
  `sistemasAfectados` varchar(300) NOT NULL,
  `enfermedades` varchar(300) NOT NULL,
  `medicacion` varchar(150) NOT NULL,
  `capacidadCognitiva` varchar(200) NOT NULL,
  `dificultades` varchar(200) DEFAULT NULL,
  `vacunas` varchar(200) DEFAULT NULL,
  `centroSanitario` varchar(300) NOT NULL,
  PRIMARY KEY (`id_paciente`),
  CONSTRAINT `FK1_codiPaciente23` FOREIGN KEY (`id_paciente`) REFERENCES `paciente` (`id_paciente`)
) ENGINE=InnoDB;

-- La exportación de datos fue deseleccionada.


-- Volcando estructura para tabla Timber.informesocial
CREATE TABLE IF NOT EXISTS `informesocial` (
  `id_paciente` int(11) NOT NULL,
  `convivencia` varchar(100) NOT NULL,
  `horarioSol` varchar(100) NOT NULL,
  `relacionFamilia` varchar(100) NOT NULL,
  `visitas` varchar(100) NOT NULL,
  `compatibilidad` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id_paciente`),
  CONSTRAINT `FK1_codiPaciente24` FOREIGN KEY (`id_paciente`) REFERENCES `paciente` (`id_paciente`)
) ENGINE=InnoDB;

-- La exportación de datos fue deseleccionada.

-- Volcando estructura para tabla Timber.servicioteleasistencia
CREATE TABLE IF NOT EXISTS `servicioteleasistencia` (
  `id_paciente` int(11) NOT NULL,
  `fecha_alta` date NOT NULL,
  `cuotasServicio` varchar(50) NOT NULL,
  `datosBanco` varchar(100) NOT NULL,
  `nombreContratador` varchar(200) NOT NULL,
  `apellido1Contratador` varchar(200) NOT NULL,
  `apellido2Contratador` varchar(200) NOT NULL,
  `dniContratador` varchar(10) NOT NULL,
  `telefonoContratador` varchar(20) NOT NULL,
  `provinciaContratador` varchar(20) NOT NULL,
  `cpostalContratador` varchar(20) NOT NULL,
  `serviciosBasicos` varchar(200) NOT NULL,
  `modalidades` varchar(200) NOT NULL,
  `prestacionesAdicionales` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id_paciente`),
  UNIQUE KEY `dniContratador` (`dniContratador`),
  CONSTRAINT `FK1_cdiPaciente` FOREIGN KEY (`id_paciente`) REFERENCES `paciente` (`id_paciente`)
) ENGINE=InnoDB;

-- Volcando estructura para tabla Timber.llamada
/*CREATE TABLE IF NOT EXISTS `llamada` (
  `id_llamada` int(11) NOT NULL AUTO_INCREMENT,
  `id_paciente` int(11) NOT NULL,
  `id_contacto` int(11) NOT NULL,
  `id_empleado` int(11) NOT NULL,
  `inicio_llamada` datetime NOT NULL,
  `fin_llamada` datetime NOT NULL,
  PRIMARY KEY (`id_paciente`,`id_contacto`,`id_empleado`,`inicio_llamada`),
  CONSTRAINT `FK1_cdiPaciente1` FOREIGN KEY (`id_paciente`) REFERENCES `paciente` (`id_paciente`),
  CONSTRAINT `FK1_cdiContacto1` FOREIGN KEY (`id_contacto`) REFERENCES `contacto` (`id_contacto`),
  CONSTRAINT `FK1_cdiEmpleado1` FOREIGN KEY (`id_empleado`) REFERENCES `empleado` (`id_empleado`) 
) ENGINE=InnoDB;*/

-- La exportación de datos fue deseleccionada.
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;

-- Volcado de datos.
-- Da error de carga en los apellidos con acento.
-- INSERT INTO empleado (`dni`,`nombre`,`apellido1`, `apellido2`, `fecha_nacimiento`,`esAdmin`,`usuario`,`contrasenya`) VALUES ('46476282K','Manolita','Martos', 'Lima','1994-11-06','NO','Manoli','');
-- INSERT INTO empleado (`dni`,`nombre`,`apellido1`, `apellido2`, `fecha_nacimiento`,`esAdmin`,`usuario`,`contrasenya`) VALUES ('12345678Z','Alba','Conde', 'Rodriguez','1994-11-06','SI','Cleo','Timber');
-- INSERT INTO empleado (`dni`,`nombre`,`apellido1`, `apellido2`, `fecha_nacimiento`,`esAdmin`,`usuario`,`contrasenya`) VALUES ('12345678A','Zoair','Ayadi', 'Rharbi','1994-11-06','SI','Zoair','Timber');
-- INSERT INTO empleado (`dni`,`nombre`,`apellido1`, `apellido2`, `fecha_nacimiento`,`esAdmin`,`usuario`,`contrasenya`) VALUES ('12345678B','Xavi','Navarro', '','1994-11-06','SI','Xavi','Timber');
-- 
-- INSERT INTO empleado (`dni`,`nombre`,`apellido1`, `apellido2`, `fecha_nacimiento`,`esAdmin`,`usuario`,`contrasenya`) VALUES ('12345678C','Empleado1','apellido1', 'apellido1','1994-11-06','NO','','');
-- 
-- INSERT INTO empleado (`dni`,`nombre`,`apellido1`,`apellido2`,`fecha_nacimiento`,`esAdmin`,`usuario`,`contrasenya`) VALUES ('46476582K','Pascual','Baena','López','1950-12-07','NO','Pascual','');
-- INSERT INTO empleado (`dni`,`nombre`,`apellido1`,`apellido2`,`fecha_nacimiento`,`esAdmin`,`usuario`,`contrasenya`) VALUES ('12775678Z','Pamela','Anderson','Viles','1967-01-05','NO','Pamela','');
-- INSERT INTO empleado (`dni`,`nombre`,`apellido1`,`apellido2`,`fecha_nacimiento`,`esAdmin`,`usuario`,`contrasenya`) VALUES ('12345688Z','Pol','Baena','Lamas','1991-07-13','NO','Pol','');
-- INSERT INTO empleado (`dni`,`nombre`,`apellido1`,`apellido2`,`fecha_nacimiento`,`esAdmin`,`usuario`,`contrasenya`) VALUES ('12345678Z','Alejandro','Lomas','Miró','1972-05-01','NO','Alex','');
-- 
-- 
-- INSERT INTO empleado (`dni`,`nombre`,`apellido1`,`apellido2`,`fecha_nacimiento`,`esAdmin`,`usuario`,`contrasenya`) VALUES ('45476582K','Borja','De Luna','Padilla','1986-11-07','NO','Borja','');
-- INSERT INTO empleado (`dni`,`nombre`,`apellido1`,`apellido2`,`fecha_nacimiento`,`esAdmin`,`usuario`,`contrasenya`) VALUES ('12005678Z','Agustín','Castillo','Pérez','1955-02-04','NO','Agus','');
-- 
-- INSERT INTO empleado (`dni`,`nombre`,`apellido1`,`apellido2`,`fecha_nacimiento`,`esAdmin`,`usuario`,`contrasenya`) VALUES ('35476522K','Laura','Saéz','Rueda','1985-10-10','NO','Laura','');
-- INSERT INTO empleado (`dni`,`nombre`,`apellido1`,`apellido2`,`fecha_nacimiento`,`esAdmin`,`usuario`,`contrasenya`) VALUES ('15005608Z','Penélope','Cruz','Carmona','1978-04-04','NO','Pe','');


