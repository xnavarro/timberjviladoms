<%-- 
    Document   : formularioDatosDeContactos
    Created on : Apr 26, 2016, 6:45:12 PM
    Author     : root
--%>
<%@page contentType="text/html" pageEncoding="UTF-8" language="java"%>
<%@ page import="java.io.*,
         java.util.*" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<c:set var="language" value="${not empty param.language ? param.language : not empty language ? language : pageContext.request.locale}" scope="session" />
<fmt:setLocale value="${language}" />
<fmt:setBundle basename="/messages/app" />
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.2/jquery.min.js"></script>
        <link rel="stylesheet" type="text/css" href="skin/css/style.css">
        <script type="text/javascript">
            $(document).on("ready", function () {
                $("#horarioTrabajo").hide();
                $(".form-group").on("click", function () {
                    if ($("#trabajo").is(":checked")) {
                        $("#horarioTrabajo").show();
                    } else {
                        $("#horarioTrabajo").hide();
                    }
                });
                $("#modalForm :input").each(function () {
                    var array = new Array();
                    array.push($(this).val());
                });
            });
        </script>
    </head>

    <body>
        <div class="row p-x-1">
            <form id="modalForm" action="ControladorLanding" role="form" class="form-horizontal">
                <div class="col-sm-4 col-xs-12">
                    <label class="control-label" for="nombre"></label>
                    <input type="text" class="form-control" id="nombre" placeholder=<fmt:message key="patient.pd.name" /> name="nombre">
                </div>
                <div class="col-sm-4 col-xs-12">
                    <label class="control-label" for="apellido1"></label>
                    <input type="text" class="form-control" id="apellido1" placeholder=<fmt:message key="patient.pd.surname1" /> name="apellido1">
                </div>
                <div class="col-sm-4 col-xs-12">
                    <label class="control-label" for="apellido2"></label>
                    <input type="text" class="form-control" id="apellido2" placeholder=<fmt:message key="patient.pd.surname2" /> name="apellido2">
                </div>
                <div class="col-sm-6 col-xs-12 m-t-1">
                    <label for="sexo" class="control-label"><fmt:message key="patient.pd.sex" /></label>
                    <select class="form-control" id="sexo" name="sexo">
                        <option value="hombre"><fmt:message key="patient.pd.man" /></option>
                        <option value="mujer"><fmt:message key="patient.pd.woman" /></option>
                    </select>
                </div>
                <div class="col-sm-6 col-xs-12 m-t-1">
                    <label class="control-label" for="relacionConPaciente"><fmt:message key="patient.cd.relationship" /></label>
                    <select class="form-control" name="relacionConPaciente" id="relacionConPaciente">
                        <option value="buena" selected="selected"><fmt:message key="patient.cd.relationship.good" /></option>	
                        <option value="mala"><fmt:message key="patient.cd.relationship.bad" /></option>
                        <option value="nula"><fmt:message key="patient.cd.relationship.nonexistant" /></option>
                    </select>
                </div>
                <div class="col-sm-12 col-xs-12">
                    <label for="direccion"></label>
                    <input type="text" class="form-control" id="direccion" placeholder=<fmt:message key="patient.pd.address" /> name="direccion">
                </div>
                <div class="col-sm-8 col-xs-12">
                    <label for="poblacion"></label>
                    <input type="text" class="form-control" id="poblacion" placeholder=<fmt:message key="patient.pd.town" /> name="poblacion">
                </div>
                <div class="col-sm-4 col-xs-12">
                    <label for="cPostal"></label>
                    <input type="text" class="form-control" id="cPostal" placeholder=<fmt:message key="patient.pd.postcode" /> name="cPostal">
                </div>
                <div class="col-sm-6 col-xs-12">
                    <label for="tlf"></label>
                    <input type="text" class="form-control" id="tlf" placeholder=<fmt:message key="patient.cd.contact" /> name="tlf">
                </div>  
                <div class="col-sm-6 col-xs-12">
                    <label for="email"></label>
                    <input type="email" class="form-control" id="email" name="email"  placeholder= <fmt:message key="patient.pd.email"/> >
                </div>
                <div class="form-group m-x-0 col-xs-12 col-sm-12 m-t-2">
                    <div class="col-xs-12 col-sm-12">
                        <label class="control-label" for="trabajo">
                            <input type="checkbox" id="trabajo" name="trabajo"/> <fmt:message key="patient.cd.work" />
                        </label>
                    </div>
                    <div id="horarioTrabajo" >
                        <div class="col-xs-6">
                            <label class="control-label" for="horario"><fmt:message key="patient.cd.workinghours" /></label>
                            <input type="text" name="horario" id="horarioDesde" class="form-control" placeholder=<fmt:message key="patient.cd.workinghours.from" /> />
                        </div>
                        <div class="col-xs-6">
                            <label class="control-label"> &nbsp; </label>
                            <input type="text" name="horario" id="horarioHasta" class="form-control" placeholder=<fmt:message key="patient.cd.workinghours.to" /> />
                        </div>
                    </div>
                </div>
                <label class="col-xs-12 col-sm-12" for="trabajo">
                    <input type="checkbox" id="trabajo" name="trabajo"/> <fmt:message key="patient.cd.keys" />
                </label>
            </form>
        </div>
    </body>
</html>