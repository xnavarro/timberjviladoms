<%-- 
    Document   : formularioInformeDependencia
    Created on : Apr 26, 2016, 6:46:05 PM
    Author     : root
--%>
<%@page contentType="text/html" pageEncoding="UTF-8" language="java"%>
<%@ page import="java.io.*,
         java.util.*" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<c:set var="language" value="${not empty param.language ? param.language : not empty language ? language : pageContext.request.locale}" scope="session" />
<fmt:setLocale value="${language}" />
<fmt:setBundle basename="/messages/app" /> 
<!DOCTYPE html>
<html>


    <script>
        $(document).on("ready", function () {
            $("#productoSoporte").hide();

            $(".form-group").on("click", function () {
                if ($("#autonomoConSoporte").is(":checked")) {
                    $("#productoSoporte").show();
                } else {
                    $("#productoSoporte").hide();
                }
            });

            $("#siguiente").on("click", function () {
                localStorage.setItem("anterior1", window.location.href);
                $("form").attr("action", "formularioFichaSanitaria.jsp");
                $("form").submit();
            });

            $("#atras").on("click", function () {
                $("form").attr("action", localStorage.getItem("anterior0"));
                $("form").submit();
            });
        });
    </script>
    <body>
        <div class="row p-a-2">	
            <fieldset>	
                <legend  class="formLegend"><fmt:message key="persons.label.socialcare" /></legend>
                <form role="form" class="" name="infdep" method="POST">
                    <!-- DEAMBULACIO -->
                    <fieldset>
                        <legend class="formLegend2 p-a-2"><fmt:message key="patient.dep.motricity" /></legend>
                        <div class="form-group col-md-12">
                            <div class="col-sm-3 col-xs-12">
                                <label class="" for="deambulacio"><input type="radio" id="autonomo" name="deambulacio" value="Autónomo">
                                    <fmt:message key="patient.dep.autonomous" />

                                </label>
                            </div>

                            <div class="col-sm-3 col-xs-12">
                                <label for="deambulacio">
                                    <input type="radio" id="autonomoConSoporte" name="deambulacio" value="Autónomo con soporte">
                                    <fmt:message key="patient.dep.autsupport" />

                                </label>
                                <div id="productoSoporte" class="controls span2 col-sm-offset-2 control-group">
                                    <label class="checkbox" for="baston">
                                        <input type="checkbox" class="" id="baston" name="baston" value="Bastón"> <fmt:message key="patient.dep.cane" />
                                    </label>

                                    <label class="checkbox" for="andador">
                                        <input type="checkbox" class="" id="andador" name="andador" value="Andador"> <fmt:message key="patient.dep.walker" />
                                    </label>

                                    <label class="checkbox" for="silla">
                                        <input type="checkbox" class="" id="silla" name="silla" value="Silla de ruedas"> <fmt:message key="patient.dep.wheelchair" />
                                    </label>
                                </div>
                            </div>

                            <div class="col-sm-3 col-xs-12">
                                <label for="encamado">
                                    <input type="radio" id="encamado" name="deambulacio" value="Encamado">
                                    <fmt:message key="patient.dep.inbed" />

                                </label>
                            </div>


                            <span class="col-sm-3 col-xs-12">
                                <label for="ayudaExterna">
                                    <input type="radio" id="ayudaExterna" name="deambulacio" value="Ayuda externa">
                                    <fmt:message key="patient.dep.help" /></label>

                            </span>
                        </div>
                    </fieldset>	

                    <!-- DEPENDECIA -->
                    <fieldset>
                        <legend class="formLegend2 p-a-2"><fmt:message key="patient.dep.dependancy" /></legend>
                        <div class="form-group">
                            <label for="gradoDependencia"><fmt:message key="patient.dep.dependancydeg" /></label>
                            <span class="col-xs-12 col-sm-5 pull-right">
                                <select class="form-control" name="gradoDependencia" id="gradoDependecia">
                                    <option value="menor33" selected="selected"><fmt:message key="patient.dep.under33" /></option>	
                                    <option value="menor65"><fmt:message key="patient.dep.under66" /></option>	
                                    <option value="mayor65"><fmt:message key="patient.dep.over66" /></option>	
                                </select>
                            </span>
                        </div>

                        <div class="form-group">
                            <label for="gradoDependenciaLegal"><fmt:message key="patient.dep.legaldependancy" /></label>
                            <span class="col-xs-12 col-sm-5 pull-right">
                                <select class="form-control" name="gradoDependenciaLegal" id="gradoDependenciaLegal">
                                    <option value="menor33" selected="selected"><fmt:message key="patient.dep.under33" /></option>	
                                    <option value="menor65"><fmt:message key="patient.dep.under66" /></option>	
                                    <option value="mayor65"><fmt:message key="patient.dep.over66" /></option>		
                                </select>
                            </span>
                        </div>
                        <div class="form-group">
                            <label for="prestacionEconomica"><fmt:message key="patient.dep.benefits" /> </label>
                            <span class="col-xs-12 col-sm-5 pull-right">
                                <select class="form-control" name="prestacionEconomica" id="prestacionEconomica">
                                    <option value="SI" selected="selected"><fmt:message key="persons.label.yes" /></option>	
                                    <option value="NO"><fmt:message key="persons.label.no" /></option>
                                </select>
                            </span>
                        </div>
                    </fieldset>


                    <!-- INFORME SOCIAL -->
                    <fieldset>
                        <legend class="formLegend2 p-a-2"><fmt:message key="patient.dep.socsit" /></legend>
                        <div class="form-group">
                            <label for="situacionConvivencia"><fmt:message key="patient.dep.cohabitation" /></label>
                            <span class="col-xs-12 col-sm-5 pull-right">
                                <select class="form-control" name="situacionConvivencia" id="situacionConvivencia">
                                    <option value="solo" selected="selected"><fmt:message key="patient.dep.alone" /></option>	
                                    <option value="noSolo"><fmt:message key="patient.dep.company" /></option>
                                </select>
                            </span>
                        </div>
                        <div class="form-group">
                            <label for="horarioSolo"><fmt:message key="patient.dep.hoursalone" /></label>
                            <span class="col-xs-12 col-sm-5 pull-right">
                                <select class="form-control" name="horarioSolo" id="horarioSolo">
                                    <option value="mañana" selected="selected"><fmt:message key="patient.dep.hours.morning" /></option>	
                                    <option value="tarde"><fmt:message key="patient.dep.hours.afternoon" /></option>
                                    <option value="noche"><fmt:message key="patient.dep.hours.nights" /></option>
                                </select>
                            </span>
                        </div>
                        <div class="form-group">
                            <label for="relacion"><fmt:message key="patient.dep.familyrel" /></label>
                            <span class="col-xs-12 col-sm-5 pull-right">
                                <select class="form-control" name="relacion" id="relacion">
                                    <option value="buena" selected="selected"><fmt:message key="patient.cd.relationship.good" /></option>	
                                    <option value="mala"><fmt:message key="patient.cd.relationship.bad" /></option>
                                    <option value="nula"><fmt:message key="patient.cd.relationship.nonexistant" /></option>
                                </select>
                            </span>
                        </div>
                        <div class="form-group">
                            <label for="visitas"><fmt:message key="patient.dep.familyvis" /></label>
                            <span class="col-xs-12 col-sm-5 pull-right">
                                <select class="form-control" name="visitas" id="visitas">
                                    <option value="diaria" selected="selected"><fmt:message key="patient.dep.daily" /></option>	
                                    <option value="semanal"><fmt:message key="patient.dep.weekly" /></option>
                                    <option value="mensual"><fmt:message key="patient.dep.monthly" /></option>
                                    <option value="nunca"><fmt:message key="patient.dep.never" /></option>
                                </select>
                            </span>
                        </div>
                        <div class="form-group">
                            <label for="ayudaDomicilio" class =""><fmt:message key="patient.dep.compatible" /></label>

                            <div class="col-sm-12">
                                <div class="controls">
                                    <label class="checkbox col-sm-12 col-sm-offset-2 text_subcheck" for="ayudaDomicilio">
                                        <input type="checkbox" class="" id="ayudaDomicilio" name="ayudaDomicilio" value="Ayuda a domicilio"> <fmt:message key="patient.dep.homehelp" />
                                    </label>

                                    <label class="checkbox col-sm-12 col-sm-offset-2 text_subcheck" for="centroDia">
                                        <input type="checkbox" class="" id="centroDia" name="centroDia" value="Centro de dia"> <fmt:message key="patient.dep.daycare" />
                                    </label>

                                    <label class="checkbox col-sm-12 col-sm-offset-2 text_subcheck" for="centroNoche">
                                        <input type="checkbox" class="" id="centroNoche" name="centroNoche" value="Centro de noche"> <fmt:message key="patient.dep.nightcare" />
                                    </label>
                                </div>
                            </div>
                        </div>
                    </fieldset>
                    <div class="alert alert-danger errorGeneral" role="alert">
                        <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
                        <fmt:message key="form.error.empty_fields" />
                    </div>
                </form>
            </fieldset>
        </div>

    </body>
</html>