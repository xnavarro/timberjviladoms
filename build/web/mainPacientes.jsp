<%-- 
    Document   : mainPacientes
    Created on : 26-abr-2016, 6:21:22
    Author     : Yo
--%>
<%@page contentType="text/html" pageEncoding="UTF-8" language="java"%>
<%@ page import="java.io.*,
         java.util.*" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<c:set var="language" value="${not empty param.language ? param.language : not empty language ? language : pageContext.request.locale}" scope="session" />
<fmt:setLocale value="${language}" />
<fmt:setBundle basename="/messages/app" /> 
<!DOCTYPE html>
<html>
    <head>
        <link rel="stylesheet" href="https://cdn.datatables.net/1.10.11/css/jquery.dataTables.min.css"/>
        <link rel="stylesheet" href="bootstrap-3.3.6-dist/css/bootstrap.css" >
        <script src="bootstrap-3.3.6-dist/js/bootstrap.js" ></script>
        <link rel="stylesheet" type="text/css" href="skin/css/style.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.2/jquery.min.js"></script>
        <script src="https://cdn.datatables.net/1.10.11/js/jquery.dataTables.min.js"></script> <!-- Para las DataTables() -->
        <script src="bootstrap-3.3.6-dist/js/bootstrap.js" ></script>
    </head>
    <script type="text/javascript">
    </script>
    <body>


        <div id="info" class="container">
            <table id="tablaInfo" class="table table-responsive table-striped display">
                <thead>
                    <tr>
                        <th width="25%"></th>
                        <th width="20%"><fmt:message key="persons.label.id" /></th>
                        <th width="35%"><fmt:message key="persons.label.namesurname" /></th>
                        <th width="20%"><fmt:message key="persons.label.birthdate" /></th>
                    </tr>
                </thead>
                <tbody>
                    
                     <c:forEach items="${pacientes}" var="paciente">
                        <tr>
                            <td class="botonera col-xs-2"> 
                            
                                <span class="col-xs-3">
                                    <button class="btn btn-sm btn-primary pac-modificar" onclick="javascript:lanzarUrl(this);" data-action="modificar" data-type="pac">
                                        <span class="glyphicon glyphicon-eye-open"></span>
                                    </button>
                                </span>
                                <span class="col-xs-3">
                                    <button class="btn btn-sm btn-success pac-llamar" onclick="javascript:lanzarUrl(this);" data-action="llamar" data-type="pac">
                                        <span class="glyphicon glyphicon-phone-alt"></span>
                                    </button>
                                </span>
                                <span class="col-xs-3">
                                    <button class="btn btn-sm btn-warning pac-consultar" onclick="javascript:lanzarUrl(this);" data-action="consultar" data-type="pac">
                                        <span class="glyphicon glyphicon-pencil"></span>
                                    </button>
                                </span>
                                <span class="col-xs-3">
                                    <button class="btn btn-sm btn-danger pac-borrar accion" data-action="borrar" data-type="pac">
                                        <span class="glyphicon glyphicon-trash"></span>
                                    </button>
                                </span>
                            
                            </td>
                            <td class="dni"><c:out value="${paciente.dni}"/></td>
                        <td><c:out value="${paciente.nombre} ${paciente.apellido1} ${paciente.apellido2}"/></td>
                        <td><fmt:formatDate value="${paciente.fechaNacimiento}" pattern="dd-MM-yyyy"/></td> 
                        </tr>
                     </c:forEach>
                </tbody>
            </table>
        </div>
    </body>
</html>
