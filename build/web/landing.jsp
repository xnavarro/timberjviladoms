<%-- 
    Document   : mainPacientes
    Created on : 26-abr-2016, 6:21:22
    Author     : Yo
--%>
<%@page contentType="text/html" pageEncoding="UTF-8" language="java"%>
<%@ page import="java.io.*,
         java.util.*" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<c:set var="language" value="${not empty param.language ? param.language : not empty language ? language : pageContext.request.locale}" scope="session" />
<fmt:setLocale value="${language}" />
<fmt:setBundle basename="/messages/app" /> 
<!DOCTYPE html>
<html>
    <head>

        <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.2/jquery.min.js"></script>
        <script src="https://cdn.datatables.net/1.10.11/js/jquery.dataTables.min.js"></script> <!-- Para las DataTables() -->
        <link href="https://cdn.datatables.net/1.10.11/css/dataTables.bootstrap.min.css"/>
        <link rel="stylesheet" href="bootstrap-3.3.6-dist/css/bootstrap.css" >
        <script src="bootstrap-3.3.6-dist/js/bootstrap.js" ></script>

        <link rel="stylesheet" type="text/css" href="skin/css/style.css">
    </head>
    
    <body>
        <jsp:include page="topmenu.jsp">
            <jsp:param name="myVar" value="<%=(String)request.getParameter("goingTo")%>"/>
        </jsp:include>


        <div id="infoContainer" class="container">
            <h1><fmt:message key="patients.label.top" /></h1>
            <hr>            
            <jsp:include page="search.jsp"/>
            <jsp:include page="<%=(String)request.getAttribute("pageToGo")%>"/>
        </div>
    </body>
</html>
