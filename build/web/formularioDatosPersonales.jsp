<%-- 
    Document   : formularioDatosPresonales
    Created on : Apr 26, 2016, 6:23:11 PM
    Author     : root
--%>
<%@page contentType="text/html" pageEncoding="UTF-8" language="java"%>
<%@ page import="java.io.*,
         java.util.*" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<c:set var="language" value="${not empty param.language ? param.language : not empty language ? language : pageContext.request.locale}" scope="session" />
<fmt:setLocale value="${language}" />
<fmt:setBundle basename="/messages/app" /> 
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8"/>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.2/jquery.min.js"></script>
        <link rel="stylesheet" href="bootstrap-3.3.6-dist/css/bootstrap.css" >
        <link rel="stylesheet" type="text/css" href="skin/css/style.css">

    </head>
    <script type="text/javascript">
        $(document).on("ready", function () {
            function guardarDatosEnCookie() {
                var $inputs = $('#modalForm :input');
                var values = {};
                $inputs.each(function () {
                    values[this.name] = $(this).val();
                });
                localStorage.setItem(values["nombre"], $('#modalForm :input').serialize());
            }

            function getStorage(datos, dato) {
                var campos = datos.split("&").join("=").split("+").join(" ").split("=");
                console.log(campos);
                var param = campos[campos.indexOf(dato) + 1];
                return param;
            }

            $("#nuevoContacto, #finalizar").on("click", function () {
                if ($("#modalForm #nombre").val() !== "") {
                    $('#modalForm :input').each(function () {
                        if ($(this).val() === "") {
                            $(this).parent().addClass("has-error");
                        } else {
                            $(this).parent().removeClass("has-error");
                        }
                    });
                    if ($("#añadirContactos").parent().children().size() <= 1) {
                        $("#añadirContactos").parent().prepend("<div class='form-group'>\n\
                            <div class='col-xs-11 col-xs-offset-1'>\n\
                                <p id='" + $("#myModal #nombre").val() + "'>\n\
                                    <button type='button' class='editar btn btn-sm btn-warning'><i class='fa fa-pencil-square-o'></i></button>\n\
                                    <button type='button' class='eliminar btn btn-sm btn-danger'><i class='fa fa-times'></i></button>\n\
                                    " + $("#myModal #nombre").val() + " " + $("#myModal #apellido1").val() + " " + $("#myModal #apellido2").val() + "\n\
                                </p>\n\
                            </div>\n\
                        </div>");
                    } else {
                        if (!($("#" + $("#modalForm #nombre").val()).length === 1)) {
                            $("#añadirContactos").siblings(".form-group").find(".col-xs-11").append("<p id='" + $("#myModal #nombre").val() + "'>\n\
                                       <button type='button' class='editar btn btn-sm btn-warning'><i class='fa fa-pencil-square-o'></i></button>\n\
                                       <button type='button' class='eliminar btn btn-sm btn-danger'><i class='fa fa-times'></i></button>\n\
                                       " + $("#myModal #nombre").val() + " " + $("#myModal #apellido1").val() + " " + $("#myModal #apellido2").val() + "\n\
                                   </p>");
                        } else {
                            $("#" + $("#modalForm #nombre").val()).html("<p id='" + $("#myModal #nombre").val() + "'>\n\
                                       <button type='button' class='editar btn btn-sm btn-warning'><i class='fa fa-pencil-square-o'></i></button>\n\
                                       <button type='button' class='eliminar btn btn-sm btn-danger'><i class='fa fa-times'></i></button>\n\
                                       " + $("#myModal #nombre").val() + " " + $("#myModal #apellido1").val() + " " + $("#myModal #apellido2").val() + "\n\
                                   </p>");
                        }
                    }
                    guardarDatosEnCookie();
                    $("#myModal form")[0].reset();
                } else {
                    $('#modalForm :input').each(function () {
                        if ($(this).val() === "") {
                            $(this).parent().addClass("has-error");
                        } else {
                            $(this).parent().removeClass("has-error");
                        }
                    });
                }
            });

            $(document).on("click", "button.editar", function () {
                $("#añadirContactos").trigger("click");
                var campos = localStorage.getItem($(this).parent().text().trim().split(" ")[0]);
                console.log(getStorage(campos, "apellido1"));
                var modalForm = $("#modalForm");
                modalForm.find("#nombre").val(getStorage(campos, "nombre"));
                modalForm.find("#apellido1").val(getStorage(campos, "apellido1"));
                modalForm.find("#apellido2").val(getStorage(campos, "apellido2"));
                //modalForm.find("#direccion").val(getStorage(campos, "direccion"));
                modalForm.find("#poblacion").val(getStorage(campos, "poblacion"));
                modalForm.find("#cPostal").val(getStorage(campos, "cPostal"));
                modalForm.find("#tlf").val(getStorage(campos, "tlf"));
            });

            $(document).on("click", "button.eliminar", function () {
                $(this).parent().remove();
            });



            $("#finalizar").on("click", function () {
                $("#myModal form")[0].reset();
            });

            $("#siguiente").on("click", function () {
                localStorage.setItem("anterior0", window.location.href);
                $("form").attr("action", "formularioInformeDependencia.jsp");
                $("form").submit();
            });

            $("#menu").on("click", function () {
                $("form").attr("action", "menu.jsp");
                $("form").submit();
            });

        });
    </script>
    <body>
        <div class="row p-a-2">
            <fieldset>
                <legend class="formLegend2"><fmt:message key="persons.label.personaldata" /></legend>
                <form role="form" class="form-horizontal" name="datpers" method="POST">
                    <div class="form-group">
                        <div class="col-sm-4 col-xs-12">
                            <label class="control-label" for="nombre"></label>
                            <input type="text" class="form-control" id="nombre" placeholder="<fmt:message key='patient.pd.name' />" name="nombre">
                            <div class="alert alert-danger errores" id="nombreError" role="alert">
                                <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
                                <fmt:message key="form.error.wrong_data_text" />
                            </div>
                        </div>
                        <div class="col-sm-4 col-xs-12">
                            <label class="control-label" for="apellido1"></label>
                            <input type="text" class="form-control" id="apellido1" placeholder=<fmt:message key="patient.pd.surname1" /> name="apellido1">
                            <div class="alert alert-danger errores" id="apellido1Error" role="alert">
                                <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
                                <fmt:message key="form.error.wrong_data_text" />
                            </div>
                        </div>
                        <div class="col-sm-4 col-xs-12">
                            <label class="control-label" for="apellido2"></label>
                            <input type="text" class="form-control" id="apellido2" placeholder=<fmt:message key="patient.pd.surname2" /> name="apellido2">
                            <div class="alert alert-danger errores" id="apellido2Error" role="alert">
                                <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
                                <fmt:message key="form.error.wrong_data_text" />
                            </div>
                        </div>
                        <div class="col-sm-3 col-xs-6 m-t-1">
                            <label for="sexo" class="control-label"><fmt:message key="patient.pd.sex" /></label>
                            <select class="form-control" id="sexo" name="sexo">
                                <option value="hombre"><fmt:message key="patient.pd.man" /></option>
                                <option value="mujer"><fmt:message key="patient.pd.woman" /></option>
                            </select>
                        </div>
                        <div class="col-sm-3 col-xs-6 m-t-1">
                            <label class="control-label" for="fechaNacimiento"><fmt:message key="persons.label.birthdate" /></label>
                            <input type="date" class="form-control" id="fechaNacimiento" name="fechaNacimiento">
                        </div>
                        <div class="col-sm-3 col-xs-6 m-t-1">
                            <label for="edad" class="control-label"> <fmt:message key="patient.pd.age" /> </label>
                            <input type="text" class="form-control" id="edad" placeholder="<fmt:message key="patient.pd.age" />" name="edad">
                            <div class="alert alert-danger errores" id="edadError" role="alert">
                                <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
                                <fmt:message key="form.error.wrong_data_numeric" />
                            </div>
                        </div>
                        <div class="col-sm-3 col-xs-6 m-t-1">
                            <label class="control-label" for="dni"><fmt:message key="persons.label.id" /></label>
                            <input type="text" class="form-control" id="dni" placeholder="<fmt:message key="persons.label.id" />" name="dni"/>
                            <div class="alert alert-danger errores" id="dniError" role="alert">
                                <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
                                <fmt:message key="form.error.dni_invalid" />
                            </div>
                        </div>  
                        <div class="col-sm-7 col-xs-12">
                            <label for="direccion"></label>
                            <input type="text" class="form-control" id="direccion" placeholder="<fmt:message key="patient.pd.address" />" name="direccion">
                        </div>
                        <div class="col-sm-3 col-xs-6">
                            <label for="poblacion"></label>
                            <input type="text" class="form-control" id="poblacion" placeholder="<fmt:message key="patient.pd.town" />" name="poblacion">
                        </div>
                        <div class="col-sm-2 col-xs-6">
                            <label for="cPostal"></label>
                            <input type="text" class="form-control" id="cPostal" placeholder=<fmt:message key="patient.pd.postcode" /> name="cPostal">
                            <div class="alert alert-danger errores" id="cPostalError" role="alert">
                                <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
                                <fmt:message key="form.error.wrong_data_numeric" />
                            </div>
                        </div>
                        <div class="col-sm-12 col-xs-12">
                            <label for="masInfo"></label>
                            <textarea class="form-control" id="masInfo" placeholder=<fmt:message key="patient.pd.addressadditional" /> name="masInfo"></textarea>
                        </div>
                        <div class="clearfix visible-sm-block m-b-2"></div>   

                        <div class="col-sm-6 col-xs-12">
                            <label for="email"></label>
                            <input type="text" class="form-control" id="email" placeholder=<fmt:message key="patient.pd.email" /> name="email">
                        </div>
                        <div class="col-sm-3 col-xs-6">
                            <label for="mobil"></label>
                            <input type="text" class="form-control" id="mobil" maxlength="9" placeholder=<fmt:message key="patient.pd.mobno" /> name="mobil">
                            <div class="alert alert-danger errores" id="mobilError" role="alert">
                                <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
                                <fmt:message key="form.error.wrong_data_numeric" />
                            </div>
                        </div>
                        <div class="col-sm-3 col-xs-6">
                            <label for="fijo"></label>
                            <input type="text" class="form-control" id="fijo" maxlength="9" placeholder=<fmt:message key="patient.pd.landno" /> name="fijo">
                            <div class="alert alert-danger errores" id="fijoError" role="alert">
                                <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
                                <fmt:message key="form.error.wrong_data_numeric" />
                            </div>
                        </div>

                    </div>

                    <div class="form-group">
                        <label class="col-xs-12"><fmt:message key="patient.pd.contactlist" /></label>
                        <div class="col-md-12">
                            <p class="btn btn-default" id="añadirContactos" name="añadirContactos" data-toggle="modal" data-target="#myModal"><i class="fa fa-plus"></i> <fmt:message key="patient.pd.newcontact" /></p>
                        </div>
                    </div>
                    <div class="alert alert-danger errorGeneral" role="alert">
                        <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
                        <fmt:message key="form.error.empty_fields" />
                    </div>

                    <div class="alert alert-danger errores" id="dniExiste" role="alert">
                        <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
                        <fmt:message key="form.error.dni_exist" />
                    </div>  
                </form>
            </fieldset>
        </div>
        <div id="myModal" class="modal fade" role="dialog">
            <div class="modal-dialog" style="width: 50%">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title"><fmt:message key="patient.pd.contactdata" /> 
                            <p class="btn btn-default" name="nuevoContacto" id="nuevoContacto"><i class="fa fa-plus"></i> <fmt:message key="patients.label.new" /> </p>
                        </h4>
                    </div>
                    <div class="modal-body">
                        <jsp:include page="formularioDatosDeContactos.jsp"/>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-primary" id="finalizar" data-dismiss="modal"><fmt:message key="patients.label.done" /></button>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>
