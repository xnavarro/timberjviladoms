<%-- 
    Document   : search
    Created on : 04/05/2016, 16:42:01
    Author     : xavi
--%>
<%@page contentType="text/html" pageEncoding="UTF-8" language="java"%>
<%@ page import="java.io.*,
         java.util.*" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<c:set var="language" value="${not empty param.language ? param.language : not empty language ? language : pageContext.request.locale}" scope="session" />
<fmt:setLocale value="${language}" />
<fmt:setBundle basename="/messages/app" /> 
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title><fmt:message key="management.title.general"/></title>
        <link rel="stylesheet" href="bootstrap-3.3.6-dist/css/bootstrap.css" >
        <link rel="stylesheet" href="bootstrap-3.3.6-dist/css/bootstrap-theme.css">
        <link rel="stylesheet" href="font-awesome-4.6.1/css/font-awesome.min.css">
        <script src="https://cdn.datatables.net/1.10.11/js/jquery.dataTables.min.js"></script> <!-- Para las DataTables() -->
        <link href="https://cdn.datatables.net/1.10.11/css/dataTables.bootstrap.min.css"/>
        <script src="bootstrap-3.3.6-dist/js/bootstrap.js" ></script>

        <link rel="stylesheet" type="text/css" href="skin/css/style.css">
    </head>
    <script>
        $(document).on("ready", function () {
            <%String goingTo = (String) request.getParameter("goingTo");%>
            $(".accion").on("click", function () {
                var tipo = $(this).data("type");
                var accion = $(this).data("action");
                var dni = $(this).parent().parent().siblings(".dni").text();
                
                if (tipo === "pac") {
                    url = "ControladorPaciente?goingTo=paciente";
                    eliminar = confirm("<fmt:message key="datatable.alert.delete"/>" + " <fmt:message key="patients.label.message"/>" + "?");
                } else if (tipo === "trab") {
                    url = "ControladorTrabajador?goingTo=trabajador";
                    eliminar = confirm("<fmt:message key="datatable.alert.delete"/>" + " <fmt:message key="employee.label.message"/>" + "?");
                }

                if (accion === "borrar") {
                    if (eliminar) {
                        $.ajax({
                            method: "POST",
                            url: url,
                            data: {dni: dni, accion: accion}
                        }).success(function () {
                            if (url.split("=")[1] === "paciente") {
                                location.href = "ControladorLanding?goingTo=paciente";
                            } else {
                                location.href = "ControladorLanding?goingTo=trabajador";
                            }
                        });
                    }
                }
            });
            
            var table = $("#tablaInfo").DataTable({
                "language": {
                    "lengthMenu": "<fmt:message key="datatable.label.show"/>",
                    "emptyTable": "<fmt:message key="datatable.warning.emptyTable"/>",
                    "zeroRecords": "<fmt:message key="datatable.warning.noRecords"/>",
                    "paginate": {
                        "previous": "<fmt:message key="datatable.label.previous"/>",
                        "next": "<fmt:message key="datatable.label.next"/>"
                    }
                }
            });
            $("#search").on("keyup", function () {
                table.search(this.value).draw();
            });
            $("input[type='search']").parent().remove();
            $("#tablaInfo_info").remove();
            $("th:first").removeClass("sorting_asc");
            $("th:first").unbind("click");
            
            $("#paciente").on("click", function() {
                <%request.setAttribute("tab", "datpers");%>
            });
        });

        function lanzarUrl(obj) {
            var accion = $(obj).data("action");
            var dni = $(obj).parent().parent().siblings(".dni").text();
            console.log(dni);
            var url = "";
            <%if(goingTo != null && goingTo.equals("trabajador")) {%>
                url = "ControladorTrabajador?goingTo=trabajador&accion=" + accion + "&dni=" + dni;
            <%} else if (goingTo != null && goingTo.equals("paciente")) {%>
                url = "ControladorPaciente?goingTo=paciente&accion=" + accion + "&dni=" + dni;
            <%}%>
            $(obj).attr("href", url);
        }
    </script>
    <body>
        <div class="row box">
            <div class=" col-xl-8 col-lg-8 col-md-8">
                <div class="input-group">
                    <input type="text" id="search" class="form-control" placeholder=<fmt:message key="person.placeholder.search" />>
                    <span class="input-group-btn">
                        <button class="btn btn-default" type="button"><i class="fa fa-search"></i></button>
                    </span>
                </div>
            </div>
            <div class=" col-xl-4 col-lg-4 col-md-4 ">
                <!--<p class="btn btn-default" id="añadirContactos" name="añadirContactos" data-toggle="modal" data-target="#myModal"><i class="fa fa-plus"></i> <fmt:message key="patient.pd.newcontact" /></p>-->
                <%if (goingTo != null && goingTo.equals("paciente")) {%>
                <a href="ControladorPaciente?goingTo=paciente&accion=alta" id="paciente" class="btn btn-default pull-right"><i class="fa fa-plus"></i> <fmt:message key="person.button.new" /></a>
                <%} else {%>
                <a href="ControladorTrabajador?goingTo=trabajador" id="trabajador" class="btn btn-default pull-right"><i class="fa fa-plus"></i> <fmt:message key="person.button.new" /></a>
                <%}%>
            </div>
        </div>

    </body>
</html>

