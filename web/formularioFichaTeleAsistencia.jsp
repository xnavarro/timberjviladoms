<%-- 
    Document   : formularioFichaTeleAsistencia
    Created on : Apr 26, 2016, 6:45:46 PM
    Author     : root
--%>
<%@page contentType="text/html" pageEncoding="UTF-8" language="java"%>
<%@ page import="java.io.*,
         java.util.*" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<c:set var="language" value="${not empty param.language ? param.language : not empty language ? language : pageContext.request.locale}" scope="session" />
<fmt:setLocale value="${language}" />
<fmt:setBundle basename="/messages/app" />
<!DOCTYPE html>
<html>
   

    <script type="text/javascript">
        $(document).on("ready", function () {
            $("#alarmas").hide();
            $(".form-group").on("click", function () {
                if ($("#prestacionAdicional5").is(":checked")) {
                    $("#alarmas").show();
                } else {
                    $("#alarmas").hide();
                }
            });

            $("#siguiente").on("click", function () {
                $("form").attr("action", "menu.jsp");
                $("form").submit();
            });

            $("#anterior").on("click", function () {
                $("form").attr("action", localStorage.getItem("anterior2"));
                $("form").submit();
            });
        });
    </script>
    <body>
        <div class="container row p-x-1">
            <fieldset>
                <legend class="formLegend2 p-a-2"> <fmt:message key="patients.label.telecare" /> </legend>
                <form role="form" method="POST" name="teleas">
                    <div class="form-group form-inline">
                        <label  for="fechaAltaSTA"><fmt:message key="patient.tc.from" /></label>
                        <input type="date" class="form-control" id="fechaAltaSTA" name="fechaAltaSTA">
                    </div>
                    <!-- CUOTAS DEL SERVICIO -->
                    <fieldset>
                        <legend class="formLegend2 p-a-2"> <fmt:message key="patient.tc.paymnt" /> </legend>
                        <div class="form-group">					
                            <div class="col-sm-offset-2 control-group">
                                <div class="controls span2">
                                    <label class="checkbox" for="cuotaUnica">
                                        <input type="checkbox" class="" id="cuotaUnica" name="cuotaUnica" value="Única alta"> <fmt:message key="patient.tc.single" />
                                    </label>

                                    <label class="checkbox" for="cuotaDeposito">
                                        <input type="checkbox" class="" id="cuotaDiposito" name="cuotaDeposito" value="Depósito"> <fmt:message key="patient.tc.dep" />
                                    </label>

                                    <label class="checkbox" for="cuotaMensual">
                                        <input type="checkbox" class="" id="cuotaMensual" name="cuotaMensual" value="Mantenimiento mensual"> <fmt:message key="patient.tc.month" />
                                    </label>
                                </div>
                            </div>
                    </fieldset>
                    <!-- DATOS DE LA CUENTA CORRIENTE -->
                    <fieldset>
                        <legend class="formLegend2 p-a-2"> <fmt:message key="patient.tc.bankacnt" /> </legend>
                        <div class="form-inline form-group">
                            <label for="entidadBancaria"></label>
                            <input type="text" class="form-control" maxlength="4" id="entidadBancaria" placeholder=<fmt:message key="patient.tc.bank" /> name="entidadBancaria" size="4" >

                            <label for="sucursalBancaria"></label>
                            <input type="text" class="form-control" maxlength="4" id="sucursalBancaria" placeholder=<fmt:message key="patient.tc.branch" /> name="sucursalBancaria" size="4">

                            <label for="controlBancario"></label>
                            <input type="text" class="form-control" maxlength="2" id="controlBancario" placeholder=<fmt:message key="patient.tc.ccode" /> name="controlBancario" size="2">

                            <label for="cuentaBancaria"></label>
                            <input type="text" class="form-control" maxlength="10" id="cuentaBancaria" placeholder=<fmt:message key="patient.tc.account" /> name="cuentaBancaria" size="10">
                        </div>
                    </fieldset>
                    <!-- DATOS DEL CONTRATADOR -->
                    <fieldset>
                        <legend class="formLegend2 p-a-2"> <fmt:message key="patient.tc.hirer" /> </legend>
                        <div class="form-group">
                            <div class="col-sm-3 col-xs-12">
                                <label class="control-label" for="nombre"></label>
                                <input type="text" class="form-control" id="nombre" placeholder=<fmt:message key="patient.pd.name" /> name="nombre">
                            </div>
                            <div class="col-sm-3 col-xs-12">
                                <label class="control-label" for="apellido1"></label>
                                <input type="text" class="form-control" id="apellido1" placeholder=<fmt:message key="patient.pd.surname1" /> name="apellido1">
                            </div>
                            <div class="col-sm-3 col-xs-12">
                                <label class="control-label" for="apellido2"></label>
                                <input type="text" class="form-control" id="apellido2" placeholder=<fmt:message key="patient.pd.surname2" /> name="apellido2">
                            </div>
                            <div class="col-sm-3 col-xs-12">
                                <label class="control-label" for="dni"></label>
                                <input type="text" class="form-control" id="dni" placeholder=<fmt:message key="persons.label.id" /> name="dni"/>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-4 col-xs-12">
                                <label class="control-label" for="tlf"></label>
                                <input type="text" class="form-control" id="tlf" placeholder=<fmt:message key="patient.cd.contact" /> name="tlf"/>
                            </div>
                            <div class="col-sm-4 col-xs-12">
                                <label class="control-label" for="poblacion"></label>
                                <input type="text" class="form-control" id="poblacion" placeholder=<fmt:message key="patient.pd.town" /> name="poblacion">
                            </div>
                            <div class="col-sm-4 col-xs-12">
                                <label class="control-label" for="cPostal"></label>
                                <input type="text" class="form-control" id="cPostal" placeholder=<fmt:message key="patient.pd.postcode" /> name="cPostal">
                            </div>
                        </div>
                    </fieldset>
                    <!-- SERVICIOS BÁSICOS CONTRATADOS -->
                    <fieldset >
                        <legend class="formLegend2 p-a-2"><fmt:message key="patient.tc.services" /></legend>
                        <div class="form-group ">
                            <label class="control-label col-sm-4 col-xs-12" for="tipoDispositivo"><fmt:message key="patient.tc.device" /> </label>
                            <span class="col-sm-4 col-xs-12">
                                <select class="form-control" name="tipoDispositivo" id="tipoDispositivo">
                                    <option value="dispositivoFijo" selected="selected"><fmt:message key="patient.tc.fixed" /></option>	
                                    <option value="dispositivoInalambrico"><fmt:message key="patient.tc.wireless" /></option>
                                </select>
                            </span>
                        </div>
                    </fieldset>
                    <!-- MODALIDADES DEL STA CONTRATADAS -->
                    <fieldset>
                        <legend class="formLegend2 p-a-2"><fmt:message key="patient.tc.services2" /></legend>
                        <div class="form-group">
                            <label class="control-label col-sm-4 col-xs-12"><fmt:message key="patient.tc.mobile" /> </label>
                            <span class="col-sm-4 col-xs-12">
                                <select class="form-control" name="unidadMobil" id="unidadMobil">
                                    <option value="unidadSiMobil" selected="selected"><fmt:message key="persons.label.yes" /> </option>	
                                    <option value="unidadNoMobil"><fmt:message key="persons.label.no" /> </option>
                                </select>
                            </span>
                        </div>
                    </fieldset>
                    <!-- PRESTACIONES ADICIONALES CONTRATADAS -->
                    <fieldset >
                        <legend class="formLegend2 p-a-2"> <fmt:message key="patient.tc.services3" /> </legend>
                        <div class="form-group">					
                            <div class="col-sm-4 col-xs-12">
                                <div class="">
                                    <label class="checkbox col-sm-12 text_subcheck" for="prestacionAdicional1">
                                        <input type="checkbox" id="prestacionAdicional1" name="prestacionAdicional1" value="prestacionAdicional1"> <fmt:message key="patient.tc.add1" />
                                    </label>

                                    <label class="checkbox col-sm-12 text_subcheck" for="prestacionAdicional2">
                                        <input type="checkbox" id="prestacionAdicional2" name="prestacionAdicional2" value="prestacionAdicional2"> <fmt:message key="patient.tc.add2" />
                                    </label>

                                    <label class="checkbox col-sm-12 text_subcheck" for="prestacionAdicional3">
                                        <input type="checkbox" id="prestacionAdicional3" name="prestacionAdicional3" value="prestacionAdicional3"> <fmt:message key="patient.tc.add3" />
                                    </label>
                                    
                                    <label class="checkbox col-sm-12  text_subcheck" for="prestacionAdicional4">
                                        <input type="checkbox" id="prestacionAdicional4" name="prestacionAdicional4" value="prestacionAdicional4"> <fmt:message key="patient.tc.add4" />
                                    </label>

                                    <label class="checkbox col-sm-12 text_subcheck" for="prestacionAdicional5">
                                        <input type="checkbox" id="prestacionAdicional5" name="prestacionAdicional5" value="prestacionAdicional5"> <fmt:message key="patient.tc.add5" />
                                    </label>
                                </div>

                                <div id="alarmas" class="col-sm-offset-2 pull-left">
                                    <label class="checkbox col-sm-12 text_subcheck2" for="prestacionAdicional51">
                                        <input type="checkbox" id="prestacionAdicional51" name="prestacionAdicional51" value="prestacionAdicional51"> <fmt:message key="patient.tc.add51" />
                                    </label>

                                    <label class="checkbox col-sm-12 text_subcheck2" for="prestacionAdicional52">
                                        <input type="checkbox" id="prestacionAdicional52" name="prestacionAdicional52" value="prestacionAdicional52"> <fmt:message key="patient.tc.add52" />
                                    </label>

                                    <label class="checkbox col-sm-12 text_subcheck2" for="prestacionAdicional53">
                                        <input type="checkbox" id="prestacionAdicional53" name="prestacionAdicional53" value="prestacionAdicional53"> <fmt:message key="patient.tc.add53" />
                                    </label>
                                    <label class="checkbox col-sm-12 text_subcheck2" for="prestacionAdicional54">
                                        <input type="checkbox" id="prestacionAdicional54" name="prestacionAdicional54" value="prestacionAdicional54"> <fmt:message key="patient.tc.add54" />
                                    </label>
                                    <label class="checkbox col-sm-12 text_subcheck2" for="prestacionAdicional55">
                                        <input type="checkbox" id="prestacionAdicional55" name="prestacionAdicional55" value="prestacionAdicional55"> <fmt:message key="patient.tc.add55" />
                                    </label>
                                </div>
                            </div>
                    </fieldset>
                </form>
            </fieldset>
        </div>
    </body>
</html>
