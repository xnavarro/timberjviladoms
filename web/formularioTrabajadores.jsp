<%-- 
    Document   : sumarDatosTrabajadores
    Created on : 28-abr-2016, 6:21:22
    Author     : Yo
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>

        <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.2/jquery.min.js"></script>

        <link rel="stylesheet" href="bootstrap-3.3.6-dist/css/bootstrap.css" >
        <link rel="stylesheet" href="bootstrap-3.3.6-dist/css/bootstrap-theme.css">
        <script src="bootstrap-3.3.6-dist/js/bootstrap.js" ></script>

        <link rel="stylesheet" type="text/css" href="css/estiloGeneral.css">
    </head>
    <script type="text/javascript">
        $(document).on("ready", function () {
            $("#masInfo").parent().parent().remove();
            $("#sexo").parent().parent().remove();
            $("#direccion").parent().parent().remove();
            $("#poblacion").parent().parent().remove();
            $("#cPostal").parent().parent().remove();
            $("#fijo").parent().parent().remove();
            $("#mobil").parent().parent().remove();
            $("#email").parent().parent().remove();
            $("#edad").parent().parent().remove();
            $("#siguiente").parent().parent().remove();
            $("#añadirContactos").parent().parent().remove();
        });
    </script>
    <body>
        <div id="formIncluded">
            <jsp:include page="formularioDatosPersonales.jsp"/>
        </div
      </body>
</html>
