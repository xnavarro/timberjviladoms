<%-- 
    Document   : index
    Created on : Apr 26, 2016, 6:46:40 PM
    Author     : root
--%>

<%@page contentType="text/html" pageEncoding="UTF-8" language="java"%>
<%@ page import="java.io.*,java.util.*" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<c:set var="language" value="${not empty param.language ? param.language : not empty language ? language : pageContext.request.locale}" scope="session" />
<fmt:setLocale value="${language}" />
<fmt:setBundle basename="/messages/app" /> 
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>


<t:plantilla idbody ="signin">
    <jsp:attribute name="header">
        <nav class="navbar navbar-default transparent">
            <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" >
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                </div>
                <div id="navbar" class="navbar-collapse collapse">

                    <ul class="nav navbar-nav navbar-right">
                        <li>
                            <form>
                                <select id="language" class="form-control" name="language" onchange="submit()">
                                    <option value="en" ${language == 'en' ? 'selected' : ''}>English</option>
                                    <option value="ca" ${language == 'ca' ? 'selected' : ''}>Català</option>
                                    <option value="es" ${language == 'es' ? 'selected' : ''}>Español</option>
                                </select>
                            </form> 
                        </li>
                    </ul>
                </div><!--/.nav-collapse -->
            </div>
        </nav>


    </jsp:attribute>
    <jsp:attribute name="footer">
    </jsp:attribute>

    <jsp:body>
        <div class="container">
            <form  class="form-signin" method="post" action="ControladorIndex">
                <h2 class="form-signin-heading inverse">Timber</h2>

                <label class="sr-only"></label>

                <div class="input-group">
                    <span class="input-group-addon" id="basic-addon1"><i class="fa fa-user fa-fw"></i></span>
                    <input type="text" id="nombre" name="nombre" class="form-control" placeholder="<fmt:message key="login.label.username" />" aria-describedby="basic-addon1">
                </div>

                <label  class="sr-only"></label>

                <div class="input-group">
                    <span class="input-group-addon" id="basic-addon2"><i class="fa fa-key fa-fw"></i></span>
                    <input type="text" id="contrasena" name="contrasena" class="form-control" placeholder=<fmt:message key="login.label.password" /> aria-describedby="basic-addon1">
                </div>

                <div class="alert alert-danger errores" id="loginError" role="alert">
                    <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
                    <fmt:message key="login.label.wrong_user" />
                </div>

                <div class="checkbox inverse">
                    <label>
                        <input type="checkbox" /><fmt:message key="login.label.remember" />
                    </label>
                </div>
                <button class="btn btn-lg btn-primary btn-block" type="submit" name="go" id="go"><i class ="fa fa-sign-in"></i> <fmt:message key="login.button.submit" /></button>
                <a class="inverse" href="#"><fmt:message key="login.label.forgot" /></a>
            </form> 
        </div>

    </jsp:body>

</t:plantilla>
<script>
    $(document).on("ready", function () {
        $(".errores").hide();
        <%ArrayList<String> errores = (ArrayList) request.getAttribute("errores");
        if (errores != null && errores.size() != 0) {
            for (String s : errores) {%>
                $("#<%=s%>").show();
            <%}
        }%>
    });
</script>