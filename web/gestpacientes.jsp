<%-- 
    Document   : gestPacientes
    Created on : 26-abr-2016, 6:21:22
    Author     : Yo
--%>
<%@page import="java.util.Map.Entry"%>
<%@page contentType="text/html" pageEncoding="UTF-8" language="java"%>
<%@ page import="java.io.*,
         java.util.*" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<c:set var="language" value="${not empty param.language ? param.language : not empty language ? language : pageContext.request.locale}" scope="session" />
<fmt:setLocale value="${language}" />
<fmt:setBundle basename="/messages/app" /> 
<!DOCTYPE html>
<html>
    <head>

        <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.2/jquery.min.js"></script>

        <link rel="stylesheet" href="bootstrap-3.3.6-dist/css/bootstrap.css" >
        <link rel="stylesheet" href="bootstrap-3.3.6-dist/css/bootstrap-theme.css">

        <link rel="stylesheet" type="text/css" href="skin/css/style.css">
    </head>
    <script>
        $(document).on("ready", function () {
        <%  HashMap<String, String> info = (HashMap) request.getAttribute("info");
            if (info != null) {
                for (Entry<String, String> entry : info.entrySet()) {
                    String key = entry.getKey();
                    String value = entry.getValue();
                    if (key.equals("esAdmin")) {%>
            if ("<%=value%>" === "SI") {
                $("#<%=key%>").attr("checked", "true");
            }
        <%} else {%>
            $("#<%=key%>").val("<%=value%>");
        <%}
                }
            }%>

            $(".errores").hide();
            $(".errorGeneral").hide();
        <%ArrayList<String> errores = (ArrayList) request.getAttribute("errores");
            if (errores != null && !errores.isEmpty()) {
                for (String s : errores) {%>
            if ("<%=s.equals("errorGeneral")%>" === "true") {
                $(".errorGeneral").show();
            }
            $("#<%=s%>").show();
        <%}
            }%>


        });

        function lanzarInsert() {
            document.formEmpleado.action = "ControladorTrabajador?accion=alta";
            document.formEmpleado.submit();
        }

        function lanzarUpdate() {
            document.formEmpleado.action = "ControladorTrabajador?accion=modificando";
            document.formEmpleado.submit();
        }
    </script>
    <body>
        <jsp:include page="topmenu.jsp"/>
        <div class="container">
            <h1><fmt:message key="patients.label.caredata" /></h1>
            <hr>
            <div class="row box">
                <div>

                    <!-- Nav tabs -->
                    <ul class="nav nav-tabs" role="tablist">
                        <li role="presentation" class=""><a href="#datpers" aria-controls="datpers" role="tab" data-toggle="tab"><fmt:message key="persons.label.personaldata" /></a></li>
                        <li role="presentation" class=""><a href="#infdep" aria-controls="infdep" role="tab" data-toggle="tab"><fmt:message key="patients.label.careassistance" /></a></li>
                        <li role="presentation" class=""><a href="#fichsan" aria-controls="fichsan" role="tab" data-toggle="tab"><fmt:message key="patients.label.medicalinfo" /></a></li>
                        <li role="presentation" class=""><a href="#teleas" aria-controls="teleas" role="tab" data-toggle="tab"><fmt:message key="patients.label.telecare" /></a></li>
                    </ul>

                    <!-- Tab panes -->
                    <div class="tab-content">
                        <div role="tabpanel" class="tab-pane" id="datpers">
                            <jsp:include page="formularioDatosPersonales.jsp"/>
                        </div>
                        <div role="tabpanel" class="tab-pane" id="infdep">
                            <jsp:include page="formularioInformeDependencia.jsp"/>
                        </div>
                        <div role="tabpanel" class="tab-pane" id="fichsan">
                            <jsp:include page="formularioFichaSanitaria.jsp"/>
                        </div>
                        <div role="tabpanel" class="tab-pane" id="teleas">
                            <jsp:include page="formularioFichaTeleAsistencia.jsp"/>
                        </div>
                    </div>
                </div>
            </div>
            <script>
                $(document).on("ready", function () {
                <%String tab = (String) request.getAttribute("tab");%>
                    $("a[href='#<%=tab%>']").parent().addClass("active");
                    $("#<%=tab%>").addClass("active");
                });

                function lanzarForm() {
                    document.forms.<%=tab%>.action = "ControladorPaciente?goingTo=paciente&accion=<%=request.getParameter("accion")%>&tab=<%=tab%>";
                    document.forms.<%=tab%>.submit();
                }
            </script>
            <div class="btn-group pull-right" role="group">
                <a href="ControladorLanding?goingTo=paciente" class="btn btn-default btn-danger" ><i class="fa fa-times"></i><fmt:message key="button.action.cancel"/></a>
                <a href="javascript:lanzarForm()" id="guardar" class="btn btn-default btn-success" ><i class="fa fa-save"></i><fmt:message key="button.action.save"/></a>
            </div> 
        </div>
    </body>

</html>
