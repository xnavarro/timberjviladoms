<%-- 
    Document   : gestPacientes
    Created on : 26-abr-2016, 6:21:22
    Author     : Yo
--%>
<%@page import="java.util.Map.Entry"%>
<%@page contentType="text/html" pageEncoding="UTF-8" language="java"%>
<%@ page import="java.io.*,
         java.util.*" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<c:set var="language" value="${not empty param.language ? param.language : not empty language ? language : pageContext.request.locale}" scope="session" />
<fmt:setLocale value="${language}" />
<fmt:setBundle basename="/messages/app" /> 
<!DOCTYPE html>
<html>
    <head>

        <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.2/jquery.min.js"></script>

        <link rel="stylesheet" href="bootstrap-3.3.6-dist/css/bootstrap.css" >
        <link rel="stylesheet" href="bootstrap-3.3.6-dist/css/bootstrap-theme.css">
        <script src="bootstrap-3.3.6-dist/js/bootstrap.js" ></script>

        <link rel="stylesheet" type="text/css" href="skin/css/style.css">
    </head>
    <script>
        $(document).on("ready", function () {
        <%  HashMap<String, String> info = (HashMap) request.getAttribute("info");
            if (info != null) {
                for (Entry<String, String> entry : info.entrySet()) {
                    String key = entry.getKey();
                    String value = entry.getValue();
                    if (key.equals("esAdmin")) {%>
            if ("<%=value%>" === "SI") {
                $("#<%=key%>").attr("checked", "true");
            }
        <%} else {%>
            $("#<%=key%>").val("<%=value%>");
        <%}
                }
            }%>

            $(".errores").hide();
            $(".errorGeneral").hide();
        <%ArrayList<String> errores = (ArrayList) request.getAttribute("errores");
            if (errores != null && !errores.isEmpty()) {
                for (String s : errores) {%>
            if ("<%=s.equals("errorGeneral")%>" === "true") {
                $(".errorGeneral").show();
            }
            $("#<%=s%>").show();
        <%}
            }%>


        });

        function lanzarInsert() {
            document.formEmpleado.action = "ControladorTrabajador?accion=alta";
            document.formEmpleado.submit();
        }

        function lanzarUpdate() {
            document.formEmpleado.action = "ControladorTrabajador?accion=modificando";
            document.formEmpleado.submit();
        }
    </script>
    <body>
        <jsp:include page="topmenu.jsp"/>
        <div class="container">
            <h1><fmt:message key="employee.label.caredata" /></h1>
            <hr>
            <div class="row box">

                <form role="form" class="form-horizontal" name="formEmpleado" method="POST">
                    <div>
                        <!-- Nav tabs -->
                        <ul class="nav nav-tabs" role="tablist">
                            <li role="presentation" class="active"><a href="#datpers" aria-controls="datpers" role="tab" data-toggle="tab"><fmt:message key="persons.label.personaldata" /></a></li>
                        </ul>

                        <!-- Tab panes -->
                        <div class="tab-content">
                            <div role="tabpanel" class="tab-pane active col-xs-12" id="datpers">
                                <div class="col-xs-6 col-xs-push-3">
                                    <%if (request.getAttribute("dadoDeAlta") != null) {%>
                                    <div class="alert alert-success" role="alert">
                                        <span class="glyphicon glyphicon-ok" aria-hidden="true"></span>
                                        <fmt:message key="form.employee.info_label" />
                                    </div>
                                    <%}%>
                                    <fieldset>
                                        <legend class="formLegend"><fmt:message key="persons.label.personaldata"/></legend>
                                        <div class="form-group">
                                            <div class="col-md-4 col-xs-12">
                                                <label class="control-label" for="nombre"></label>
                                                <input type="text" class="form-control" id="nombre" placeholder="<fmt:message key="patient.pd.name"/>" name="nombre" <%=request.getAttribute("readOnly")%>>
                                                <div class="alert alert-danger errores" id="nombreError" role="alert">
                                                    <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
                                                    <fmt:message key="form.error.wrong_data_text" />
                                                </div>
                                            </div>
                                            <div class="col-md-4 col-xs-12">
                                                <label class="control-label" for="apellido1"></label>
                                                <input type="text" class="form-control" id="apellido1" placeholder=<fmt:message key="patient.pd.surname1"/> name="apellido1" <%=request.getAttribute("readOnly")%>>
                                                <div class="alert alert-danger errores" id="apellido1Error" role="alert">
                                                    <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
                                                    <fmt:message key="form.error.wrong_data_text" />
                                                </div>
                                            </div>
                                            <div class="col-md-4 col-xs-12">
                                                <label class="control-label" for="apellido2"></label>
                                                <input type="text" class="form-control" id="apellido2" placeholder=<fmt:message key="patient.pd.surname2"/> name="apellido2" <%=request.getAttribute("readOnly")%>>
                                                <div class="alert alert-danger errores" id="apellido2Error" role="alert">
                                                    <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
                                                    <fmt:message key="form.error.wrong_data_text" />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-md-12">
                                                <label class="control-label" for="fechaNacimiento"><fmt:message key="persons.label.birthdate"/> </label>
                                                <input type="date" class="form-control" id="fechaNacimiento" placeholder="" name="fechaNacimiento" <%=request.getAttribute("readOnly")%>>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-md-12">
                                                <label for="dni"></label>
                                                <input type="text" class="form-control" id="dni" placeholder="<fmt:message key="persons.label.id"/>" name="dni" <%=request.getAttribute("readOnly")%>>
                                                <div class="alert alert-danger errores" id="dniError" role="alert">
                                                    <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
                                                    <fmt:message key="form.error.dni_invalid" />
                                                </div>
                                            </div>
                                        </div>
                                    </fieldset>
                                    <legend class="formLegend"><fmt:message key="employee.label.userData" /></legend>
                                    <div class="form-group">
                                        <div class="col-md-12">
                                            <label class="control-label" for="usuario"></label>
                                            <input type="text" class="form-control" id="usuario" name="usuario" placeholder="<fmt:message key="login.label.username"/>" name="usuario" <%=request.getAttribute("readOnly")%>/>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-md-12">
                                            <label class="control-label" for="contrasena"></label>
                                            <input type="password" class="form-control" id="contrasena" name="contrasena" placeholder=<fmt:message key="login.label.password"/> name="contrasena" <%=request.getAttribute("readOnly")%>/> 
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="control-group col-md-12">
                                            <label class="control-label" for="esAdmin">
                                                <input type="checkbox" id="esAdmin" name="esAdmin" <%=request.getAttribute("readOnly")%>> <fmt:message key="employee.question.isadm"/>
                                            </label>
                                        </div>
                                    </div>
                                    <div class="alert alert-danger errorGeneral" role="alert">
                                        <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
                                        <fmt:message key="form.error.empty_fields" />
                                    </div>

                                    <div class="alert alert-danger errores" id="dniExiste" role="alert">
                                        <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
                                        <fmt:message key="form.error.dni_exist" />
                                    </div>

                                    <div class="alert alert-danger errores" id="userExiste" role="alert">
                                        <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
                                        <fmt:message key="form.error.user_exist" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="btn-group pull-right" role="group">
                        <a href="ControladorLanding?goingTo=paciente" class="btn btn-default btn-danger" ><i class="fa fa-times"></i><fmt:message key="button.action.cancel"/></a>
                            <%  String accion = (String) request.getParameter("accion");
                                if (accion != null && (accion.equals("modificar") || accion.equals("modificando"))) {%>
                        <button onClick="lanzarUpdate()" class="btn btn-default btn-success" ><i class="fa fa-save"></i><fmt:message key="button.action.save"/></button>
                            <%} else if (accion == null || accion.equals("alta")) {%>
                        <button onClick="lanzarInsert()" class="btn btn-default btn-success" ><i class="fa fa-save"></i><fmt:message key="button.action.save"/></button>
                            <%}%>
                    </div>

                </form>
            </div>
    </body>

</html>
