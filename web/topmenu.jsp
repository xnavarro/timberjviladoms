<%-- 
    Document   : topmenu
    Created on : 04/05/2016, 16:18:17
    Author     : xavi
--%>
<%@page contentType="text/html" pageEncoding="UTF-8" language="java"%>
<%@ page import="java.io.*,
         java.util.*" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<c:set var="language" value="${not empty param.language ? param.language : not empty language ? language : pageContext.request.locale}" scope="session" />
<fmt:setLocale value="${language}" />
<fmt:setBundle basename="/messages/app" />    
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title><fmt:message key="management.title.general"/></title>
        <link rel="stylesheet" href="bootstrap-3.3.6-dist/css/bootstrap.css" >
        <link rel="stylesheet" href="bootstrap-3.3.6-dist/css/bootstrap-theme.css">
        <link rel="stylesheet" href="font-awesome-4.6.1/css/font-awesome.min.css">
        <script src="https://cdn.datatables.net/1.10.11/js/jquery.dataTables.min.js"></script> <!-- Para las DataTables() -->
        <link href="https://cdn.datatables.net/1.10.11/css/dataTables.bootstrap.min.css"/>
        <script src="bootstrap-3.3.6-dist/js/bootstrap.js" ></script>

        <link rel="stylesheet" type="text/css" href="skin/css/style.css">
    </head>
    <script>    
    $(document).on("ready", function() {  
        $(".nav").find(".active").removeClass("active");
        $("#<%=(String)request.getParameter("goingTo")%>").parent().addClass("active");
        
    <%
        String usuario = (String)request.getSession().getAttribute("usuario");
        if (((String)request.getSession().getAttribute("isAdmin")).equalsIgnoreCase("false")) {%>
        $("a[href='ControladorLanding?goingTo=trabajador']").parent().remove();
    <%}%>
    });
     </script>
    <body>
        <nav class="navbar navbar-default ">
            <div class="container">
                <div class="navbar-header">

                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" >
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>

                    <a class="navbar-brand">TIMBER</a>
                </div>
                <div id="navbar" class="navbar-collapse collapse">
                    <ul class="nav navbar-nav">
                        <li><a id="paciente" href="ControladorLanding?goingTo=paciente"><fmt:message key="topmenu.label.patients" /></a></li>
                        <li><a id="trabajador" href="ControladorLanding?goingTo=trabajador"><fmt:message key="topmenu.label.employees" /></a></li>
                        <li><a href="llamar.jsp"><fmt:message key="topmenu.label.call" /></a></li>
                    </ul>
                    <ul class="nav navbar-nav navbar-right">
                        <li><p class="navbar-text"><%=usuario%></p></li>
                        <li><a href="ControladorLanding?goingTo=paciente" class="navbar-link"><i class ="fa fa-cog"></i></a></li>
                        <li><a href="index.jsp" class="navbar-link"><i class ="fa fa-sign-out"></i></a></li>
                    </ul>
                </div><!--/.nav-collapse -->
            </div>
        </nav>

    </body>
</html>
