<%-- 
    Document   : formularioFichaSanitaria
    Created on : Apr 26, 2016, 6:45:27 PM
    Author     : root
--%>
<%@page contentType="text/html" pageEncoding="UTF-8" language="java"%>
<%@ page import="java.io.*,
         java.util.*" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<c:set var="language" value="${not empty param.language ? param.language : not empty language ? language : pageContext.request.locale}" scope="session" />
<fmt:setLocale value="${language}" />
<fmt:setBundle basename="/messages/app" />
<!DOCTYPE html>
<html>
            <script>
            $(document).on("ready", function () {
                $("#datosPublica").hide();
                $("#datosPrivada").hide();

                $(".control-label").on("click", function () {
                    if ($("#privado").is(":checked")) {
                        $("#datosPrivada").show();
                    } else {
                        $("#datosPrivada").hide();
                    }

                    if ($("#publico").is(":checked")) {
                        $("#datosPublica").show();
                    } else {
                        $("#datosPublica").hide();
                    }
                });

                $("#siguiente").on("click", function () {
                    localStorage.setItem("anterior2", window.location.href);
                    $("form").attr("action", "formularioFichaTeleAsistencia.jsp");
                    $("form").submit();
                });

                $("#atras").on("click", function () {
                    $("form").attr("action", localStorage.getItem("anterior1"));
                    $("form").submit();
                });

            });

        </script>
    <body>
        <div class="row p-a-2">
            <form role="form" class="form-horizontal" name="fichsan" method="POST">
                <fieldset>
                    <legend  class="formLegend"><fmt:message key="patients.label.medicalinfo" /></legend>
                    <div class="form-group">
                        <div class="col-sm-3 col-xs-6">
                            <label class="control-label"></label>
                            <div class="input-group p-a-1">
                                <input id="altura" name="altura" class="form-control " placeholder=<fmt:message key="patient.md.height" /> type="text">
                                <span class="input-group-addon">cm</span>
                            </div>
                        </div>
                        <div class="col-sm-3 col-xs-6">
                            <label class="control-label "></label>
                            <div class="input-group p-a-1">
                                <input id="peso" name="peso" class="form-control" placeholder=<fmt:message key="patient.md.weight" /> type="text">
                                <span class="input-group-addon">kg</span>
                            </div>
                        </div>

                        <div class="col-sm-6 col-xs-12">
                            <label class="control-label" for="estadoActual"><fmt:message key="patient.md.status" /></label>
                            <select class="form-control" id="estadoActual" name="estadoActual">

                                <option value="buena"><fmt:message key="patient.md.healthy" /> </option>
                                <option value="enfermo"><fmt:message key="patient.md.ill" /> </option>
                                <option value="muyEnfermo"><fmt:message key="patient.md.severelyill" /> </option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-6 col-xs-12">
                            <label class="control-label" for="patologias"></label>
                            <textarea class="form-control" id="patologias" name="patologias" placeholder=<fmt:message key="patient.md.pathologies" /> ></textarea>
                        </div>

                        <div class="col-sm-6 col-xs-12">
                            <label class="control-label" for="alergias"></label>
                            <textarea class="form-control" id="alergias" name="alergias" placeholder=<fmt:message key="patient.md.allergies" /> ></textarea>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-sm-6 col-xs-12">
                            <label><fmt:message key="patient.md.systems" /></label>
                            <br>

                            <div class="">
                                <label class="checkbox col-sm-12 col-sm-offset-2 text_subcheck" for="osteomuscular">
                                    <input type="checkbox" class="" id="osteomuscular" name="osteomuscular" value="osteomuscular"> <fmt:message key="patient.md.osteo" />
                                </label>
                                <label class="checkbox col-sm-12 col-sm-offset-2 text_subcheck" for="cardiovascular">
                                    <input type="checkbox" class="" id="cardiovascular" name="cardiovascular" value="cardiovascular"> <fmt:message key="patient.md.cardio" />
                                </label>
                                <label class="checkbox col-sm-12 col-sm-offset-2 text_subcheck" for="respiratorio">
                                    <input type="checkbox" class="" id="respiratorio" name="respiratorio" value="respiratorio"> <fmt:message key="patient.md.respir" />
                                </label>
                                <label class="checkbox col-sm-12 col-sm-offset-2 text_subcheck" for="digestivo">
                                    <input type="checkbox" class="" id="digestivo" name="digestivo" value="digestivo"> <fmt:message key="patient.md.digest" />
                                </label>
                                <label class="checkbox col-sm-12 col-sm-offset-2 text_subcheck" for="nervioso">
                                    <input type="checkbox" class="" id="nervioso" name="nervioso" value="nervioso"> <fmt:message key="patient.md.nerv" />
                                </label>
                                <label class="checkbox col-sm-12 col-sm-offset-2 text_subcheck" for="reproductorExcretor">
                                    <input type="checkbox" class="" id="reproductorExcretor" name="reproductorExcretor" value="reproductorExcretor"> <fmt:message key="patient.md.repexc" />
                                </label>

                            </div>

                        </div>

                        <div class="col-sm-6 col-xs-12">
                            <label class="control-label" for="cognitiva"><fmt:message key="patient.md.cognitive" /></label>
                            <select class="form-control" id="cognitiva" name="cognitiva">
                                <option value="normal"><fmt:message key="patient.md.none" /></option>
                                <option value="deterioroLeve"><fmt:message key="patient.md.verylow" /></option>
                                <option value="demenciaLeve"><fmt:message key="patient.md.low" /></option>
                                <option value="demenciaModerada"><fmt:message key="patient.md.moderate" /></option>
                                <option value="demenciaGrave"><fmt:message key="patient.md.severe" /></option>
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-xs-12">
                            <label class="control-label" for="enfermedades"></label>
                            <input type="text" class="form-control" id="enfermedades" name="enfermedades" placeholder=<fmt:message key="patient.md.illness" /> >
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-xs-12">
                            <label class="control-label" for="medicacion"></label>
                            <input type="text" class="form-control" id="medicacion" name="medicacion" placeholder=<fmt:message key="patient.md.medication" />/>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-xs-12">
                            <label class="control-label" for="deficiencias"></label>
                            <input type="text" class="form-control" id="deficiencias" name="deficiencias" placeholder=<fmt:message key="patient.md.perception" />/>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-xs-12">
                            <label class="control-label" for="vacunas"></label>
                            <input type="text" class="form-control" id="vacunas" name="vacunas" placeholder=<fmt:message key="patient.md.vaccines" /> />
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-xs-12">
                            <label class="control-label" for="proveedorsalud"><fmt:message key="patient.md.health" /></label>
                        </div>

                        <div class="col-xs-12 col-md-6">
                            <label class="control-label col-md-6">
                                <input type="radio" name="centroSanitario" id="publico" value="publico"/> <fmt:message key="patient.md.public"/> 
                            </label>
                            <div id="datosPublica" class="col-md-10">
                                <div>
                                    <label class="control-label" for="cap"></label>
                                    <input type="text" class="form-control" id="cap" name="cap" placeholder=<fmt:message key="patient.md.mc"/>>
                                </div>
                                <div>
                                    <label class="control-label" for="capTlf"></label>
                                    <input type="text" class="form-control" id="capTlf" name="capTlf" placeholder=<fmt:message key="patient.md.mctel"/>>
                                </div>
                                <div>
                                    <label class="control-label" for="medico"></label>
                                    <input type="text" class="form-control" id="medico" name="medico" placeholder=<fmt:message key="patient.md.doctor"/>>
                                </div>
                                <div>
                                    <label class="control-label" for="hospital"></label>
                                    <input type="text" class="form-control" id="hospital" name="hospital" placeholder=<fmt:message key="patient.md.hospital"/>>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-12 col-md-6">
                            <label class="control-label col-md-6">
                                <input type="radio" name="centroSanitario" id="privado" value="privado"/> <fmt:message key="patient.md.private"/>
                            </label>
                            <div id="datosPrivada" class="col-md-10">
                                <div>
                                    <label class="control-label" for="mutua"></label>
                                    <input type="text" class="form-control" id="mutua" name="mutua" placeholder=<fmt:message key="patient.md.hmo"/>>
                                </div>
                                <div>
                                    <label class="control-label" for="hospitalPrivado"></label>
                                    <input type="text" class="form-control" id="hospitalPrivado" name="hospitalPrivado" placeholder=<fmt:message key="patient.md.hospital"/>>
                                </div>
                            </div>
                        </div>
                    </div>
                </fieldset>
            </form>
        </div>
    </body>

</html>