<%-- 
    Document   : mainPacientes
    Created on : 26-abr-2016, 6:21:22
    Author     : Yo
--%>
<%@page contentType="text/html" pageEncoding="UTF-8" language="java"%>
<%@ page import="java.io.*,
         java.util.*" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<c:set var="language" value="${not empty param.language ? param.language : not empty language ? language : pageContext.request.locale}" scope="session" />
<fmt:setLocale value="${language}" />
<fmt:setBundle basename="/messages/app" /> 
<!DOCTYPE html>
<html>
    <head>


        <link rel="stylesheet" href="https://cdn.datatables.net/1.10.11/css/jquery.dataTables.min.css"/>
        <link rel="stylesheet" href="bootstrap-3.3.6-dist/css/bootstrap.css" >
        <link rel="stylesheet" href="bootstrap-3.3.6-dist/css/bootstrap-theme.css">
        <link rel="stylesheet" type="text/css" href="skin/css/style.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.2/jquery.min.js"></script>
        <script src="https://cdn.datatables.net/1.10.11/js/jquery.dataTables.min.js"></script> <!-- Para las DataTables() -->
        <script src="bootstrap-3.3.6-dist/js/bootstrap.js" ></script>
    </head>
    <script type="text/javascript">
        
    </script>
    <body>


        <div id="info" class="container">
            <form name="tabla">
                <table id="tablaInfo" class="table table-responsive table-striped display">
                    <thead>
                        <tr>
                            <th width="25%"></th>
                            <th width="10%"><fmt:message key="persons.label.id" /></th>
                            <th width="25%%"><fmt:message key="persons.label.namesurname" /></th>
                            <th width="15%"><fmt:message key="persons.label.birthdate" /></th>
                            <th width="15%"><fmt:message key="persons.label.user" /></th>
                            <th width="5%"><fmt:message key="persons.label.admin" /></th>
                        </tr>
                    </thead>
                    <tbody>

                        <c:forEach items="${trabajadores}" var="empleado">
                            <tr>
                                <td class="botonera col-xs-2"> 

                                    <span class="col-xs-3">
                                        <a class="btn btn-sm btn-primary trab-consultar" onclick="javascript:lanzarUrl(this);" data-action="consultar" data-type="trab">
                                            <span class="glyphicon glyphicon-eye-open"></span>
                                        </a>
                                    </span>
                                    <span class="col-xs-3">
                                        <a class="btn btn-sm btn-success trab-llamar" onclick="javascript:lanzarUrl(this);" data-action="llamar" data-type="trab">
                                            <span class="glyphicon glyphicon-phone-alt"></span>
                                        </a>
                                    </span>
                                    <span class="col-xs-3">
                                        <a class="btn btn-sm btn-warning trab-modificar" onclick="javascript:lanzarUrl(this);" data-action="modificar" data-type="trab">
                                            <span class="glyphicon glyphicon-pencil"></span>
                                        </a>
                                    </span>
                                    <span class="col-xs-3">
                                        <a class="btn btn-sm btn-danger trab-borrar accion" data-action="borrar" data-type="trab">
                                            <span class="glyphicon glyphicon-trash"></span>
                                        </a>
                                    </span>

                                </td>
                                <td class="dni"><c:out value="${empleado.dni}"/></td>
                                <td><c:out value="${empleado.nombre} ${empleado.apellido1} ${empleado.apellido2}"/></td>
                                <td><c:out value="${empleado.fechaNacimiento}"/></td> 
                                <td><c:out value="${empleado.usuario}"/></td> 
                                <td><c:out value="${empleado.esAdmin}"/></td> 
                            </tr>
                        </c:forEach>
                    </tbody>
                </table>
            </form>
        </div>
    </body>
</html>
