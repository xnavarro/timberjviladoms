<%-- 
    Document   : llamar
    Created on : 26-abr-2016, 6:21:22
    Author     : Yo
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<c:set var="language" value="${not empty param.language ? param.language : not empty language ? language : pageContext.request.locale}" scope="session" />
<fmt:setLocale value="${language}" />
<fmt:setBundle basename="/messages/app" /> 
<!DOCTYPE html>
<html>
    <head>
        <title><fmt:message key="management.title.call"/></title>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.2/jquery.min.js"></script>
        <link rel="stylesheet" href="bootstrap-3.3.6-dist/css/bootstrap.css" >
        <link rel="stylesheet" href="bootstrap-3.3.6-dist/css/bootstrap-theme.css">
        <script src="bootstrap-3.3.6-dist/js/bootstrap.js" ></script>

        <link rel="stylesheet" type="text/css" href="skin/css/style.css">
        <script src="http://www.w3schools.com/lib/w3data.js"></script>
    </head>
    <body>
        <jsp:include page="topmenu.jsp"/>
        <div class="container">
            <h1><fmt:message key="topmenu.label.call"/></h1>
            <hr>
            <div class="row box">
                <div>

                <div class="clearfix">
                    <img src="skin/img/old_phone_mockup.png" alt="Phone mockup">
                </div>
            </div>
        </div>
    </body>
</html>
