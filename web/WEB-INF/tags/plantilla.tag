<%-- 
    Document   : plantilla
    Created on : 07/05/2016, 11:49:10
    Author     : xavi
--%>
<%@tag description="Plantilla basica" pageEncoding="UTF-8"%>
<%@attribute name="header" fragment="true" %>
<%@attribute name="footer" fragment="true" %>
<%@attribute name="idbody" required="true"%>
<html>
    <head>
        <meta charset="UTF-8"/>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.2/jquery.min.js"></script>
        <link rel="stylesheet" href="bootstrap-3.3.6-dist/css/bootstrap.css" >
        <link rel="stylesheet" href="font-awesome-4.6.1/css/font-awesome.min.css">
        <link rel="stylesheet" type="text/css" href="skin/css/style.css">
        <script src="bootstrap-3.3.6-dist/js/bootstrap.js" ></script>
        <title>Timber - managing telecare</title>
       
    </head>
    <body id=${idbody}>
        <div id="pageheader">
            <jsp:invoke fragment="header"/>
        </div>
        <div id="body">
            <jsp:doBody/>
        </div>
        <div id="pagefooter">
            <jsp:invoke fragment="footer"/>
        </div>
        
    </body>
</html>